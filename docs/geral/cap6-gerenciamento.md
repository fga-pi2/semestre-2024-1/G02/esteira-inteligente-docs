# **6. Aspectos de gerenciamento do projeto**

## **6.1. Termo de abertura do projeto**
 
O termo de abertura do projeto é responsável por trazer uma visão detalhada do gerenciamento do projeto,apontando os requisitos do projeto, seu objetivo geral, os stakeholders envolvidos, os riscos identificados e o plano de gerenciamento de riscos e demais informações essenciais para a concepção do projeto.

### **6.1.1 Requisitos do projeto**

Durante a concepção da ideia do projeto, foram discutidos os pontos em que a Esteira Inteligente deve solucionar. Este apêndice descreve as funcionalidades da esteira e o nível de criticidade de cada requisito da Esteira Inteligentes. 

Os requisitos foram levantados a partir de <i>brainstorms</i> do grupo no decorrer das aulas da disciplina:
<h6 style="text-align:center">Tabela 6.1.11: Requisitos funcionais para o produto</h6>

| Requisito | Descrição | Observações| Nível de Criticidade |
| :-------: | :-------: | :--------: |:------: |
|RF01| Composição das esteiras | O sistema será composto por três esteiras que funcionam a partir de passos.| Desejável. A ideia é que o sistema de esteiras seja personalizado para cada tipo de industria, sendo a ideia principal ele ser modular, podendo ser possível a adição de esteiras de acordo com a necessidade do cliente.|
|RF02|Quantidade de componentes |O sistema deverá aceitar 3 componentes diferentes.| Desejável. Como explicado no RF01.|
|   RF03    |    Distribuição das peças     |A esteira será de tal forma a possuir divisórias que compõem cada uma das peças a serem triadas. | Essencial. Como a esteira precisa do controle dos atuadores por meio de controle de passos é necessário a utilização de métodos para que peças sejam separadas. |
|   RF04    |   Assertividade de despacho   |O sistema deve liberar apenas os componentes dos kits do kit escolhido. Além disso, o passo da esteira deve ser tal que caia apenas um componente por vez.| Essencial. Explicado no RF03|
|   RF05    |      Checagem por imagem      |As câmeras deverão ser posicionadas de tal forma que reconheça  se os componentes que estarão distribuídos sobre a célula de carga estão com a quantidade correta| Essencial. Um dos principais diferenciais da Esteira Inteligente é ter a habilidade de fazer uma dupla checagem |
|   RF06    |Dupla checagem|A célula de carga deverá checar se o peso das peças corresponde ao kit escolhido.|Essencial. Elemento da dupla checagem|
|   RF07    |Aviso de verificação|Caso exista divergência da quantidade de peças nos processos de checagem o operador deverá ser comunicado pela tela.| Essencial para manter a movimentação continua da Esteira Inteligente quando solicitada, enquanto não for corrigida essa divergência a esteira não funcionará. |
|   RF08    | Armazenamento dos componentes |Ao final do processo as peças deverão estar dispostas nas divisórias da célula de carga. Ao final do processo o operador receberá o aviso para ensacá-los.| Desejável. Para que aconteça o prosseguimento de montagem dos kits de forma continua. |
|   RF09    |  Cadastro de dados dos kits   | O sistema deve ter um cadastro único de cada kit, onde, este deve possuir quantos e quais componentes possui. Os componentes do kit devem conter as informações de: nome do componente, dimensões de tamanho, peso unitário, um id de 8 dígitos inteiros, kits em que são vinculados. | Essencial. A equipe de engenharia deve fazer esse cadastro de kits para o funcionamento correto da esteira. |
|   RF10    |Alimentação do sistema     |O sistema deve funcionar sendo alimentado por uma bateria com sistema semelhante ao <i>NoBreak<i>| Essencial. Em caso de falta de energia a Esteira Inteligente deverá ser hábil de concluir um ciclo de operação |
|   RF11    |Validação de acesso|Deverá existir um sistema de cadastro de operadores, apenas operadores cadastrados poderão utilizar o equipamento.| Essencial. Para o funcionamento Correto da esteira é necessário o cadastro dos componentes e dos Kits. |


<p style="text-align:center">Fonte: Autoria própria.</p>


<h6 style="text-align:center">Tabela 6.1.12: Requisitos não funcionais para o produto</h6>

| Requisito | Descrição|Observações| Nível de Criticidade |
| :-------: | :------: | :-------: |:-------: |
|   RNF01   |Usabilidade do sistema|A usabilidade do sistema deve ser intuitiva de forma a não ser custoso o uso pelo operador.| Essencial. Os principal Stakeholders da solução é operador e Industria Nacional de Médio e pequeno porte. |
|   RNF02   |     Ergonomia do sistema      | O design da estrutura deve ser tal qual facilite a interação entre o operador e a esteira inteligente. | Desejável. A esteira deve reduzir o máximo do desconforto do operador durante a operação. |
|   RNF03   |  Limitação de uso do sistema  |O uso do sistema deve ser restrita aos operadores responsáveis.| Essencial. Para evitar o uso de operadores não habituados com a máquina, a fim de evitar risco de acidentes de trabalho|
|   RNF04   | Consumo energético do sistema |       O sistema energético deve ser eficiente e otimizado para evitar altos custos operacionais.       | Desejável. Para que o consumo de energético do sistema não seja elevado.|
|   RNF05   |   Confiabilidade do sistema   |          A esteira inteligente deve ser o mais assertiva de forma a minimizar erros e falhas.          | Essencial. A esteira deve ser projetada para esse intuito, visando evitar problemas de alteração de projeto. |

### **6.1.2. Objetivo do projeto.**

Como destacado na visão do projeto, o objetivo da Esteira Inteligente é ser um sistema automatizado de transporte de peças para ambientes industriais. O intuito é desenvolver e implementar um sistema que ofereça assertividade no transporte de peças, evitando erros de quantidade ou a presença de peças inesperadas. Isso será realizado por meio de verificação via software e aferição de massa. 

Com isso, deseja-se alcançar os seguintes pontos:

- Evitar erros do operador durante o transporte das peças e montagem dos kits;

- Prevenir falhas devido à falta de informação no processo de atualização de projeto;

- Reduzir custos a longo prazo decorrentes de falhas no transporte de peças e montagem de peças;

- Reduzir o estoque a médio prazo, eliminando a necessidade de guias para verificação das peças no formato manual;

- Prevenir erros na automação por meio da checagem visual no final do processo.

Para atingir o objetivo geral são definidos os  objetivos específicos que são:

- Desenvolver e implementar um protótipo da Esteira Inteligente que utilize tecnologias de verificação por meio de imagem associado a inteligência artificial e a aferição por meio da massa;

- Assegurar que todos os kits sejam montados corretamente, por meio de duas etapas de checagem durante o ciclo;

- Otimizar espaço de estoque através do armazenamento de dados relacionados os kits de forma digital;

- Garantir a geração de relatórios para o Gestor de Operação e para a Equipe de Engenharia e Qualidade do usuário da Esteira Inteligente.


### **6.1.3. Stakeholders**

A ideia da Esteira Inteligente é contemplar indústrias de pequeno e médio porte. Segundo o BNDES, as pequenas indústrias tem faturamento anual de até R$4,8 milhões por ano, enquanto às de médio porte possuem um faturamento atual de até R$300 milhões  por ano. Com relação a quantidade de funcionários, de acordo com o IBGE, indústrias de pequeno porte empregam de 20 a 99 pessoas já as empresas de médio porte empregam de 100 a 499 pessoas na indústria, esta que é considerada indústria de médio porte.

Dentro das industrias descritas a Esteira Inteligente é de interesse dos seguintes cargos:

- Equipe de Engenharia: O projeto visa facilitar as alterações necessárias de redução de custo ou alteração de fornecimento de componentes dos subprodutos.  
  
- Equipe de Qualidade: O projeto visa facilitar a verificação da equipe de qualidade durante as alterações dos kits de montagem.

- Operadores de Máquina: O projeto visa reduzir o desconforto do operador ao realizar a atividade de montagem de kits, por meio de um sistema semiautomático de verificação.

Além disso, considera-se os participantes da disciplina de Projeto Integrador da Universidade de Brasília:

- Docentes da disciplina de PI2: O projeto visa além de gerar um protótipo de uso indutrial contemplar as necessidades definidas pelos docentes da disciplina de Projeto Integrador de Engenharia (FGA0250), da Faculdade do Gama - Universidade de Brasília(FGA/UnB), no primeiro semestre de 2024.

- Discentes Integrantes do Grupo 02 - Esteira Inteligente: O projeto visa auxiliar os discentes integrantes do grupo 02 a ter uma visão ampla de concepção, planejamento, desenvolvimento e confecção de um projeto de engenharia.  

### **6.1.4. Riscos**

A compreensão dos riscos que serão enfrentados é de suma importância para a condução do projeto de forma adequada olhando desde os pontos relacionadas a gestão deste projeto até os pontos relacionados a qualidade. O intuito de realizar o gerenciamento de riscos é trazer a redução de eventos indesejado e proporcionar a mitigação destes problemas. Para isso serão levados em consideração os seguintes pontos.

  a. Riscos Orçamentários e Éticos
  b. Riscos de Alterações de Requisitos
  C. Riscos organizacionais relacionados à Recursos Humanos e Capacidade Técnica 

Para facilitar a compreensão dos riscos esses serão dispostos de acordo com sua área de impacto.

**<h6 align="center"> Figura 6.4.1: EAR do projeto </h6>**

![](../assets/images/EAR.png)

<p style="text-align:center">Fonte: Autoria própria.</p>




<h6 style="text-align:center">Tabela 6.4.1:Descrição EAR</h6>

| Classificação | Riscos relacionados a|
| :-------: | :------: |
|   Comunicação   |Comunicação entre membros|
|   Planejamento   |Planejamento e entregas|
|   Recursos Humanos   |Competência técnica|
|   Recursos financeiros   |Investimento disponível|
|   Requisitos   |Definição do produto|
|   Tecnologia   |Recursos tecnológicos e disponibilidade|
|   Qualidade   |Cumprimento das necessidades dos usuários|
|   Fornecedores   |Disponibilidade e cumprimento dos prazos de entrega|
|   Saúde   | saúde dos membros da equipe|

<p style="text-align:center">Fonte: Autoria própria.</p>

Para uma gestão eficaz dos riscos, é crucial identificar, avaliar e priorizar os riscos utilizando a metodologia de classificação baseado no impacto e na probabilidade.

<h6 style="text-align:center">Tabela 6.4.2: Peso do Impacto </h6>

| Probabilidade | Descrição | Peso |
| :-------: | :------: | :------: | 
|   Nula  | Nulo | 0 |
|   Muito baixa | Pouco Expressivo  | 1 |
|   Baixa   | Pouco Impacto  | 2 |
|   Média   | Médio Impacto | 3 |
|   Alta   | Grande Impacto | 4 |
|   Muito Alta  | Impossibilita a continuação do Projeto | 5 |


<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 style="text-align:center">Tabela 6.4.3: Peso da Probabilidade </h6>

| Probabilidade | Peso |
| :-------: | :------: | 
|   Nula  | 0 |
|   Muito baixa  | 1 |
|   Baixa   | 2 |
|   Média   | 3 |
|   Alta   | 4 |
|   Muito Alta   | 5 |


<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 style="text-align:center">Tabela 6.4.4: Grau de Risco </h6>

| Impacto X Probabilidade | Nulo | Muito Baixa | Baixa | Média | Alta | Muito Alta |
| :-------: | :------: | :------: | :------: | :------: | :------: | :------: | 
|   Nulo  | 0 |  0 |  0 |  0 | 0  | 0  |
|   Muito baixa  | 0 |  1  | 2  |  3 | 4  | 5  |
|   Baixa   | 0 |  2 | 4  | 6  |  8 | 10  |
|   Média   | 0 |  3 |  6 |  9 | 12  | 15  |
|   Alta   | 0 | 4  |  8 | 12  | 16  |  20 |
|   Muito Alta   | 0 |  5 | 10  | 15  |  20 | 25  |


<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 style="text-align:center">Tabela 6.4.5: Classificação do Grau de Risco </h6>

| Grau de Risco | Classificação |
| :-------: | :------: | 
|   0  | Nulo |
|   0 < Risco <= 5  | Baixo |
|   5 < Risco < 15   | Médio |
|  15 <= Risco | Elevado |

<p style="text-align:center">Fonte: Autoria própria.</p>


<h6 style="text-align:center">Tabela 6.4.6: Plano de Gerenciamento de Risco </h6>

| Risco | Consequência | Probabilidade | Impacto | Grau do Risco | Prevenir | Mitigação |
| :-------: | :------: | :------: | :------: | :------: | :------: | :------: | 
|   Desistência da disciplina  | Sobrecarga dos membros |  1 |  5 |  5 | Apoiar o time com um ambiente propício para um bom trabalho  |  Replanejar atividades com esforço proporcional |
|   Não utilização dos meios de comunicação  | Problemas com acompanhamento, alinhamento de atividades, tomadas de decisão |  2  | 3  | 6 | Utilizar plataforma de conhecimento geral, reunião de acompanhamento semanal entre a equipe  | Capacitar o time à respeito das plataformas utilizadas.  |
|   Requisitos que não geram atividades claras   | Desperdício de esforço, sobrecarga com atividades muitas vezes não necessárias |  3 | 4  | 12  |  Especificação dos requisitos com todo o time para todos entenderem o que precisam fazer | Construção das tarefas a partir do kanban para facilitar o entendimento. |
|   Alteração do escopo   | Retrabalho e sobrecarga par todo o time |  2 |  5 |  10 | Replanejar cronograma de atividades  |  Envolver todos os membros da equipe nesse processo. |
|   Baixo orçamento   | Impossibilidade de atender os requisitos | 2  |  5 | 10  | Reduzir o escopo e a quantidade de material estrutural | Planejamento financeiro |
|   Incompatibilidade na integração    | Perda de componentes gerando mais custo e retrabalho | 2 |  5 | 10  | Comunicação clara entre os relacionados com o processo de integração fazendo a validação dos esquemáticos de forma correta  |  Investigar o que ocasionou a incompatibilidade |
|   Falhas na Inteligência artificial    | Retrabalho e risco de não validação da quantidade de peças  | 3 |  5 | 15  | Iniciar o treinamento da IA com antecedência necessária  | Apoio de todo o time para evitar que tais problemas ocorram.  |
|   Atraso dos fornecedores    | Atraso na construção do protótipo  | 3 |  5 | 15  | Investigação de fornecedores que facilitem a entrega  | Replanejamento de cronograma e antecipação da compra de material.  |
|   Ausência de membro por questões de saúde    | Redistribuição das tarefas  | 2 |  5 | 10  | -  | Redistribuir as atividades necessárias.  |
|   Falta de habilidade técnica    | Dificuldade na realização da atividade  | 2 |  5 | 10  | Estabelecer um escopo que esteja dentro do conhecimento de ao menos um membro da equipe  | Promover treinamentos para trabalhar a habilidade.  |


<p style="text-align:center">Fonte: Autoria própria.</p>


## **6.2. Lista É / Não É**

### **6.2.1. É**
- Um equipamento destinado ao uso industrial;
- Um sistema para o transporte de peças;
- Um sistema que verifica a precisão dos kits em relação Às peças;
- Um sistema de esteiras. 

### **6.2.2. Não é**
- Um equipamento para uso doméstico;
- Um sistema de classificação ou separação de peças;
- Um dispositivo de armazenamento.

## **6.3. Organização da equipe**

**<h6 align="center"> Figura 3.1: Organograma da Equipe</h6>**

![](../assets/images/Organograma.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>


**<h6 align="center"> Tabela 6.3.1: Composição da Equipe</h6>**

|Nome|Matrícula|Curso|Atribuições|
|:----:|:----:|:----:|:----:|
|Ana Paula de Souza de Araujo|190024577|Engenharia Aeroespacial|Diretora de Estruturas|
|Caio Sampaio de Carvalho e Silva|190085321|Engenharia de Energia|Desenvolvedor|
|Danilo Domingo Vitoriano Silva|180015311|Engenharia de Software|Diretor de Software|
|Fernanda Noronha Coelho dos Santos Marques|190027762|Engenharia Aeroespacial|Diretora de Qualidade|
|Heitor Marques Simões Barbosa|202016462|Engenharia de Software|Desenvolvedor|
|Jefferson França Santos|180102761|Engenharia de Software|Desenvolvedor|
|Joel Jefferson Fernandes Moreira|190043083|Engenharia Eletrônica|Coordenador Geral|
|Lucas Alexandre Gama Correia|180149741|Engenharia Aeroespacial|Desenvolvedor|
|Matheus Henrick Dutra dos Santos|190018101|Engenharia de Software|Desenvolvedor|
|Matheus Phillipo Silverio Silva|150154348|Engenharia de Software|Desenvolvedor|
|Mauricio Maiyson Hoffman de Melo|160138574|Engenharia Eletrônica|Diretor de Elétrica/Eletrônica|
|Michael Emanuel Silva Costa|190035528|Engenharia de Energia|Desenvolvedor|

<p style="text-align:center">Fonte: Autoria própria.</p>

## **6.4. Repositórios**

A equipe utilizou o gitlab como repositório para o projeto.

## **6.5. EAP (Estrutura Analítica de Projeto) Geral do Projeto**

Segundo o Guia PMBOK, pode-se definir que a EAP é uma decomposição hierárquica orientada à entrega do trabalho (<a href="bib.md">PMI,2008</a>). Sua meta é alcançar os objetivos do projeto, permitindo a visualização dos entregáveis de cada um dos seus níveis.

### **6.5.1. EAP da Gerência**

**<h6 align="center"> Figura 5.1.1: EAP da Gerência</h6>**

![](../assets/images/EAP_gerencia.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

### **6.5.2. EAP de Elétrica/Eletrônica**

**<h6 align="center"> Figura 5.2.1: EAP de Elétrica/Eletrônica</h6>**
![](../assets/images/EAP_Eletônica.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

### **6.5.3. EAP de Estruturas**

**<h6 align="center"> Figura 5.3.1: EAP de Estruturas</h6>**

![](../assets/images/EAP_Estruturas.jpeg)

<p style="text-align:center">Fonte: Autoria própria.</p>

### **6.5.4. EAP de Software**

**<h6 align="center"> Figura 5.4.1: EAP de Software</h6>**

![](../assets/images/EAP_Software.jpeg)

<p style="text-align:center">Fonte: Autoria própria.</p>

## **6.6. Definição de atividades e backlog das sprints**

Definir as atividades gerais que compõem o projeto e elaborar o
cronograma básico de execução. Além disso, identificar os responsáveis
por cada atividade é crucial para garantir uma melhor organização e planejamento das ações ao longo do tempo.

**<h6 align="center"> Tabela 6.6.1: Definição de atividades e backlog das sprints</h6>**

| Atividade | Início | Final | Responsáveis |
|:---------:|:---------:|:---------:|:-----------:|
|     Definição de Temas e Grupo     |    22/03/2024     |    01/04/2024      |   Todos   |
|     Definição de escopo    |    01/04/2024     |    10/04/2024      |   Todos   |
|     Benchmarking e normas técnicas     |    10/04/2024     |    15/04/2024      |   Todos   |
|     Elicitação de Requisitos     |    10/04/2024     |    17/04/2024      |   Todos   |
|    Idealização do Subsistema de Estruturas     |    10/04/2024     |    17/04/2024      |   Estruturas   |
|    Idealização do Subsistema de Elétrica/Eletrônica     |    10/04/2024     |    17/04/2024      |   Elétrica/Eletrônica   |
|    Idealização do Subsistema de Software     |    10/04/2024     |    17/04/2024      |   Software   |
|    Construção dos CAD's     |    12/04/2024     |    27/04/2024      |   Estruturas   |
|    Reunião de Validação    |    17/04/2024     |    17/04/2024      |   Todos   |
|    Materiais do projeto    |    18/04/2024     |    24/04/2024      |   Estruturas   |
|    Definição de componentes   |    18/04/2024     |    24/04/2024      |   Elétrica/Eletrônica   |
|    Diagramas e Esquemáticos   |    18/04/2024     |    24/04/2024      |   Software   |
|    Esquemáticos e Diagramas de Dados    |    24/04/2024     |    01/05/2024      |   Elétrica/Eletrônica   |
|    Projeto do Subsistema de Estruturas     |    24/04/2024     |    02/05/2024      |   Estruturas   |
|    Projeto do Subsistema de Elétrica/Eletrônica     |    24/04/2024     |    02/05/2024      |   Elétrica/Eletrônica   |
|    Projeto do Subsistema de Software     |    24/04/2024     |    02/05/2024      |   Software   |
|    Integração dos CADS dos componentes     |    24/04/2024     |    27/04/2024      |   Estruturas   |
|    Integração dos CADS dos subsistemas     |    27/04/2024     |    29/04/2024      |   Estruturas   |
|    Renderização e desenhos técnicos     |    29/04/2024     |    02/05/2024      |   Estruturas   |
|     Elaboração Relatório do Ponto de Controle 1     |    27/04/2024     |    04/05/2024      |   Todos   |
|     Entrega Relatório do Ponto de Controle 1     |    05/05/2024     |    05/05/2024      |   Coordenador Geral   |
|     Elaboração da Apresentação do PC1     |    05/05/2024     |    09/05/2024      |   Todos   |
|     Apresentação do PC1     |    10/05/2024     |    10/05/2024      |   Todos   |
|     Ajustes Subsistema Estruturas     |    10/05/2024     |    17/05/2024      |   Estruturas   |
|     Ajustes Subsistema Elétrica/Eletrônica     |    10/05/2024     |    17/05/2024      |   Elétrica/Eletrônica   |
|     Ajustes Subsistema Software     |    10/05/2024     |    17/05/2024      |   Software  |
|     Construção Subsistema Estruturas     |    18/05/2024     |    30/05/2024      |   Estruturas   |
|     Construção Subsistema Elétrica/Eletrônica     |    18/05/2024     |    30/05/2024      |   Elétrica/Eletrônica   |
|     Construção Subsistema Software     |    18/05/2024     |    30/05/2024      |   Software  |
|     Elaboração Relatório do Ponto de Controle 2     |    30/05/2024     |    06/06/2024      |   Todos   |
|     Entrega Relatório do Ponto de Controle 2     |    06/06/2024     |    06/06/2024      |   Coordenador Geral   |
|     Elaboração da Apresentação do PC2     |    06/06/2024     |    11/06/2024      |   Todos   |
|     Apresentação do PC2     |    12/06/2024     |    12/06/2024      |   Todos   |
|     Integração Subsistemas     |    14/06/2024     |    26/06/2024      |   Todos   |
|     Montagem do projeto     |    26/06/2024     |    28/06/2024      |   Todos  |
|     Elaboração Banner e marketing     |    28/06/2024     |    01/07/2024      |   Todos  |
|     Elaboração da Apresentação do PC3     |    29/06/2024     |    02/07/2024      |   Todos   |
|     Apresentação do PC3    |    03/07/2024     |    03/07/2024      |   Todos   |
|     Possíveis correções    |    04/07/2024     |    09/07/2024      |   Todos   |
|     Apresentação de projetos na FIT/FGA   |    10/07/2024     |    10/07/2024      |   Todos   |

<p style="text-align:center">Fonte: Autoria própria.</p>

## **6.7. Orçamento Geral**
**<h6 align="center"> Tabela 7.7.1: Orçamento Geral</h6>**

| Item  N° |            Componente          | Quantidade |      Valor (Em reais) |
|:-------------------------:|:---:|:--------------:|:-----------:|
|     1      |         Acoplador         |  3  |       1,00       |
|     2      |           Mancal          |  18 |     2,00   |
|     3      |       Suporte motor       |  3  |       1,00      |
|     4      |      Prato de Medição     |  1  |       55,00      |
|     5      |          Borracha         |  2  |      79,00    |
|     6      |          Correia          |  1  |       25,00      |
|     7      |         Rolamento         |  16 |        2,00    |
|     8      |          Cano 50          |  1  |      20,00      |
|      9     |           CAP 50          |  18 |       5,00     |
|     10      |           Polia           |  6  |     20,00    |
|     11      |       Conector funil      |  4  |       1,00    |
|    12       |            Mesa           |  1  |       80,00     |
|    13       |   Barra de estabilização  |  3  |      13,00     |
|    14       |       Chapa inferior      |  2  |       13,00         |
|    15       |           Joelho          |  1  |       13,00      |
|     16      |  Barra suporte com motor  |  3  |        13,00     |
|     17      |  Barra suporte sem motor  |  3  |        13,00     |
|     18      |      Canto de ajuste      |  2  |        3,00           |
|     19      |           Funil           |  1  |       25,00     |
|     20      |        Suporte em V       |  2  |         13,00       |
|     21      |      Barra de ângulo      |  2  |      13,00       |
|     22      | Parafuso soberbo 3,5X60mm |  54 |             1,00       |  
|     23      | Parafuso soberbo 3,5X30mm |  11 |         1,00       | 
|     24      |      Parafuso M3X20mm     |  12 |        1,00       |   
|     25      |          Porca M3         |  12 |        1,00       |
|       26    |      Parafuso M4X80mm     |  3  |         2,00       |
|    27       |          Porca M4         |  3  |        1,00       |   
|    28       |      Porca borboleta      |  3  |        2,00       |
|    29       |      Suporte da Câmera     |        2        |    34,20     |   
|    30       |      Suporte da Tela       |        1        |    69,99     | 
|31                        |Orange pi PC                                     |1         |277,00          |
|32                        |Orange PI Zero W                                 |1         |172,00          |
|33                        |Arduino Uno                                      |2         |74,02           |
|34                        |CNC Shield V3                                    |1         |36,01           |
|35                        |Driver A4988                                     |3         |49,92           |
|36                        |KNUP KP-TV14                                     |1         |450,00          |
|37                        |Conversor AC/DC + Célula de carga                |1         |32,90           |
|38                        |Motor Nema17                                     |3         |182,00          |
|39                        |Módulo LDR                                       |3         |13,90           |
|40                       |DPS 20A iClamper Pocket                          |1         |49,99           |
|41                       |Step Down LM2596                                 |1         |22,74           |
|42                       |Fonte Seasonic 300W                              |1         |110,00          |
|43                       |Módulo de comutação Automática Para Bateria yx850|1         |26,00           |
|44                       |Bateria Lítio 12v 9.6 AH                         |1         |375,00          |
|45                       |Chave liga/Desliga Verde                         |1         |11,04           |
|46                       |Push button Vermelho                             |1         |11,98           |
|47                       |Webcam 12mp                                      |2         |100,00          |
|48                       |Teclado E Mouse                                  |1         |23,00           |
|   Quantidade Total |       221       |      Valor total     |    3.046,89    |

<p style="text-align:center">Fonte: Autoria própria.</p>