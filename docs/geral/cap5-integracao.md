# 5. **Integração de subsistemas e finalização do produto**

A integração de subsistemas e a finalização do produto são etapas críticas que garantem que todos os componentes desenvolvidos funcionem como um sistema coeso. Esta seção descreve as etapas necessárias para combinar os diferentes subsistemas em uma única solução funcional e assegurar que o produto final atenda aos requisitos estabelecidos. 


Nesta fase, os subsistemas desenvolvidos separadamente (elétrica e eletrônica,estrutura e software) são integrados. Isso envolve:

- Interligação Eletrônica: Conexão dos circuitos e dispositivos eletrônicos para assegurar a comunicação e operação conjunta.Isso pode ser visualizado na [**Seção de integração de elétrica e eletrônica**](../eletronica-energia/elet-cap5-integracao.md).

- Conexão Física: Montagem física dos componentes estruturais e eletrônicos, garantindo que todas as partes se encaixem corretamente. Esta etapa é detalhada na [**Seção de integração de estruturas**](../estruturas/est-cap4-projeto-e-construcao.md).

- Sincronização de Software: Instalação e configuração do software nos sistemas eletrônicos, garantindo que o software possa controlar e monitorar os componentes físicos adequadamente. Esta parte é explicada na [**Seção de integração de software**](../software/soft-cap5-integracao.md).


