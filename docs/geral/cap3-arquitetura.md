# 3. **Arquitetura geral da solução**

<span style="text-align: justify">
A arquitetura geral pode ser percebida mediante a definição dos requisitos gerais, funcionais e não funcionais delineados neste documento. Nesse sentido, a arquitetura geral será configurada por um sistema de esteiras, em que cada esteira será designada para receber um tipo específico de peça. O sistema deverá ser controlável de maneira que apenas uma peça seja manipulada por vez. Adicionalmente, será implementado um duplo sistema de verificação, consistindo em uma análise de peso e uma verificação por imagem, esta última realizada por meio de uma inteligência artificial.

O processo em sua totalidade será gerenciado por meio de um software dedicado, o qual será responsável pelo cadastro das peças e dos operadores autorizados a operar o protótipo. Além disso, a arquitetura pode ser visualizada de forma abrangente nos tópicos:
</span>

3.1. [**Arquitetura Eletrônica/Energia**](../eletronica-energia/elet-cap3-arquitetura.md);

3.2. [**Arquitetura Estruturas**](../estruturas/est-cap3-arquitetura.md);

3.3. [**Arquitetura Software**](../software/soft-cap3-arquitetura.md);




