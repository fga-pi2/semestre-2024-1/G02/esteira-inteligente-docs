# **1. Introdução** {#introducao}

Durante toda a disciplina de Projeto Integrador 2, no primeiro semestre de 2024, será desenvolvido um protótipo funcional da Esteira Inteligente.

## **1.1. O que é a Esteira Inteligente** {#o-que-e-a-esteira-inteligente}

A Esteira Inteligente é um equipamento industrial projetado para facilitar a modificação de conjuntos de produtos, denominados kits. Alguns desses kits podem apresentar diferenças sutis entre as peças, o que pode passar despercebido por gestores de produção e operadores. O propósito do produto é solucionar esse tipo de problemas.

Através de um sistema de esteiras, a Esteira Inteligente transporta peças previamente determinadas pela equipe de Engenharia e Qualidade, utilizando um sistema de seleção integrado em rede local. Durante o funcionamento, a máquina verifica as peças dos conjuntos por meio de um sistema de câmeras e aferição de massa.

É importante destacar que a Esteira Inteligente não se destina ao uso doméstico. Trata-se de um sistema de separação de peças, embora contenha divisórias, sendo necessário o fornecimento de alimentação de forma contínua para o seu funcionamento.

## **1.2. Detalhamento do problema** {#detalhamento-do-problema}

Ao longo da linha de produção, é comum ocorrerem problemas de montagem. Um dos desafios enfrentados por indústrias com uma vasta gama de produtos são as pequenas alterações nos projetos, que para as equipes de Qualidade e Engenharia são extremamente relevantes, podendo afetar a experiência do usuário ou modificar integralmente as estruturas físicas dos produtos.

Entretanto, essas modificações podem passar despercebidas por operadores e gestores de produção, em virtude do grande volume de variedades. Nesse contexto, a Esteira Inteligente surge como uma solução eficaz, agilizando a alteração ou criação de novos produtos, prevenindo lacunas no estoque e aumentando o valor rotativo do produto, especialmente quando uma simples modificação de um componente pode resulta em uma alteração substancial do produto.


## **1.3. Levantamento de normas técnicas**{#levantamento-de-normas-tecnicas-relacionadas-ao-problema}

O levantamento das normas considerou o que é o produto e o que ele pretende solucionar. As normas escolhidas para o projeto estão descritas nesta sessão, com um breve texto explicativo.

Vale ressaltar que qualquer alteração deste trecho só será feita com base em versionamento formal.

### **1.3.1. NR-12 - Segurança no trabalho em máquinas e equipamentos**

A NR-12 regulamenta a segurança no trabalho de máquinas e equipamentos, contendo pontos relevantes para a concepção deste projeto. A Esteira Inteligente pode ser classificada como uma transportadora de materiais e, a partir do item 12.8,são observados detalhes que especificam seu uso. 

De acordo com o item 12.8.6, durante a movimentação da esteira é proibida a circulação de pessoas. Nesse caso, é imperativo que o empregador informe e sinalize devidamente a área ocupada pela Esteira Inteligente. 

Conforme o item 12.8.7 da mesma norma, para a operação contínua de uma esteira, o operador deve ter acesso a um botão de emergência sinalizado, a fim de interromper a esteira em caso de necessidade. Para cumprir essa normativa, haverá um botão de fácil acesso na parte lateral da esteira.

O item 12.9 faz referência à norma NR-17, relacionada à ergonomia do operador, fator de extrema importância para a futura aplicação deste protótipo. Para a construção da Esteira foram consideradas as sugestões de altura definidas pela OMS, de maneira a possibilitar que o operador trabalhe em pé, com a altura adaptada para uma boa postura.

### **1.3.2. NR-26 - Sinalização de segurança**

Para garantir a segurança do operador da Esteira Inteligente, a NR-26 foi considerada, assegurando que o ambiente de instalação esteja devidamente sinalizado e bem iluminado, conforme os padrões de segurança estabelecidos.

### **1.3.3. ABNT NBR 5410 - Segurança em instalações elétricas de baixa tensão**

O projeto utilizará a NBR5410 para todas as instalações elétricas e eletrônicas. Em particular, será cumprido o que está descrito no item 3.2, relacionado à proteção contra choques elétricos.

### **1.3.4. ABNT NBR 9050 - Acessibilidade a edificações, mobiliário, espaços e equipamentos urbanos**
O projeto utiliza a NBR9050 para definir altura e largura do equipamento visando acessibilidade de todos os funcionários. 

### **1.3.5. NR-17 - Ergonomia**
Para garantir a melhor ergonomia para o trabalhador, em simultâneo com a NBR 9050, utilizamos a NR 17 como referência para as medidas do produto, proporcionando uma ergonomia ideal para o operador.


## **1.4. Identificação de soluções comerciais** {#identificacao-de-solucoes-comerciais}

A Esteira Inteligente é composta não apenas por sua estrutura, mas também por um software de verificação de montagem de kits. Não foi possível encontrar nenhuma solução semelhante à proposta. As soluções encontradas consistem em sistemas de esteiras para transporte de materiais, mas sem sistema de validação de montagem. Portanto, o produto proposto pode ser considerado inovador. Na Tabela 1.4.1, apresenta-se uma comparação com duas soluções de transporte de materiais encontradas.

**<h6 align="center"> Tabela 1.4.1: Comparação de soluções comerciais com a Esteira Inteligente </h6>**

|   | <a href="https://conduire.com.br/transportadores/transportadores-de-transferencia/">Solução da Conduire<a> | <a href="https://www.macsulferramentas.com.br/esteira-transportadora-horizontal-industrial">Solução Macsulferramentas | Solução Esteira Inteligente |  
|:-:|:----------:|:----------:|:-:|
| Transporta Materiais| Atende | Atende | Atende  |
| Tem um Sistema de Integração Com o Estoque | Não Atende | Não Atende |  Atende |
| Permite montagem de gama de kits            | Não Atende | Atende |  Atende |
| Possui um sistema de verificação visual por IA | Não Atende | Não Atende |  Atende | 

<p style="text-align:center">Fonte: Autoria própria.</p>


## **1.5. Objetivos gerais do projeto** {#objetivo-geral-do-projeto}

O projeto Esteira Inteligente é um sistema automatizado de transporte de peças para ambiente industrial, cujo objetivo é desenvolver e implementar um sistema que ofereça assertividade no transporte de peças. Este sistema visa evitar erros de quantidade ou a ocorrência de peças diferentes das esperadas, utilizando verificação via software e aferição de massa. Com isso, deseja-se:

- Evitar erros do operador durante o transporte das peças e montagem dos kits;

- Prevenir falhas devido à falta de informação no processo de atualização de projeto;

- Reduzir custos a longo prazo decorrentes de falhas no transporte de peças e montagem de peças;

- Reduzir o estoque a médio prazo, eliminando a necessidade de guias para verificação das peças no formato manual;

- Prevenir erros na automação por meio da checagem visual no final do processo.

## **1.6. Objetivos específicos do projeto** {#objetivos-especificos-do-projeto}

Para cumprir o objetivo geral elencam-se os seguintes objetivos específicos:

- Desenvolver e implementar um protótipo da Esteira Inteligente que utilize tecnologias de verificação por meio de imagem associado a inteligência artificial e a aferição por meio da massa;

- Assegurar que todos os kits sejam montados corretamente, por meio de duas etapas de checagem durante o ciclo;

- Otimizar espaço de estoque através do armazenamento de dados relacionados os kits de forma digital;

- Garantir a geração de relatórios para o Gestor de Operação e para a Equipe de Engenharia e Qualidade do usuário da Esteira Inteligente.
