# **8. Manual de Montagem e Uso do Produto**

O manual de montagem e uso preliminar da Esteira Inteligente está descrito abaixo.

## **8.1. O que está contido no conjunto da Esteira Inteligente?**

8.1.1. Tela

8.1.2. Funil

8.1.3. Câmera

8.1.4. Bateria

8.1.5. Esteiras

8.1.6. Microcomputador

8.1.7. Conjunto célula de carga

8.1.8. Estrutura de Suporte da Esteira

## **8.2. Como montar a Esteira Inteligente?**

**8.2.1. Verifique se possui todos os componentes necessários:**
   - Certifique-se de que todos os itens listados acima estão presentes na embalagem.

**8.2.2.Posicione a estrutura de suporte da Esteira com as esteiras em um local que não seja arriscado:**
   - Escolha um local estável e seguro para montar a estrutura da esteira;
   - Coloque a estrutura de suporte da esteira em uma superfície plana;
   - Encaixe as esteiras junto ao funil com o apoio da peça de impressão 3D;
   - Posicione a câmera em cima do conjunto célula de carga já posicionado na estrutura de suporte da esteira.

**8.2.3. Encaixe as baterias no local correto e conecte aos dispositivos:**
   - Localize o compartimento de bateria na estrutura da esteira.
   - Insira a bateria no compartimento e conecte os cabos de energia aos respectivos dispositivos;
   - Ligue a fonte na tomada.

## **8.3. Como utilizar a Esteira Inteligente?**

**8.3.1. Adicione as informações sobre os usuários da esteira inteligente:**
   - O acesso ao software deve ser configurado na rede local;
   - Com o acesso a rede qualquer computador com login e senha autorizados poderão utiliza a esteira;
   - Acesse o software;
   - Navegue até a seção de configuração de usuários;
   - Insira os dados necessários para cadastrar os usuários autorizados a operar a esteira.

**8.3.2. Cadastre as peças que serão transportadas:**
   - No software, vá até a seção de cadastro de peças;
   - Insira as informações detalhadas de cada peça que será transportada pela esteira, como nome, peso e dimensões, através do banco de dados.

**8.3.3. Cadastre as informações dos kits, levando em consideração as peças que serão transportadas e suas respectivas quantidades:**
   - Acesse a seção de configuração de kits no software;
   - Configure os kits especificando quais peças devem ser incluídas e em quais quantidades.

**8.3.4. Adicione as peças na esteira:**
   - Coloque uma peça em cada divisória da esteira;
   - Certifique-se de que as peças estão posicionadas corretamente para serem identificadas e processadas pelo sistema.

**8.3.5. Inicialize o uso para o início da montagem dos kits:**
   - No software, acione a opção de inicialização da esteira;
   - Acompanhe o processo de montagem dos kits conforme as configurações estabelecidas.