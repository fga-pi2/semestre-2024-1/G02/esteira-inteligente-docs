# **2.Concepção e detalhamento da solução**{#concepcao-e-detalhamento-da-solucao}


## **2.1. Requisitos gerais**{#requisitos-gerais}

**<h6 align="center"> Tabela 2.1.1: Requisitos funcionais para o produto </h6>**

| Requisito |           Descrição           |                                                                                                                                     Observações                                                                                                                                     |
| :-------: | :---------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|   RF01    |    Composição das esteiras    |                                                                                                     O sistema será composto por três esteiras que funcionam a partir de passos.                                                                                                     |
|   RF02    |   Quantidade de componentes   |                                                                                                                 O sistema deverá aceitar 3 componentes diferentes.                                                                                                                  |
|   RF03    |    Distribuição das peças     |                                                                                          A esteira será de tal forma a possuir divisórias que compõem cada uma das peças a serem triadas.                                                                                           |
|   RF04    |   Assertividade de despacho   |                                                             O sistema deve liberar apenas os componentes dos kits do kit escolhido. Além disso, o passo da esteira deve ser tal que caia apenas um componente por vez.                                                              |
|   RF05    |      Checagem por imagem      |                                                          As câmeras deverão ser posicionadas de tal forma que reconheça  se os componentes que estarão distribuídos sobre a célula de carga estão com a quantidade correta                                                          |
|   RF06    |        Dupla checagem         |                                                                                                  A célula de carga deverá checar se o peso das peças corresponde ao kit escolhido.                                                                                                  |
|   RF07    |     Aviso de verificação      |                                                                                Caso exista divergência da quantidade de peças nos processos de checagem o operador deverá ser comunicado pela tela.                                                                                 |
|   RF08    | Armazenamento dos componentes |                                                             Ao final do processo as peças deverão estar dispostas nas divisórias da célula de carga. Ao final do processo o operador receberá o aviso para ensacá-los.                                                              |
|   RF09    |  Cadastro de dados dos kits   | O sistema deve ter um cadastro único de cada kit, onde, este deve possuir quantos e quais componentes possui. Os componentes do kit devem conter as informações de: nome do componente, dimensões de tamanho, peso unitário, um id de 8 dígitos inteiros, kits em que são vinculados. |
|   RF10    |    Alimentação do sistema     |                                                                                          O sistema deve funcionar sendo alimentado por uma bateria com sistema semelhante ao <i>nobreak<i>                                                                                           |
|   RF11    |      Validação de acesso      |                                                                                 Deverá existir um sistema de cadastro de operadores, apenas operadores cadastrados poderão utilizar o equipamento.                                                                                  |

<p style="text-align:center">Fonte: Autoria própria.</p>

**<h6 align="center"> Tabela 2.1.2: Requisitos não funcionais para o produto </h6>**

| Requisito |           Descrição           |                                              Observações                                               |
| :-------: | :---------------------------: | :----------------------------------------------------------------------------------------------------: |
|   RNF01   |    Usabilidade do sistema     |      A usabilidade do sistema deve ser intuitiva de forma a não ser custoso o uso pelo operador.       |
|   RNF02   |     Ergonomia do sistema      | O design da estrutura deve ser tal qual facilite a interação entre o operador e a esteira inteligente. |
|   RNF03   |  Limitação de uso do sistema  |                    O uso do sistema deve ser restrita aos operadores responsáveis.                     |
|   RNF04   | Consumo energético do sistema |       O sistema energético deve ser eficiente e otimizado para evitar altos custos operacionais.       |
|   RNF05   |   Confiabilidade do sistema   |          A esteira inteligente deve ser o mais assertiva de forma a minimizar erros e falhas.          |

<p style="text-align:center">Fonte: Autoria própria.</p>


Um esboço dos requisitos foram decididos em uma reunião presencial, onde a equipe de software colaborou ativamente na definição e esboço das especificações. Posteriormente, esses requisitos foram organizados e detalhados utilizando a plataforma [Miro](https://miro.com/app/board/uXjVKQT0lBc=/), facilitando a visualização, discussão e acompanhamento por todos os membros envolvidos no projeto.


**<h6 align="center"> Tabela 2.4.1: Requisitos funcionais para o Software do produto</h6>**

|   ID   |             Descrição             |                                            Requisito                                            |
| :----: | :-------------------------------: | :---------------------------------------------------------------------------------------------: |
| RFSO01 |          Cadastrar kits           |                     O sistema deve permitir que os usuários cadastrem kits                      |  |
| RFSO02 |            Editar kits            |                     O sistema deve permitir que os usuários editem os kits                      |
| RFSO03 |            Listar kits            |                     O sistema deve permitir que os usuários listem os kits                      |
| RFSO04 |           Excluir kits            |                     O sistema deve permitir que os usuários excluam os kits                     |
| RFSO05 |        Cadastrar usuários         |                     O sistema deve permitir que usuários sejam cadastrados                      |
| RFSO06 |         Editar usuários          |                       O sistema deve permitir que usuários sejam editados                       |
| RFSO07 |         Listar usuários          |                       O sistema deve permitir que usuários sejam listados                       |
| RFSO08 |         Excluir usuários         |                      O sistema deve permitir que usuários sejam excluídos                       |
| RFSO09 |       Cadastrar componentes       |                 O sistema deve permitir que os usuários cadastre os componentes                 |
| RFSO10 |        Editar componentes         |                  O sistema deve permitir que os usuários editem os componentes                  |
| RFSO11 |        Listar componentes         |                  O sistema deve permitir que os usuários listem os componentes                  |
| RFSO12 |        Excluir componentes        |                  O sistema deve permitir que os usuários exclua os componentes                  |
| RFSO13 |     Reconhecimento de imagens     |            O sistema deve ser capaz de reconhecer as imagens dos elementos dos kits             |
| RFSO14 |      Tratar dados da balança      |  O sistema deve ser capaz de comparar os dados da balança e das imagens dos componentes do kit  |
| RFSO15 |   Gerenciar controle de estoque   |                    O sistema deve ser capaz de realizar controle de estoque                     |
| RFSO16 |  Listagem de componentes usados   | O sistema deve ser capaz de informar quantos e quais componentes, ao concluir a montagem do kit |
| RFSO17 | Avaliar qualidade dos componentes |             O sistema deve ser capaz de avaliar a qualidade dos componentes do kit              |
| RFS018 |   Selecionar kit para produção    |               O sistema deve permitir que o usuários selecione kits para produção               |
| RFS019 |     Autenticação de usuário     |     O sistema deve permitir que o usuários se autentique no sistema com o seu devido papel      |

<p style="text-align:center">Fonte: Autoria própria.</p>


**<h6 align="center"> Tabela 2.4.2: Requisitos não funcionais para o Software do produto</h6>**

|   ID    |                      Descrição                      |                                                               Requisito                                                               |
| :-----: | :-------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------: |
| RNFSO01 |                  Papel de usuário                   |                                 Os usuários do sistemas serão separados por administrador e operador                                  |
| RNFSO02 |        Tempo de reconhecimento de componente        |                                A detecção e reconhecimento de componentes devem ocorrer em tempo real                                 |
| RNFSO03 |         Poder lidar com múltiplas demandas          |                         O sistema deve ser capaz de lidar com múltiplas solicitações de CRUD simultaneamente                          |
| RNFSO04 | Lidar com múltiplas requisições de origens diversas | O sistema deve ser capaz de suportar a adição de novos tipos de componentes e atualizações  sem interromper as operações em andamento |
| RNFSO05 |             Antecipar possíveis falhas              |                          O sistema deve ser robusto o suficiente para lidar co possíveis falhas de hardware                           |
| RNFSO06 |              Usabilidade na interface               |                                       A interface do usuário deve ser intuitiva e fácil de usar                                       |
| RNFSO07 |                  Mensagens claras                   |                                           Mensagens de erro devem ser claras e informativas                                           |
| RNFSO08 |                   Logs do sistema                   |                  Deve ser implementado um sistema de registro de logs abrangente para rastrear atividades do sistema                  |
| RNFSO09 |                Tecnologia do backend                |                                              O backend sera desenvolvido em Python/Django                                              |
| RNFSO10 |              Tecnologia do Frontend S               |                                            O frontend sera desenvolvido em typescript/React                                            |
| RNFSO11 |             Plataforma que deve operar              |             O sistema deve ser uma aplicação web acessível através de navegadores web padrão, como Chrome, Firefox e etc.             |
| RNFSO12 |            Tecnologia do banco de dados             |                                              O banco de dados sera desenvolvido em MySQL                                              |
<p style="text-align:center">Fonte: Autoria própria.</p>

O histórico de versão do requisitos de software pode ser encontrado no [repositório de documentação de software](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-software/-/blob/main/docs/requisitos_software.md?ref_type=heads).