# 4. **Projeto e construção de subsistemas da solução proposta**

O projeto e construção de subsistemas da solução estão organizados em seções separadas de acordo com o seu respectivo subsistema. Essa abordagem segmentada permite uma visualização mais eficiente e clara dos diferentes subsistemas, facilitando a gestão e execução do trabalho. Os subsistemas podem ser observados nos tópicos abaixo. 

Durante toda a confecção do trabalho foram utilizadas matérias primas comerciais, na tabela 4.1 segue as informações orçamentárias que dizem respeito ao projeto como todo. Nesta tabela também estará descrito quais itens foram cedidos.

**<h6 align="center"> Tabela 4.1: Orçamento Total</h6>**

| Item  N° |            Componente          | Quantidade |      Valor Estimado (Em reais) | Valor Gasto (Em reais)|
|:-------------------------:|:---:|:--------------:|:-----------:|:-----------:|
|     1      |         Rolo de filamento ABS 1Kg        | 1  |      78,90     | 66,00 |
|     4      |      Chapa de ACM 1,5X5m     |  1  |      642,00      | Cedido |
|     5      |          Borracha         |  3  |      159,80    | 159,80 |
|     7      |         Rolamento         |  16 |       104,55    | 104,55 |
|     8      |          Cano 40          |  1  |      47,90     | 47,90  |
|      9     |           CAP 40          |  18 |       129,04     | 129,04 |
| 10          | Barra de Metalon 6m | 3 | 150,00 | 131,00 |
|    11      |       Chapa inferior      |  2  |       13,00      | Cedido | 
|  12 | Porcas, parafusos e arruelas somados | 100 | 200,00 | 244,7 |
| 13 | Chapa de MDF CRU 9mm | 1 | 123,00 | Cedido |
| 14 | Tinta Spray | 6 | 16,50 | 99,00 |
| 15 | Lixas | 3 | 2,00 | 6,00 |
| 16 |      Suporte da Câmera     |        2        |    34,20     |  Cedido |
| 17 | Adaptadores HDMI (mini, micro) - HDMI | 1 | 60,00 | 60,00 |
| 18                       |RaspiBerry Pi 4 1GB                            |1         |477,00          | Cedido |
| 19                       |Arduino Uno                                    |2         |74,02           | 74,02 | 
| 20                       |CNC Shield V3                                  |1         |36,01           | 36,01 |
| 21                       |Driver A4988                                   |3         |49,92           | 150,50 |
| 22                      |Conversor AC/DC + Célula de carga              |1         |32,90           | 50,00 |
| 23                       |Motor Nema17                                   |3         |182,00          | 182,00 |
| 24                      |DPS 20A iClamper Pocket                          |1         |49,99           | 49,99 |
| 25                      |Step Down LM2596                                 |1         |22,74           | 22,74
| 26                      |Fonte - 12V 3A                      |1         |15,00          | 12,73 |
| 27                      |Fonte Seasonic 300W                              |1         |110,00          | 110,00 |
| 28                      |Módulo de comutação Automática Para Bateria yx850|1         |26,00           | 26,00 |
| 29                      |Bateria Lítio 12v 9.6 AH                         |1         |375,00          | 403,00 |
| 30                      |Chave liga/Desliga Verde                         |1         |11,04           | 11,04 |
| 31                      |Push button Vermelho                             |1         |11,98           | 11,98 |
| 32                      |Webcam 12mp                                      |2         |100,00          | 200,00 |
| 33 |  Caixa Elétrica de Quadro de Comando | 1 | 189,57 | Cedido | 
| -- | Total | -- | 3729,63 | 3033,71 |

<p style="text-align:center">Fonte: Autoria própria.</p>

[**4.1.** **Projeto e Construção da Elétrica e Eletrônica**](../eletronica-energia/elet-cap4-projeto-e-construcao.md)

Nesta parte, são abordados os componentes elétricos e eletrônicos essenciais para o funcionamento do sistema. A seção cobre desde o layout dos circuitos até a integração de sensores e atuadores, além de garantir que todos os componentes eletrônicos estejam em conformidade com os padrões de segurança e eficiência energética.

[**4.2.** **Projeto e Construção da Estrutura**](../estruturas/est-cap4-projeto-e-construcao.md)

Esta seção detalha o design e a construção da estrutura física da solução. Inclui especificações sobre os materiais utilizados, as técnicas de montagem e os testes estruturais para garantir a durabilidade e a estabilidade da estrutura.


[**4.3.** **Projeto e Construção do Software**](../software/soft-cap4-projeto-e-construcao.md)

Aqui, são apresentados os detalhes sobre o desenvolvimento do software que controla e monitora o sistema, além do desenvolvimento relacionado a Inteligência Artificial. Inclui a arquitetura de software, as linguagens de programação utilizadas, as funcionalidades implementadas e os testes de validação para assegurar que o software atende aos requisitos operacionais e de desempenho.
