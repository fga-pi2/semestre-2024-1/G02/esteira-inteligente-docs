# **7. Plano de Testes Funcionais do Produto**

## 7.1. Objetivo

O objetivo de realizar um plano de testes é validar se o que foi planejado é executável considerando a sua funcionalidade, confiabilidade e segurança de uso. Sendo assim, este processo nos levará a garantir que a Esteira Inteligente funcione de forma adequada.

Os testes possuem o intuito de validar todas as funcionalidades da Esteira Inteligente, dessa forma, é importante que estes sejam realizados levando em consideração a estrutura, a elétrica, eletrônica e o software que são os componentes fundamentais deste projeto.

Dessa maneira, o plano de testes preliminar do produto está descrito abaixo.

## 7.2. Equipamentos Necessários

Para realizar os testes é indispensável termos alguns equipamentos, como:

- Esteiras;
- Sistema de célula de carga;
- Peças para serem transportadas pela esteira;
- Computador com o software do sistema da esteira;
- Câmeras para avaliação via imagem;
- Equipamentos de medição elétrica.

## 7.3. Procedimentos de Teste

### 7.3.1. Teste de Funcionalidade da Esteira:

- **Verificação da borracha**:
  - Verificar se a borracha estica devidamente para permitir o rolamento adequado.
  - Procedimento: Inspecionar visualmente e fisicamente a elasticidade da borracha.
  - Critério de aceitação: A borracha deve esticar sem apresentar rupturas ou desgastes anormais.

- **Giro do motor de passo**:
  - Verificar se a esteira gira de forma adequada a partir do motor de passo.
  - Procedimento: Acionar o motor e observar o movimento da esteira.
  - Critério de aceitação: A esteira deve girar suavemente e sem interrupções.

- **Simulação do giro correto**:
  - Simular o giro no passo correto para a queda das peças no ritmo certo.
  - Procedimento: Configurar a esteira para operar em diferentes velocidades e observar a queda das peças.
  - Critério de aceitação: As peças devem cair no ritmo esperado sem falhas.

### 7.3.2. Teste de Validação da Célula de Carga:

- **Sinalização da célula de carga**:
  - Verificar se a célula de carga envia os sinais necessários para conduzir as informações necessárias.
  - Procedimento: Aplicar diferentes pesos na célula e monitorar os sinais enviados.
  - Critério de aceitação: A célula deve enviar sinais precisos correspondentes aos pesos aplicados.

- **Precisão da célula de carga**:
  - Verificar se a precisão da célula de carga está adequada para identificar as divergências de peso entre os materiais.
  - Procedimento: Comparar as leituras da célula com um padrão de peso conhecido.
  - Critério de aceitação: As leituras devem estar dentro de uma margem de erro aceitável.

### 7.3.3. Teste de Funcionalidade do Sistema:

- **Comunicação do software**:
  - Verificar se o software consegue enviar e receber as informações necessárias e corretas dos dispositivos eletrônicos.
  - Procedimento: Realizar testes de envio e recepção de dados entre o software e os dispositivos.
  - Critério de aceitação: A comunicação deve ser estável e sem perdas de dados.

- **Usabilidade do software**:
  - Verificar se possui fácil usabilidade.
  - Procedimento: Realizar testes de uso com diferentes usuários e coletar feedback.
  - Critério de aceitação: O software deve ser intuitivo e de fácil navegação.

- **Criação de kits**:
  - Testar a criação de kits a partir dos materiais disponíveis para transporte.
  - Procedimento: Configurar e executar a montagem de kits no sistema.
  - Critério de aceitação: Os kits devem ser montados corretamente conforme configurado.

### 7.3.4. Teste da Inteligência Artificial:

- **Funcionamento da IA**:
  - Verificar se ela está funcionando corretamente no sistema operacional da Esteira Inteligente.
  - Procedimento: Testar a IA em diferentes cenários de identificação de peças.
  - Critério de aceitação: A IA deve identificar e diferenciar as peças corretamente.

- **Identificação de peças**:
  - Verificar se a IA consegue identificar e diferenciar as peças a partir das imagens.
  - Procedimento: Apresentar diferentes peças e verificar a identificação pela IA.
  - Critério de aceitação: A IA deve ter uma taxa de acerto alta na identificação das peças.

### 7.3.5. Teste de Sistema de Alerta:

- **Eficiência dos alertas**:
  - Avaliar se os métodos de aviso são eficientes para evitar erros na composição dos kits.
  - Procedimento: Simular erros na composição e verificar se os alertas são acionados.
  - Critério de aceitação: Os alertas devem ser acionados em todos os casos de erro.

### 7.3.6. Teste de Falhas de Energia:

- **Simulação de corte de energia**:
  - Simular corte de energia para validar se a bateria consegue fornecer energia para todos os componentes e não comprometer o uso da esteira.
  - Procedimento: Desligar a fonte principal de energia e monitorar o funcionamento da esteira.
  - Critério de aceitação: A esteira deve continuar funcionando corretamente com a energia da bateria.

### 7.3.7. Testes de Segurança do Hardware:

- **Verificação das conexões e componentes**:
  - Verificar se as conexões e componentes estão adequados ao sistema.
  - Procedimento: Inspecionar fisicamente e realizar testes elétricos nos componentes.
  - Critério de aceitação: Todos os componentes e conexões devem estar firmes e operacionais.

## 7.4. Critérios de Aceitação 

- **Funcionalidade da esteira**:
  - A esteira deve funcionar de forma estável e controlada para cair apenas uma peça por vez.

- **Sistema de alerta**:
  - O alerta deve ser emitido sempre que houver divergências, seja pela inteligência artificial ou pela célula de carga.

- **Usabilidade do software**:
  - O software deve ter uma usabilidade adequada e sem defeitos que atrapalhem o seu uso.

- **Posicionamento da câmera**:
  - A câmera deve estar posicionada de tal forma que seja possível a visualização das peças na dispensa da célula de carga.