## **3.1. Projeto do subsistema Elétrica/Eletrônica**

Visando atender as necessidades do projeto da Esteira Inteligente, foi realizada a divisão de quatro subsistemas, são eles: Controle, Sensoriamento, Motores e atuadores e Alimentação.

Os componentes foram escolhidos com base nos requisitos, priorizando disponibilidade abundante no mercado, preços acessíveis e capacidade satisfatória de atender os requisitos estabelecidos.

- O subsistema de controle é responsável pelo processamento de entrada e saída de dados do sistema como um todo;

- O subsistema de sensoriamento recebe e processa dados provenientes dos módulos LRD e da célula de carga, juntamente com seu conversor Analógico-digital;

- O subsistema de motores e atuadores é responsável por captar e processar dados de entrada e saída provenientes dos motores NEMA 17, juntamente com os drivers controladores de motor;

- O subsistema de alimentação garante o bom funcionamento dos componentes eletrônicos, fornecendo assim corrente e tensão necessárias.

### **3.1.1. Configuração do Subsistema de motores**

O A4988, Figura 3.1.1,  é um driver projetado visando proporcionar controle preciso de movimento de motores de passo, utilizando controle PWM. Além disso, ele incorpora proteções contra sobrecarga de corrente, temperatura e curtos. Com esse driver é possível controlar a corrente que passa por cada bobina do motor, por meio de uma relação de um resistor de sensibilidade(RS), uma tensão de referência (VREF).


Por meio de um resistor variável, é possível ajustara tensão de referência VREF para às necessidades do projeto.

**<h6 align="center"> Figura 3.1.1 Driver A4988</h6>**
![A4988_detralhes](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/A4988_detralhes.png)
<p style="text-align:center">Fonte: Autores. </a></p>


Controlar a corrente que passa no motor é de suma importância, pois com isso é possível encontrar um melhor cenário de funcionamento para os motores e o restante do circuito, evitando que componentes sejam danificados por super aquecimento, além de evitar que os motores percam precisão em relação aos passos.


Desse modo, é possível calcular a corrente que passará pelas bobinas do motor de passo da seguinte maneira:

<div align="center">

<p>VREF = Imax * (8 * RS)</p>
</div>



Visando preservar o driver controlador de motor, para os cálculos iniciais foi utilizada uma corrente máxima de 70% do valor da corrente dos motores, que nesse caso equivale a 1.4A, e o resistor de referência RS equivale a 100 Ohms. Desse modo, o valor de Vref foi definido da seguinte forma:

<div align="center">
<p>VREF = 1.4 * (8 * 100)</p>
</div>

<div align="center">
<p>VREF = 1.12 Volts</p>
</div>




Após o valor de VREF ser definido, com o auxílio de um multímetro para aferir a tensão, e uma chave de fenda, foi realizada a alteração na tensão fornecida pelo A4988, girando aos poucos o resistor variável presente no driver, até que se alcance a tensão calculada. Abaixo, na Figura 3.1.2, pode ser observado esse processo:





**<h6 align="center"> Figura 3.1.2 Multímetro aferindo a tensão</h6>**
![Setando a VREF](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/A4988_2_gif.gif)
<p style="text-align:center">Fonte: Autores. </a></p>




Com o driver A4988 devidamente acomodado na CNC shield, e no Arduino, e após conectar o motor de passo NEMA 17 no circuito, foi realizado o primeiro teste de movimento como o motor, que pode ser observado na animação a seguir(Figura 3.1.3):

**<h6 align="center"> Figura 3.1.3 Teste de movimento do motor NEMA 17</h6>**
![Teste com motor Nema17](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/A4988_3_gif.gif)
<p style="text-align:center">Fonte: Autores. </a></p>




Abaixo, na figura 3.1.4, segue uma ilustração de como os circuitos ficaram integrados. Pode-se observar a CNC Shield conectada sobre o Arduino Uno, o Driver A4988 conectado na CNC Shield, e o motor conectado nos terminais que vão para o Driver de motor. Além disso, pode-se observar que existem mais três outros slots vazios. Estes slots serão utilizados para conectar os outros motores que serão responsáveis por movimentar as outras esteiras.

**<h6 align="center"> Figura 3.1.4 Integração Eletrônica</h6>**
![Circuitos conectados](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/inte_circuit.png)
<p style="text-align:center">Fonte: Autores. </a></p>

Após a realização dos testes iniciais com o motor de passo de forma isolada, avançamos para a próxima etapa: testar o motor em uma das esteiras. Essa etapa permitiu avaliar o comportamento prático do motor em uma situação muito próxima de seu funcionamento real.

Durante o teste na esteira, tivemos a oportunidade de ajustar parâmetros essenciais, como velocidade, passo e torque. Essa abordagem mais próxima da aplicação real nos proporcionou resultados mais fidedignos e nos permitiu otimizar o desempenho do motor de acordo com as necessidades específicas do projeto.

A seguir, apresentamos um gif do primeiro teste realizado com um dos motores, inserido pela primeira vez na estrutura, com esteira provisória, confeccionada para testes:

**<h6 align="center"> Figura 3.1.5 Teste Inicial dos motores na esteira</h6>**
![Teste Inicial dos motores na esteira](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/motor_esteira1.gif)
<p style="text-align:center">Fonte: autores </a></p>


Com isso, foram obtidos dados fundamentais que nos permitiram configurar o torque e o passo dos outros dois motores. Essa etapa foi essencial para a integração completa do sistema de motores na estrutura da esteira.

Com base nos resultados obtidos, conseguimos ajustar os parâmetros dos motores adicionais, garantindo que todos trabalhem de forma correta. 
A seguir, pode-se observar o teste com os três motores já instalados na estrutura, juntamente com as peças de MDF coloridas sobre a correia da esteira. Esse teste simula a utilização real da esteira, nos permitindo avaliar melhor o sistema de motores:

**<h6 align="center"> Figura 3.1.6 Teste inicial dos motores na esteira</h6>**
![Teste Inicial dos motores na esteira](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/motor_esteira3.gif)
<p style="text-align:center">Fonte: autores </a></p>

Após a fase de testes com os três motores, partimos para os ajustes finos necessários para garantir o seu bom funcionamento. Esses ajustes foram realizados com base nas  necessidades do projeto, visando a otimização e a coordenação precisa dos motores. Também foram adicionados dois botões à esteira, um liga/desliga padrão que interrompe o funcionamento do sistema e um botão de emergência, visando evitar acidentes e travando o acionamento dos motores caso necessário, fazendo com que a esteira pare o andamento, sem que sejam perdidos os dados de software.


**<h6 align="center"> Figura 3.1.7 Botão de emergência</h6>**
![Teste Inicial dos motores na esteira](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/emergencia.jpg
)
<p style="text-align:center">Fonte: autores </a></p>

Após serem realizados todos os testes  e refinamentos dos subsistemas eletrônicos, com os ajustes necessários para o seu correto funcionamento,   e com a eletrônica/energia totalmente  integrados e instalados na estrutura, o quadro com os componentes Eletrônicos /Energia ficou da seguinte maneira:

**<h6 align="center"> Figura 3.1.7 Quadro de energia e componentes eletrônicos</h6>**
![Quadro de energia](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/quadro_eletronica.jpg)
<p style="text-align:center">Fonte: autores </a></p>


Com os ajustes concluídos, o sistema de motores estava pronto para a integração com o software. Mais especificamente, direcionamos nossos esforços para a conexão com o Backend. Essa etapa é crucial para sincronizar os comandos do software com o movimento dos motores, permitindo que a esteira funcione de maneira eficiente e precisa.


### **3.1.2 Subsistema de sensoriamento**

O subsistema de sensoriamento estava projetado para utilizar os sensores LDR e a Célula de carga. Porém, analisando as necessidades do projeto, foi concluído que a célula de carga era suficiente para realizar a validação dos Kits de peças através do seu peso.

A célula de carga é um sensor ativo que também é resistivo. A célula se trata de um sensor extensômetro, também conhecido como "strain gauges", dentro da célula de carga que será utilizada há um conjunto de sensores resistivos formando uma ponte de wheatstone.


Na célula de carga, é utiliza a lei de Ohm para o cálculo de corrente e tensão emitida por eles.


Sendo a formula descrita por: 
<div align="center">
<p>I = V/R</p>
</div>

Na ponte de wheatstone temos:

<div align="center">
<p>V_o = [R3/(R3+R4) - R2/(R1+R2)] * V_ex</p>
</div>

Sendo V_ex a tensão constante fornecida pelo microcontrolador e V_o a tensão retornada para o módulo conversor HX711. 

O HX711 é o módulo conversor analógico-digital que é responsável por tratar o dado vindo da célula de carga e transmitir ele para o micro controlador. Ele se trata de Circuito Integrado especializado em conversão  para balanças. O HX711 emite um sinal de 24-bits para o microcontrolador para que seja feita a análise de massa.


#### 3.1.2.2 **Célula de Carga**

Para os testes iniciais, fora da estrutura final na qual seria empregada a balança,na montagem da célula de carga foi utilizada uma base sólida reta de plástico, onde a célula de carga (1) foi colocada em cima. Uma protoboard para que fossem conectados os jumpers com o módulo de conversão HX711 (2) e o microcontrolador ESP32 (3). De forma provisória, nos testes iniciais, houve a alteração provisória do microcontrolador, e foi utilizada uma ESP32. Segue na Figura 3.1.5, o sistema montado:

**<h6 align="center"> Figura 3.1.8 LoadCell montada sem massa</h6>**
![LoadCell montada sem massa](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/loadcell0g.jpeg)
<p style="text-align:center">Fonte: autores </a></p>

Após a montagem foi executado o código de calibração da célula de carga. Essa calibração não é definitiva, visto que só pode ser feita de forma definitiva quando a parte estrutural da célula ficar pronta. Na Figura 3.1.6, observa-se o terminal da montagem.

**<h6 align="center"> Figura 3.1.9 Calibração célula de carga</h6>**
![Calibração célula de carga](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/terminal7gLoad_Cell.png)
<p style="text-align:center">Fonte: autores </a></p>

Na última parte da rotina de calibração é necessário colocar uma massa conhecida. A massa escolhida foi um sachê de _ketchup_ que possui 7g. Na imagem a seguir é apresentado como ele foi colocado na célula de carga.


**<h6 align="center"> Figura 3.1.10 LoadCell aferindo massa de 7 gramas</h6>**
![LoadCell aferindo massa de 7 gramas](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/loadcell7g.jpeg)
<p style="text-align:center">Fonte: autores </a></p>

Após a calibração foi escolhida outra massa conhecida para verificar se a célula estava aferindo massa corretamente, dessa vez foi escolhido um sachê de mostarda que possui 5g. Na Figura 3.1.11 a seguir, temos a saída do terminal.

**<h6 align="center"> Figura 3.1.11 Terminal da célula de carga aferindo 5g</h6>**
![Terminal da célula de carga aferindo 5g](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/terminal5gLoad_Cell.png)
<p style="text-align:center">Fonte: autores </a></p>

**<h6 align="center"> Figura 3.1.12 Célula de carga aferindo massa de 5 grama</h6>**
![Célula de carga aferindo massa de 5 gramas](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/loadcell5g.jpeg)
<p style="text-align:center">Fonte: autores </a></p>


Esses testes iniciais serviram para termos uma base do funcionamento e implementação da célula de carga. Após essa fase, partimos para a implementação utilizando o Arduino Uno, além disso, com a estrutura em fase avançada, foi possível realizar testes simulando um uso real da balança, e realizar uma calibração mais fina e eficaz da célula de carga.

A seguir, pode-se observar na imagem a montagem final da balança na estrutura, onde pudemos testar o funcionamento.

**<h6 align="center"> Figura 3.1.13 Balança na estrutura</h6>**
![Balança na estrutura](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/balança_F.png)
<p style="text-align:center">Fonte: autores </a></p>


A imagem a seguir representa uma vista mais próxima da balança desenvolvida pela equipe de estruturas para a Esteira Inteligente:


**<h6 align="center"> Figura 3.1.14 Balança montada em detalhes</h6>**
![Balança montada em detalhes](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/balanca-quadrada.jpg
)
<p style="text-align:center">Fonte: autores </a></p>


Nos testes realizados com a balança devidamente instalada na estrutura, simulando uma utilização real da célula de carga, obtivemos bons resultados, variando sempre abaixo de 5% da margem de erro. Considerando que as peças de madeira possuíam 12g(circulo), 22g(triângulo) e 32g(Quadrado), e que o kit de peças mais leve possuia em torno de 79 gramas, foi um resultado bom e suficiente para a auxiliar na validação dos kits.

### **3.1.3. Dimensionamento dos Cabos**

Para realizar o dimensionamento dos cabos do circuito é necessário primeiro saber a potência total e a tensão do projeto, com esses dois dados é calculada a corrente. Temos que a potência total do projeto é de 118,25W, a tensão utilizada é de 12V e a corrente é de 12,4A. No entendo, como temos um circuito que terá influência de um conversor (de 12V para 5V), vamos calcular a seção para cada uma das partes, sendo a primeira recebendo 12V, 96W e 8A, e a segunda 5V, 22,25W e 4,45A.

Os primeiros passos são iguais para ambos os cenários, logo, eles serão aplicados para os dois. De acordo com a norma ABNT NBR 5410 ([10](bib.md)), o método de instalação da máquina tem o método de referência B1 (Condutores isolados ou cabos unipolares em eletroduto aparente de seção circular sobre parede ou espaçado desta menos de 0,3 vez o diâmetro do eletroduto - Método de Instalação 3).

**<h6 align="center"> Tabela 3.1.1: Tipos de linhas elétricas</h6>**
![Tabela33](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_33.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

O número de condutores carregados é de dois (2), de acordo com a configuração eletrônica. Entretanto, no cálculo será utilizado apenas um, pois será necessário dividir o sistema, já que a tensão que passará pelos componentes será diferente (12V e 5V). Segundo a NBR 5410 ([10](bib.md)), teremos que utilizar dois fatores de correção. Em relação ao primeiro fator, temos que para 1 agrupamento de circuitos, Tabela 4.1.3 - Fatores de correção aplicáveis a condutores agrupados em feixe (em linhas abertas ou fechadas) e a condutores agrupados num mesmo plano, em camada única (item 1), é aplicado um fator de correção de 1,00. Um segundo fator de correção é necessário pela temperatura ambiente ser diferente de 30 graus célsius, este é obtido na Tabela 4.1.2 - Fatores de correção para temperaturas ambientes diferentes de 30ºC para linhas não-subterrâneas e de 20ºC (temperatura do solo) para linhas subterrâneas. Sabendo que o material de isolação é o PVC, seu fator de correção é de 0,94.

**<h6 align="center"> Tabela 3.1.2: Fatores de correção para temperaturas ambientes diferentes
de 30ºC para linhas não-subterrâneas e de 20ºC
(temperatura do solo) para linhas subterrâneas </h6>**
![Tabela40](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_40.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

**<h6 align="center"> Tabela 3.1.3: Fatores de correção aplicáveis a condutores agrupados em feixe (em linhas abertas ou fechadas) e a condutores agrupados num mesmo plano, em camada única</h6>**
![Tabela42](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_42.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

Para simplificar o processo do fator de correção, podemos multiplicar os dois fatores (temperatura e agrupamento) que resulta num único valor (0,94 x 1,00 = 0,94). 

Partindo para a escolha do cabo, utilizaremos a tabela 36 da norma. Sabendo que nosso método de referência é o B1 e possuímos 2 condutores carregados, primeiro temos que aplicar o fator de correção na corrente suportada pela seção nominal para obtermos um valor real. Visto que no primeiro caso (12V) a corrente é de 8,0A, é necessária uma seção que aguente uma corrente maior que essa. Neste caso, iremos utilizar a seção de 0,5m^2, que suporta uma corrente superior à projetada (9 x 0,94 = 8,46). No segundo caso, a corrente é de 4,45A, para este caso, também utilizaremos a seção de 0,5m^2, que suporta uma corrente superior à projetada (9 x 0,94 = 8,46).

**<h6 align="center"> Tabela 3.1.4: Capacidades de condução de corrente, em ampères, para os métodos de referência
A1, A2, B1, B2, C e D</h6>**
![Tabela36](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_36.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

### 3.1.4. **Dimensionamento da Bateria**

Para o dimensionamento foi admitido um requisito não funcional a respeito da eficiência energética, onde apenas um motor funciona por vez. Não permitindo desta forma o funcionamento de 2 ou mais ao mesmo tempo. Desta maneira a máquina passa a possuir um valor de corrente de descarga máxima de 8,5A. Uma bateria de 9,8Ah supre a necessidade do sistema assim como o requisito estabelecido anteriormente.

O tempo estipulado para o funcionamento do sistema de redundância será de cinco minutos, tempo este sendo mais que o suficiente para a conclusão de um ciclo de trabalho. Com esta margem de tempo garantimos uma margem de erro segura e mais que o suficiente para a conclusão de um ciclo, não sendo necessário a utilização de uma bateria maior.
A bateria é o principal componente do sistema de redundância, sendo o responsável pela alimentação do sistema em caso de queda de energia. Este sistema realiza a troca da fonte de alimentação da esteira, que antes era alimentada pela rede passa a ser pela bateria, essa troca é realizada pelo módulo de comutação.

### 3.1.5. **Equipamentos de Segurança**

3.1.5.1. Disjuntor unipolar;
3.1.5.2. Chave fusível;
3.1.5.3. DPS;
3.1.5.4. Fonte chaveada;
3.1.5.5. Aterramento.


Basicamente são 5 equipamentos/procedimentos que realizam a proteção do circuito como um todo. A corrente ao sair da rede passa por uma chave fusível (popularmente conhecida como corta-circuito) que tem a função de proteger contra a sobrecorrente originadas por sobrecargas, curto- circuitos, dentre outros. Atuando no impedimento ou barramento dessa sobrecorrente. Após passar pela chave fusível a corrente se direciona para outro dispositivo de proteção que é o disjuntor unipolar, é um mecanismo de segurança que possui a função  de prevenir que aparelhos e equipamentos sejam danificados, cortando a energia no instante antes de serem atingidos pela sobrecarga. Neste ponto a corrente se divide entre a fonte chaveada e o DPS, a fonte chaveada diminui a corrente consumida pelos equipamentos a níveis mais confiáveis além de atuar como chaves que em caso de falhas desliga automaticamente e funcionar por um curto período como uma bateria em caso de queda de energia. Já o DPS ou dispositivo de proteção contra surtos é um dispositivo que tem como função a proteção da instalação na ocorrência de surtos causados por descargas atmosféricas ou picos de tensão. Este último deve ser ligado ao aterramento para que em caso de falha no DPS o aterramento conduzirá a elevada corrente para o solo impedindo a ocorrência de acidentes.