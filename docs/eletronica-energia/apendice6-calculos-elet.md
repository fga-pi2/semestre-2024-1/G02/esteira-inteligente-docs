# **Memorial de cálculos de elementos eletrônicos e energéticos**
 
## **6.1. Cálculo de corrente das bobinas**

O cálculo da corrente que passará pelas bobinas do motor de passo será realizado da seguinte maneira:

<div align="center">

<p>VREF = Imax * (8 * RS)</p>
</div>


Para preservar o <i>driver</i> controlador de motor, para os cálculos iniciais foi utilizada uma corrente máxima de 70% do valor da corrente dos motores, que nesse caso equivale a 1.4A, e o resistor de referência RS equivale a 100 Ohms. Desse modo, o valor de Vref foi definido da seguinte forma:

<div align="center">
<p>VREF = 1.4 * (8 * 100)</p>
</div>

<div align="center">
<p>VREF = 1.12 Volts</p>
</div>

## **6.2. Cálculo de corrente e tensão dos sensores**

Para ambos os sensores, que são resistivos, é utilizada a lei de Ohm para o cálculo de corrente e tensão emitida por eles.

Sendo a formula descrita por: 
<div align="center">
<p>I = V/R</p>
</div>

Na ponte de wheatstone temos:

<div align="center">
<p>V_o = [R3/(R3+R4) - R2/(R1+R2)] * V_ex</p>
</div>

Sendo V_ex é a tensão constante fornecida pelo microcontrolador e V_o a tensão retornada para o módulo conversor HX711.

## **6.3. Dimensionamento da bateria**

Para o dimensionamento da bateria foi admitido um requisito não funcional a respeito da eficiência energética, onde apenas um motor funciona por vez. Desta maneira a máquina passa a possuir um valor de corrente de descarga máxima de 8,5A. Uma bateria de 9,8Ah supre a necessidade do sistema assim como o requisito estabelecido anteriormente.

O tempo estipulado para o funcionamento do sistema de redundância será de cinco minutos, tempo este sendo mais que o suficiente para a conclusão de um ciclo de trabalho. Com esta margem de tempo garantimos uma margem de erro segura e mais que o suficiente para a conclusão de um ciclo, não sendo necessário a utilização de uma bateria maior.