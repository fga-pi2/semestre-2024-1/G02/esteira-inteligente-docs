## **1. Requisitos de Eletrônica/Energia**{#requisitos-eletronica-energia}

**<h6 align="center"> Tabela 1.1: Requisitos funcionais para a Eletrônica e Energia do produto</h6>**

| Requisito |                        Descrição                         |                                                                                        Observações                                                                                        |
| :-------: | :------------------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|  RFEE01   |                       Alimentação                        |                                                           O sistema deverá conseguir alimentar o sistema de controle e motores.                                                           |
|  RFEE02   |                       Redundância                        |                                 O sistema deverá ter um sistema de alimentação de redundância permitindo o funcionamento de todo o sistema por 5 minutos.                                 |
|  RFEE03   |                Processamento de interface                |                              O sistema deverá conseguir processar toda a interface de usuário, sendo ela as entradas de dados pelo usuário e saída de vídeo.                              |
|  RFEE04   |                 Processamento de  dados                  |                      O sistema deverá conseguir processar todos os dados de entrada e saída, fornecido pelos subsistemas de Sensoriamento e de Motores e Atuadores.                       |
|  RFEE05   |                    Aferição de Massa                     |                                             O sistema deverá conseguir aferir a massa dos componentes selecionados com uma precisão razoável                                              |
|  RFEE06   |                 Processamento de imagem                  |                                                  O sistema deverá identificar as bordas dos objetos por meio de processamento de imagem.                                                  |
|  RFEE07   | Identificação de quantidade de objetos a partir do peso | O sistema deverá capaz de identificar massa de objetos a partir de célula de carga, decodificando o sinal e retornando uma saída nas unidades de medida de: Kg (Kilo gramas)ou g (gramas) |
|  RFEE08   |                  Sistema de redundância                  |                           O sistema deverá ter um sistema de redundância, para gerar robustez no sistema de Motores e Atuadores, por meio de sensores luminosos                           |
|  RFEE09   |           Identificação e contagem de objetos            |                                       O sistema deverá conseguir identificar qual objeto e contar os objetos, por meio de processamento de imagem.                                        |
|  RFEE10   |                       Motorização                        |                                                                    O sistema deverá ser capaz de movimentar a esteira.                                                                    |

<p style="text-align:center">Fonte: Autoria própria.</p>

**<h6 align="center"> Tabela 1.2: Requisitos não funcionais para a Eletrônica e Energia do produto</h6>**

| Requisito |             Descrição              |                                                            Observações                                                             |
| :-------: | :--------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------: |
|  RNFEE01  | Indicador luminoso de funcionamento |                 O sistema deverá ter indicadores luminosos de funcionamento e de ativação do botão de emergência.                  |
|  RNFEE02  |      Fluidez de funcionamento      |                               O sistema não poderá ter travamento durante o processamento de imagem.                               |
|  RNFEE03  |    Modo de economia de energia     | Sugere que o sistema controle os motores em 2 modos, o modo REDE e o modo BAT (bateria), visando a economia de consumo de corrente. |
|  RNFEE04  |     Indicação de funcionamento      |                                    O sistema deverá indicar qual esteira está em funcionamento.                                    |

<p style="text-align:center">Fonte: Autoria própria.</p>
