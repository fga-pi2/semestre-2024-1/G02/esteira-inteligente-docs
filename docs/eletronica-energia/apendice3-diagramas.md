
# **Diagramas elétricos e eletrônicos**

Nesta seção estão os esquemáticos e diagramas dos subsistemas de elétrica e eletrônica desenvolvidos para o projeto, mostrando as conexões entre os componentes, protocolos de comunicação, consumo dos componentes, sistema de proteção contra curtos e surtos, dentre outros.

## **5.1. Diagramas e esquemáticos eletrônicos**

Incluem-se os diagramas e esquemáticos dos circuitos, conexões, protocolos utilizados e consumo. 

### **5.1.1. Diagrama de Conexões dos Subsistemas**

**<h6 align="center"> Figura 5.1.1: Diagrama de Conexões dos subsistemas</h6>**
![conexão](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_de_blocos_V1.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

### **5.1.2. Diagrama de consumo**

**<h6 align="center"> Figura 5.1.2: Diagrama de consumo</h6>**
![Consumo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_de_consumo_v2.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

### **5.1.3. Esquemático de motorização**

**<h6 align="center"> Figura 5.1.3: Esquemático de motorização</h6>**
![Consumo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Motorizacao.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

### **5.1.4. Esquemático de baixo processamento**

**<h6 align="center"> Figura 5.1.4: Esquemático de baixo processamento</h6>**
![Consumo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Baixo_Processamento.png?ref_type=heads)
<p style="text-align:center">Fonte: Autoria própria.</p>

## **5.2. Diagramas e esquemas elétricos**

### **5.2.1. Diagrama de Proteção**

**<h6 align="center"> Figura 5.2.1: Diagrama de proteção</h6>**

![Diagrama de Proteção V1](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_de_Prote%C3%A7%C3%A3o_Vers%C3%A3o_1.jfif?ref_type=heads)

<p style="text-align:center">Fonte: Autoria própria com base na ‌[ABNT NBR 5410](bib.md) e [ABNT NBR 14039](bib.md).</p>

### **5.2.2. Diagrama de Proteção em Blocos**
**<h6 align="center"> Figura 5.2.2: Diagrama de proteção em Blocos</h6>**
![Diagrama de Proteção V2](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_Protecao_BlocosV2.png?ref_type=heads)

<p style="text-align:center">Fonte: Autoria própria‌ ‌c‌o‌m‌ ‌b‌a‌s‌e‌ ‌n‌a‌ ‌[ABNT NBR 14039](bib.md) e [ABNT NBR 14039](bib.md) .</p>

### **5.2.3. Diagrama de Unifilar**

**<h6 align="center"> Figura 5.2.3: Diagrama Unifilar</h6>**

![Diagrama Unifilar](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_Unifilar.png?ref_type=heads)

<p style="text-align:center">Fonte: Autoria própria com base na ‌[ABNT NBR 5410](bib.md) e [ABNT NBR 14039](bib.md) .</p>