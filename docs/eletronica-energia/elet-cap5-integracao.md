## **4.1. Detalhamento de atividades para integração eletrônica/energética**
A  integração entre os subsistemas de eletrônica/energia, estruturas e software é de suma importância para o adequado funcionamento da esteira.


O subsistema de energia, é encarregado de fornecer a alimentação adequada para todos os componentes da Esteira Inteligente, incluindo, os sensores, motores, drivers controladores de motor, micro controladores e computadores responsáveis pelo processamento de dados. Além disso, é responsável pelo dimensionamento dos cabos e conexões que percorrem toda a estrutura e conectam os componentes eletrônicos.


O subsistema de motores se integra à estrutura, situando-se no eixo das esteiras, o que permite o movimento da esteira para realizar a entrega das peças na balança.Por sua vez, o subsistema de sensoriamento, é dividido entre a célula de carga, que é acoplada numa plataforma da estrutura física da esteira, para funcionar como uma balança e aferir a massa das peças, e o sensor LDR, responsável pela contagem das peças que passam por ele. Este último funciona como um sistema de redundância no controle das peças e incorpora a parte de processamento de imagem por meio de duas câmeras.


O subsistema de controle, encarregado do processamento de dados de entrada e saída e da execução do sistema operacional principal da máquina, se integra ao software. Ele recebe comandos de entrada para o funcionamento do sistema e fornece dados de saída, como respostas dos sensores, motores e da câmera utilizada no processo de reconhecimento de imagem utilizando IA.





