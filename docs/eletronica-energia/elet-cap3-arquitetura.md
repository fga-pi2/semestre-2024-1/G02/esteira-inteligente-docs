## **2.1. Arquitetura do Eletrônica/Energia**

###  **2.1.1. Arquitetura De Eletrônica**

Inicialmente, foi pensado na Orange Pi PC como o microcomputador principal. Porém, para melhor adequação das necessidades do projeto, o Orange pi PC foi substituído pela Raspberry pi 4, que apresenta um melhor desempenho computacional.
A Raspberry Pi 4 atuará como o microcomputador principal, executando um sistema operacional capaz de processar os dados de entrada e saída do sistema como um todo. Por sua vez.

Além disso, inicialmente para a comunicação como microcomputador principal, seria utilizado uma orange pi zero W, porém, visando versatilidade, foi implementada uma comunicação via servidor web, que proporciona que qualquer dispositivo com um navegador, seja capaz de acionar a esteira, trazendo assim uma grande versatilidade e acessibilidade ao usuário.


O Arduino, por sua vez, assumirá a função de facilitar a comunicação através do CNC Shield. Ele estabelecerá a conexão entre os motores, os drivers de motores e o microcomputador, neste caso, o Orange Pi PC. Além disso, será responsável por receber os dados provenientes do módulo de carga e dos sensores LDR, e processar os dados de entrada e saída relativos aos subsistemas.

Após a definição dos requisitos dos subsistemas de Eletrônica, foram selecionados os componentes eletrônicos a serem utilizados no projeto. Paralelamente, foram elaborados os diagramas e esquemas necessários para a implementação dos mesmos.

####  **2.1.1.1. Diagrama de conexões dos subsistemas**

Abaixo, na Figura 2.1.1.1., tem-se o diagrama criado para detalhar as conexões dos subsistemas, bem como seus protocolos de comunicação. 

**<h6 align="center"> Figura 2.1.1.1: Diagrama de conexões dos subsistemas</h6>**
![conexão](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama1_atualizado.jpg)
<p style="text-align:center">Fonte: Autoria própria.</p>

####  **2.1.1.2. Diagrama de consumo**

Após a definição de todos os subsistemas e seus componentes correspondentes, foi desenvolvido um diagrama de consumo para os circuitos. Os circuitos foram divididos em dois blocos: aqueles que operam com uma tensão de 12V e aqueles que operam com uma tensão de 5V, como pode ser observado no diagrama de consumo da Figura 2.1.1.2.

**<h6 align="center"> Figura 2.1.1.2: Diagrama de consumo</h6>**
 ![Consumo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama2_atualizado.jpg)
 <p style="text-align:center">Fonte: Autoria própria.</p>

Desse modo, o bloco que utiliza tensão de 12V totaliza um máximo de 8A como consumo de corrente, resultando em uma potência de 96W. Já o bloco, que opera com uma tensão de 5V, totaliza um consumo máximo de corrente de 4.55A, resultando em uma potência de 22.75W. Um detalhe a ser observado é que as webcams são alimentadas pela Orange Pi PC, não acrescentando um valor extra no consumo de corrente do sistema.

####  **2.1.1.3. Esquemático do circuito de motorização**

A Figura 2.1.1.3 representa o esquemático referente aos circuitos relacionados a parte de motorização da esteira inteligente, mostrando o esquema de conexões entre os motores, drivers controladores de motores, Arduino e Orange Pi PC ([3](bib.md)).

**<h6 align="center"> Figura 2.1.1.3: Esquemático do circuito de motorização</h6>**
 ![motorização](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Motorizacao.png)
 <p style="text-align:center">Fonte: Autoria própria.</p>

####  **2.1.1.4. Esquemático de baixo processamento**

A Figura 2.1.1.4 representa o esquemático que se refere a parte dos circuitos de baixo processamento, mostrando a conexão entre os sensores LDR, e célula e carga, conversor analógico-digital e o Arduino.

**<h6 align="center"> Figura 2.1.1.4: Esquemático de baixo processamento</h6>**
 ![motorização](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Baixo_Processamento.png?ref_type=heads)
 <p style="text-align:center">Fonte: Autoria própria.</p>

####  **2.1.2. Componentes eletrônicos**

Para a seleção de componentes, diversos aspectos foram ponderados, incluindo o custo, a disponibilidade no mercado, a capacidade de desempenhar a função relevante e as opções globais do projeto. Além disso, foram considerados componentes que o grupo já possuía e que atendiam satisfatoriamente aos requisitos do projeto.

Os componentes foram divididos em subsistemas e definidos da seguinte forma:

#####  **2.1.2.1. Subsistema de controle**
 	
- Raspberry pi 4:    

É um microcomputador, sendo responsável por realizar o processamento dos dados de entrada e saída, além de rodar o sistema operacional principal.
**<h6 align="center"> Figura 2.1.2.1.1: Raspberry Pi 4</h6>**
![OrangePI](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/rasp4.jpg)

Algumas das especificações mais relevantes da Raspbery Pi 4:

Processador: Quad-core Cortex-A72 de 64 bits, com clock de até 1,8 GHz, memória RAM de 1 GB LPDDR4. Conectividade Wi-Fi,
Bluetooth, além de portas USB 2.0 e 3.0 e porta Ethernet.

Por conta dessas características, e ter um bom poder de processamento, a Raspberry Pi 4 demonstrou ser o microcomputador ideal para o projeto.

- Arduino Uno R3:

O Arduino Uno R3 ([4](bib.md)) é a placa que utiliza um microcontrolador, possibilitando o processamento de dados e a conexão entre módulos e circuitos controladores de entrada e saída, tais como o módulo LDR, motores de passo e drivers controladores de motor, e a Orange Pi. Serão utilizadas duas placas deste modelo.

**<h6 align="center"> Figura 2.1.2.1.2: Arduino Uno R3</h6>**
![ArduinoUnoR3](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/AUR3.jpeg)

- CNC Shield: 
    
Esta placa é responsável por fazer a conexão dos motores e dos drivers controladores de motores ([6](bib.md)), e acoplá-los ao Arduino.

**<h6 align="center"> Figura 2.1.2.1.3: CNC Shield</h6>**
![CNCSHIELD](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/CNCSHIELD.jpeg)

- Driver A4988:

Este é o circuito responsável por controlar os motores de passo NEMA17  ([7](bib.md)). Ele é acoplado na CNC Shield e utiliza a conexão de pinos digitais do Arduino. Serão utilizadas três placas controladoras de motor, uma para cada motor utilizado no projeto.

**<h6 align="center"> Figura 2.1.2.1.4: Driver A4988</h6>**
![A4988](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/A4988.jpeg)


- Conversor Analógico Digital HX711: 

Circuito conversor responsável por converter o sinal analógico recebido pela célula de carga em um sinal digital.

**<h6 align="center"> Figura 2.1.2.1.5: Conversor Analógico Digital HX711</h6>**
![HX711](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/HX711.jpg)

- **Push button e chave:**

Serão utilizadas uma chave liga e desliga para iniciar o funcionamento da máquina, e um push button com a finalidade de pausar o funcionamento do maquinário em situações de emergência.

**<h6 align="center"> Figura 2.1.2.1.6: Push button e chave</h6>**
![BotãoEChave](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Chave.jpg)


#####  **2.1.2.2. Subsistema de Sensoriamento**


- **Webcam**: 
   
Responsáveis por realizar a captura de imagens para o processamento de imagem, serão utilizadas duas webcams para esta finalidade.

**<h6 align="center"> Figura 2.1.2.2.1: Webcam</h6>**
![WEBCAM](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/MCAM.jpeg?ref_type=heads)

**- Célula de carga 10kg:**

Componente responsável por aferir a massa das peças depositadas na superfície de medição de peso. De acordo com a massa do objeto depositado, a célula de carga apresenta uma deformação, que altera os dados de saída, os quais serão convertidos no peso do objeto.

**<h6 align="center"> Figura 2.1.2.2.2: Célula de carga 5kg</h6>**
![CELULADECARGA](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/CEL_CARG.png)

#####  **2.1.2.3. Subsistema de motores e atuadores**
	
- **Motores Nema 17** : 

OOs motores NEMA17 são motores de passo elétricos e serão os componentes responsáveis por realizar o movimento das esteiras. Serão utilizados três motores, um para cada esteira.

**<h6 align="center"> Figura 2.1.17: Motores Nema 17</h6>**
![NEMA17](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/NEMA17.jpeg)

#####  **2.1.2.4. Subsistema de alimentação**
	 
- **Proteção anti-raios e surtos DPS 20A iClamper Pocket**:
    
Um circuito de proteção com o intuito de proteger todo o sistema eletroeletrônico, evitando a queima de componentes.

**<h6 align="center"> Figura 2.1.18: Proteção anti-raios e surtos DPS 20A iClamper Pocket</h6>**
![ICLAMPER](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/ICLAMPER.jpg)

	
- **Módulo step down LM2596**

O módulo regulador de tensão step down converte a tensão de entrada de 12V para 5V. Sua função é reduzir a tensão da fonte de alimentação para 5V, atendendo assim o bloco de componentes que operam nesta faixa de tensão.

**<h6 align="center"> Figura 2.1.19: Módulo step down LM2596</h6>**
![ICLAMPER](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Mod_Step_Down.jpg)

- **Fonte chaveada 12V**

A fonte é o componente responsável por fornecer a alimentação adequada para todo o sistema, garantindo assim o correto funcionamento dos componentes eletrônicos.

- **Módulo de comutação elétrica**

Este circuito é um módulo responsável pela comutação da energia da rede para o sistema de alimentação de redundância, acionando a bateria em caso de falha ou queda de energia.

**<h6 align="center"> Figura 2.1.20: Módulo de comutação elétrica</h6>**
![ICLAMPER](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/COMUT_MODULE.jpg)
	
- **Bateria 12V 9.6Ah**

O componente é responsável por alimentar o sistema em caso de queda da rede, funcionando assim como um sistema de redundância.

**<h6 align="center"> Figura 2.1.21: Bateria 12V 9.6Ah</h6>**
![ICLAMPER](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Bat_9800.png)
	
A bateria foi escolhida com base em um dos requisitos não funcionais, que visa eficiência energética. Ficou estabelecido que apenas um motor trabalhará por vez, resultando em uma corrente máxima de descarga de 8.5AH.

- **Fusível de cerâmica universal**:
    
Componente com intuito de interromper o circuito quando a corrente excede um valor especificado, protegendo os componentes de sobrecorrente.

**<h6 align="center"> Figura 2.1.22: Fusível cerâmico universal da CSE </h6>**
![ICLAMPER](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Fusivel_pi2.png?ref_type=heads)

####  **2.1.3. Lista de materiais (BOM)**

Com todo os componentes definidos, foi criada seguinte a lista de materiais.

**<h6 align="center"> Tabela 2.1.1: Lista de materiais </h6>**

|Item Nº                  |Componente                                       |Quantidade|Link de compra                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |Valor (Em reais)|
|-------------------------|-------------------------------------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
|1                        |Raspberry Pi 4     |1         |[link](https://www.mercadolivre.com.br/raspberry-pi-4-modelo-b-de-4-gb-de-ram/p/MLB33005781?item_id=MLB4866736610&from=gshop&matt_tool=56291529&matt_word=&matt_source=google&matt_campaign_id=14303413604&matt_ad_group_id=133074303519&matt_match_type=&matt_network=g&matt_device=c&matt_creative=584156655498&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=735098660&matt_product_id=MLB33005781-product&matt_product_partition_id=2268053647430&matt_target_id=pla-2268053647430&cq_src=google_ads&cq_cmp=14303413604&cq_net=g&cq_plt=gp&cq_med=pla&gad_source=1&gclid=CjwKCAjwqMO0BhA8EiwAFTLgINti6dDWheoImjnxlVJ-PZyMoyQ95cUoNE-XoSfRR9opunn5FBm65RoC738QAvD_BwE)                                                                                                                                                                                                                                                                                                                                                                                                                                                       |539,00          |
|2| Arduino Uno                                      |2         |[link](https://produto.mercadolivre.com.br/MLB-2905109587-compativel-arduino-uno-r3-atmega328-smd-sem-cabo-usb-_JM?matt_tool=40343894&matt_word=&matt_source=google&matt_campaign_id=14303413655&matt_ad_group_id=133855953276&matt_match_type=&matt_network=g&matt_device=c&matt_creative=584156655519&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=504460038&matt_product_id=MLB2905109587&matt_product_partition_id=2268053647630&matt_target_id=pla-2268053647630&cq_src=google_ads&cq_cmp=14303413655&cq_net=g&cq_plt=gp&cq_med=pla&gad_source=1&gclid=Cj0KCQjw_qexBhCoARIsAFgBlesx6Eu-A8ztNkdqmDi7ravQ1kU4FJtnh-UqzKX5r1jYqjbPw1KTMhYaAknBEALw_wcB)    |74,02           |
|4                        |CNC Shield V3                                    |1         |[link](https://www.mercadolivre.com.br/cnc-shield-v3-impressora-3d-reprap-grbl-para-driver-a4988/p/MLB33316259?pdp_filters=category:MLB99779#searchVariation=MLB33316259&position=5&search_layout=grid&type=product&tracking_id=98083b19-240c-4d30-8aaa-ec677872c828)                                                                                                                                                                                                                                                                                                                                                                                                             |36,01           |
|5                        |Driver A4988                                     |4         |[link](https://produto.mercadolivre.com.br/MLB-2631879695-04x-driver-controlador-motor-de-passo-a4988-arduino-pololu-_JM#position=6&search_layout=grid&type=item&tracking_id=763c6fef-bb93-4738-bfbc-211bbad588e7)                                                                                                                                                                                                                                                                                                                                                                                                                                                                |49,92           |
|6                            |Conversor AC/DC + Célula de carga                |1         |[link](https://produto.mercadolivre.com.br/MLB-1897571317-1-celula-de-carga-5kg-sensor-peso-1-modulo-hx711-arduino-_JM?matt_tool=63064967&matt_word=&matt_source=google&matt_campaign_id=14303413826&matt_ad_group_id=133431076203&matt_match_type=&matt_network=g&matt_device=c&matt_creative=584156655540&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=250230285&matt_product_id=MLB1897571317&matt_product_partition_id=2269730643578&matt_target_id=pla-2269730643578&cq_src=google_ads&cq_cmp=14303413826&cq_net=g&cq_plt=gp&cq_med=pla&gad_source=1&gclid=Cj0KCQjw_qexBhCoARIsAFgBletAY1BDcJEnrm7EARxCeFEbD2kdhxUED64HTLkQzIKHu7ivI6hB37gaAmMiEALw_wcB)|32,90           |
|7                        |Motor Nema17                                     |3         |[link](https://produto.mercadolivre.com.br/MLB-1739326340-3x-motor-de-passo-nema-17-42-kgfcm-3d-03-unidades-fortek-_JM#is_advertising=true&position=1&search_layout=stack&type=pad&tracking_id=9ec28417-f2ba-4e77-a8d1-0b88be8aa973&is_advertising=true&ad_domain=VQCATCORE_LST&ad_position=1&ad_click_id=MTkxOWY2ZTktYmI2NC00NmIyLTlkYWEtODQzZDBlMmVkY2I5)                                                                                                                                                                                                                                                                                                                       |182,00          |
|8                        |Módulo LDR                                       |3         |[link](https://produto.mercadolivre.com.br/MLB-1764711323-modulo-sensor-de-luz-ldr-digitalanalogico-arduino-node-rasp-_JM#position=2&search_layout=grid&type=item&tracking_id=f049c810-f1b6-4b00-b0fe-e840e71df168)                                                                                                                                                                                                                                                                                                                                                                                                                                                               |13,90           |
|9                       |DPS 20A iClamper Pocket                          |1         |[link](https://produto.mercadolivre.com.br/MLB-4209748250-iclamper-pocket-fit-3p-20a-protetor-surtos-transparente-_JM?matt_tool=45029758&matt_word=&matt_source=google&matt_campaign_id=14302215522&matt_ad_group_id=150145935567&matt_match_type=&matt_network=g&matt_device=c&matt_creative=649558500182&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=5330126597&matt_product_id=MLB4209748250&matt_product_partition_id=2266700345660&matt_target_id=pla-2266700345660&cq_src=google_ads&cq_cmp=14302215522&cq_net=g&cq_plt=gp&cq_med=pla&gad_source=1&gclid=Cj0KCQjw_qexBhCoARIsAFgBleuuDxL1lASoxU8c3KurctjJBOdB7eK8Ai_i3G3b73Y-CbPb0weETggaAmh1EALw_wcB)|49,99           |
|10                       |Step Down LM2596                                 |1         |[link](https://produto.mercadolivre.com.br/MLB-2921697027-2x-regulador-tenso-lm2596-conversor-dcdc-stepdown-_JM#is_advertising=true&position=3&search_layout=grid&type=pad&tracking_id=c5802ae1-b63c-407b-bf5c-16e71d4873f5&is_advertising=true&ad_domain=VQCATCORE_LST&ad_position=3&ad_click_id=MWIyNTM4ZGMtY2RkYi00ZTA4LWExZDAtNmY5ZTJhM2I5NTYx)                                                                                                                                                                                                                                                                                                                               |22,74           |
|11                       |Fonte Seasonic 300W                              |1         |[link](https://www.mercadolivre.com.br/fonte-atx-slim-ss300tfx-seasonic-dell-vostro-200-200s-outlet-cor-prata-110v220v/p/MLB20577575?pdp_filters=category:MLB6777#searchVariation=MLB20577575&position=12&search_layout=grid&type=product&tracking_id=9fa76275-42a0-4738-b2a5-579dfff95cb8)                                                                                                                                                                                                                                                                                                                                                                                       |110,00          |
|12                       |Módulo de comutação Automática Para Bateria yx850|1         |[link](https://produto.mercadolivre.com.br/MLB-3280663878-modulo-de-comutaco-automatica-para-bateria-5v-a-48v-_JM?matt_tool=18956390&utm_source=google_shopping&utm_medium=organic)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |26,00           |
|13                       |Bateria Lítio 12v 9.6 AH                         |1         |[link](https://produto.mercadolivre.com.br/MLB-1325327224-bateria-recarregavel-de-litio-12v-9800mah-_JM?variation=#reco_item_pos=0&reco_backend=recomm-platform_ranker_v2p&reco_backend_type=low_level&reco_client=vip-v2p&reco_id=595ded6a-88b1-42b6-bbd0-7871c4485735)                                                                                                                                                                                                                                                                                                                                                                                                          |375,00          |
|14                       |Chave liga/Desliga Verde                         |1         |[link](https://produto.mercadolivre.com.br/MLB-3468230388-chave-gangorra-redonda-6a250v-ligadesliga-com-led-verde--_JM#position=17&search_layout=stack&type=item&tracking_id=508bd11f-42fd-4ff0-8362-a190c07041ba)                                                                                                                                                                                                                                                                                                                                                                                                                                                                |11,04           |
|15                       |Push button Vermelho                             |1         |[link](https://produto.mercadolivre.com.br/MLB-1813077725-chave-fliperama-push-boton-acrilico-vermelho-com-microswitch-_JM#position=1&search_layout=stack&type=item&tracking_id=2865dab1-3a25-4022-a648-465532324c65)                                                                                                                                                                                                                                                                                                                                                                                                                                                             |11,98           |
|16                       |Webcam                                           |2         |[link]()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |100,00          |
|17                       |Fusível cerâmico universal CSE                                   |1         |[link](https://produto.mercadolivre.com.br/MLB-2193550157-fusivel-cermico-20a-para-microondas-127v220v-universal-_JM?matt_tool=50857357&matt_word=&matt_source=google&matt_campaign_id=14303413847&matt_ad_group_id=139467063288&matt_match_type=&matt_network=g&matt_device=m&matt_creative=584295662855&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=429229566&matt_product_id=MLB2193550157&matt_product_partition_id=2323209757127&matt_target_id=pla-2323209757127&cq_src=google_ads&cq_cmp=14303413847&cq_net=g&cq_plt=gp&cq_med=pla&gad_source=1&gclid=Cj0KCQjwpZWzBhC0ARIsACvjWRMf-S5Ugw7nmnvCDX0oipX1NwIwmaE4dciHHi3J2dI6hMSK88VxhR4aAu-zEALw_wcB) |9,00        |
|Quantidade total de peças|                                                 |28        |Custo Total  |2.026,50        |

<p style="text-align:center">Fonte: Autoria própria.</p>

###  **2.1.4. Arquitetura de Energia**

Para a confecção da Arquitetura de Energia foi utilizada a norma‌ ABNT NBR 5410 ‌([10](bib.md)). A base conta com duas versões de diagrama de proteção (blocos) e um digrama unifilar. O diagrama unifilar tem a função de representar o sistema elétrico de forma esquemática. Nele é possível identificar conexões elétricas, componentes eletrônicos e seus dispositivos de proteção, a seção dos fios e a tensão em cada estado do circuito. 

O do diagrama de proteção, que também pode ser chamado de diagrama de relés ou, ainda mais, de esquema de proteção, vem com o intuito de mostrar os dispositivos de proteção e os componentes que eles estão exercendo a função de proteger. Esse diagrama, feito no formato de blocos, foi confeccionado na fase inicial do projeto, visto isso, este é consideravelmente mais simplificado do que o diagrama unifilar. Com a evolução do projeto, ele ganhou outra versão, esta já indicando os componentes utilizados, nem como suas potências, correntes e tensão. 

####  **2.1.4.1. Diagrama de proteção**

O circuito é alimentado por uma fase de 220V e do neutro proveniente da rede, o neutro se conecta ao DPS iClamper pocket de 20A que realiza a proteção contra picos de tensão enquanto a fase se conecta ao disjuntor unipolar tipo C 230V 20A que também é um dispositivo de proteção cuja função é proteger equipamentos elétricos contra sobrecargas, este último também é conectado ao DPS pela fase. O dispositivo de proteção contra surtos como de praxe é instalado conectado ao aterramento que desempenha a função do descarregamento de uma sobrecarga para a terra. A fase e o neutro, provenientes do DPS, são conectados a fonte chaveada que converte a corrente alternada da rede para uma corrente contínua, este por sua vez também está ligado ao aterramento. Da fonte é ligada ao módulo de comutação responsável pelo controle do circuito (ligar e desligar), aonde parte tanto para a bateria que alimenta a esteira em caso de desconexão da rede como para a esteira inteligente.

**<h6 align="center"> Figura 2.1.23: Diagrama de proteção - primeira versão</h6>**

![Diagrama de Proteção V1](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_de_Prote%C3%A7%C3%A3o_Vers%C3%A3o_1.jfif?ref_type=heads)

<p style="text-align:center">Fonte: Autoria própria.</p>

**<h6 align="center"> Figura 2.1.24: Diagrama de proteção - segunda versão</h6>**

![Diagrama de Proteção V2](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/diagrama_de_prote%C3%A7%C3%A3o_terceira_vers%C3%A3o.PNG?ref_type=heads)






<p style="text-align:center">Fonte: Autoria própria.</p>

####  **2.1.4.2. Diagrama unifilar**

Desenvolvido no software CADe SIMU, o diagrama unifilar tem o objetivo de demostrar o sistema elétrico da esteira inteligente. Ele se faz necessário para mostrar uma visão planejada do sistema elétrico, dos circuitos eletrônicos e seus sistemas de proteção. Para a confecção do diagrama, foi necessário o levantamento de todos os componentes eletrônicos, bem como suas correntes, tensões e potência, o dimensionamento dos cabos elétricos e os sistemas de proteção. Além disso, também foram necessários o diagrama de consumo e o diagrama de conexões dos subsistemas, desenvolvidos pela eletrônica. Estes diagramas possibilitaram validar as dimensões e configurações do diagrama unifilar.

**<h6 align="center"> Figura 2.1.25: Diagrama unifilar </h6>**
![Diagrama Unifilar](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Diagrama_Unifilar.png?ref_type=heads)

<p style="text-align:center">Fonte: Autoria própria.</p>
