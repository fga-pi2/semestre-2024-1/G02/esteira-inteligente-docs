# Rascunho de Requisitos Geral

Foi definido o budget máximo do projeto, sendo definido em 2k600, ou 200 reais pessoas.

No nosso projeto terá uma mesa na qual o operador será obrigado a mover a bandeija para a célula de carga e para a câmera.
 
Segue um esboço de como vai ficar o projeto.

## To do list PI2

### Eletrônica
- Estudo dos requisitos
- Escolha dos componentes
- Integração dos componentes
- Montagem do sistema

### Estrutura
- Definição dos requisitos iniciais (funcionais e não funcionais);
- Esboço do modelo inicial;
- CAD dos componentes;
- Integração em CAD dos componentes;
- Integração em CAD dos subsistemas;
- Renderização para o relatório;
- Desenhos técnicos;
- Esquemáticos para o relatório
- Seleção de pontos críticos de esforços estruturais;
- Validação teórica com cálculos estruturais;
- Análise estrutural numérica (CAE), se necessário;
- Comparação teórico-numérico;
- Listagem de materiais e componentes;
- Orçamento de materiais e componentes;
- Aquisição dos materiais e componentes;
- Fabricação dos componentes;
- Montagem dos subsistemas;
- Integração completa da estrutura mecânica;
- Integração eletromecânica;
- Testes iniciais o funcionamento da estrutura;
- Correção dos componentes necessários;
- Testagem final;
- Validação completa da estrutura.

### Software
- Desenvolvimento do sistema de reconhecimento de imagem
- Desenvolvimento do banco de dados 
- Desemvolvimento de interface de interação humana

## Definiçao dos requisitos gerais

### Requisitos funcionais gerais:

1. O sistema deve liberar apenas os componentes dos kits o uso de montagem do kit solicitado

2. A câmera deverá fazer reconhecimento dos componentes colocados pelo operador e avisar  ele caso tenha algum componente faltante 

3. Deverá um segundo estado de checagem feito por peso

4. O sistema deve ter um sistema de armazenamento de facil entendimeto

5. O sistema deve ter um cadastro único de cada kit,  os componentes do kit deve contender as seguintes informações:

    - Dimensões de tamanho
    - Peso unitário
    - Um id int de 8 digitos
    - Nome do componente
    - Kits que utilizam ele

6. Os kits devem conter:

    - Quantide de cada componente
    - Quais componentes

7. O sistema deve funcionar sendo alimentado por uma bateria, sendoessa bateriaum sistema semelhante a no-break

8. O sistema deverá aceitar 5 componentes diferentes

9. Sistema de cadastro de operados, no caso apenas operadores cadastrados podem utilizar o equipamento.

10. A bandeja será levado pela a esteira durante o processo montagem

11. No final haverá um sistema de verificação, utilizando da balança e da câmera para fazer a checagem das peças. 

### Requisitos não funcionais gerais:

## Definição dos requisitos estruturais:
### Requisitos funcionais das estruturas:

1. As esteiras devem ter compartimentos que permitam o encaixe de cada peça dos kits;

2. Deve existir uma esteira para cada item dos kits;

3. É desejável que as esteiras sejam do mesmo tamanho e que consigam armazenar a mesma quantidade de itens;

4. No final das esteiras deve existir um compartimento apoiado na célula de carga para permitir a medição da massa total dos kits e realizar a verificação;

5. O suporte da célula de carga deve ser fixado de maneira que permita a queda das peças sem sofrer danos;

6. As esteiras devem ser inclinadas de forma que as peçam possam cair no compartimento de célula de carga sem impedimentos;

7. Acima do compartimento de célula de carga deve existir um suporte para fixar a câmera de verificação;

8. O suporte de câmera deve ser fixo e impedir movimentação acidental;

9. As esteiras devem ser capazes de se movimentar sem impedimento mecânico;

10. As esteiras devem ser capazes de suportar o peso das peças sem perder a mobilidade;

11. Após a célula de carga deve existir um suporte que permita a embalagem facilitada dos kits para o operador;

12. Os itens tem que ser dos mesmos tamanhos limitantes;

13. O compartimento de célula de carga precisa ter paredes para evitar que os itens escapem;

14. A estrutura completa deve ter espaços para encaixar os componentes elétricos e eletrônicos.

### Requisitos não funcionais das estruturas:

1. É desejável que as esteiras tenham baixa necessidade de manutenção mecânica;

2. As esteiras devem ser de material durável;

3. É desejável que as esteiras funcionem sem falhas ou interrupções mecânicas;

4. A esteira deve conter materiais e componentes com padrões encontrados na indústria;

5. A estrutura mecânica deve ser robusta e leve;

6. É desejável que tenham poucas peças usináveis para reduzir o custo do projeto;

7. Os roletes e a lona da esteira devem ser de material aderente.