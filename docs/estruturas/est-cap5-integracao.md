## **4. Detalhamento de atividades para integração estrutural**

Desde a concepção do projeto, foram idealizadas metodologias para a aquisição, construção e integração da parte estrutural. Essas metodologias visam facilitar a construção, o transporte, a desmontagem rápida, a manutenção e o manuseio pelos operadores, sem deixar de considerar os custos de mercado. Nosso objetivo é tornar o produto não apenas eficiente em sua função, mas também viável em termos de custo e produção em série.

A integração estrutural divide-se em três partes. A primeira parte é o projeto estrutural básico de suportes, que dá forma que a construção suporte todos os periféricos. A segunda parte é o próprio subsistema da esteira, constituído por seus mecanismos, suportes, rolamentos e cintas. A terceira e última parte é o sistema estrutural que suporta os periféricos do sistema de software e eletrônica, incluindo suportes para tela, câmera e célula de carga.

Para a parte estrutural básica, utilizamos metalon de 20x20mm com parede de 0,9mm, visando uma estrutura barata e fácil de fabricar, considerando a inexperiência geral dos alunos no processo prático de fabricação. Com demonstrações do corpo docente, aplicamos as técnicas aprendidas para utilizar serra de bancada, furadeira de bancada, solda MIG e outras ferramentas, fundamentais para a concepção das peças e a montagem estrutural.

O processo da primeira parte foi dividido nas seguintes etapas:

1. Limpeza das barras de metalon utilizadas: Para garantir melhor manuseio e segurança, foi realizada uma limpeza do óleo externo presente nas barras de metalon, óleo este que é usado a fim de evitar corrosão durante o armazenamento do material. Essa limpeza também visou melhorar o manuseio, prevenindo acidentes como escorregamentos durante os cortes, além de proporcionar um acabamento estético mais limpo ao produto.

2. Medição e corte: Com base nos desenhos técnicos do suporte de integração presentes na ([Seção de Desenhos Técnicos](../estruturas/est-cap6-dts.md)) produzidos no primeiro ponto de controle, foi feita a medição das peças necessárias para a fabricação. Utilizamos uma serra de bancada para cortar as peças, garantindo máxima precisão nas medidas do produto final.

    **<h6 align="center"> Figura 4.1: Barras de Metalon limpas e cortadas </h6>**

    ![metalon](../assets/images/metalon.jpeg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

3. Soldagem: Com o auxílio do professor Rhander, utilizamos a técnica de soldagem MIG para unir as estruturas mencionadas anteriormente, exceto as barras de estabilização, que foram parafusadas nos suportes das esteiras para facilitar desmontagens futuras para manutenção e/ou transporte.

    **<h6 align="center"> Figura 4.2: Barras de Metalon soldadas. </h6>**

    ![solda](../assets/images/barras-soldadas.jpg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

Durante as etapas anteriores, também empregamos técnicas básicas de acabamento, utilizando lima e outros equipamentos como esmeril elétrico, para garantir a precisão dos cortes e a qualidade das montagens.

Para compor a segunda parte da estrutura, utilizamos materiais plásticos como PVC para os roletes das esteiras, ABS para a estrutura impressa dos mancais e para a conexão de canto, além de componentes comerciais como rolamentos 608zz e barras roscáveis de aço para os eixos.

O processo da segunda parte foi dividido nas seguintes etapas:

1. Impressão 3D das peças em ABS: Utilizando os CADs das peças plásticas, realizamos a impressão 3D com impressoras de parceiros, empregando ABS devido às suas ótimas características de robustez mecânica e térmica.


    **<h6 align="center"> Figura 4.3: Peças de impressão 3D em ABS. </h6>**

    ![abs](../assets/images/abs-3d.jpg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

2. Corte e colagem: Realizamos cortes precisos dos tubos de PVC que constituem os roletes das esteiras, conforme medidas e desenho técnico disponíveis na [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts.md). Utilizamos uma serra de bancada para os cortes e cola de PVC para garantir aderência e evitar deformações durante o uso, o que poderia afetar o funcionamento dos motores e a precisão do algoritmo de controle da esteira.

3. Fixação dos eixos: Utilizando barras roscáveis de aço cortadas na serra de bancada, posicionamos cada barra no respectivo eixo, acoplando-a nos rolamentos e alinhando os eixos com a esteira. A fixação foi feita com porcas e pinos de frenagem.

    **<h6 align="center"> Figura 4.4: Roletes fabricados. </h6>**

    ![rolete](../assets/images/rolete.jpeg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

4. Furação: Para a fixação ideal dos mancais na estrutura metálica, utilizamos uma furadeira de bancada para fazer três furos em cada região de encaixe dos mancais. Além disso, criamos um trilho para o primeiro mancal com uma esmerilhadeira, já que a broca da fresadora foi danificada na primeira tentativa, exigindo um método mais artesanal para os trilhos.

    **<h6 align="center"> Figura 4.5: Furação da estrutura metálica. </h6>**

    ![furacoes](../assets/images/furacoes.jpg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

5. Cinta: Para a fabricação da cinta das esteiras, lixamos, cortamos e colamos borracha de 90mm por 1500mm. A cura da colagem durou cerca de 24 horas.

    **<h6 align="center"> Figura 4.6: Rolo da borracha utilizada. </h6>**

    ![borracha](../assets/images/borracha.jpg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

6. Integração: Após concluir as etapas anteriores, realizamos a integração das peças. Os suportes das esteiras com e sem motor foram encaixados na estrutura soldada, especificamente nos suportes impressos em ABS de conexão de canto e nos joelhos. Alinhamos e apertamos os parafusos para fixar todos os mancais. Após a integração dos mancais, encaixamos e alinhamos cada rolete a partir do giro do motor de passo. Finalmente, encaixamos a cinta já colada e a esticamos, validando seu funcionamento.

    **<h6 align="center"> Figura 4.7: Uma esteira integrada sem motor. </h6>**

    ![esteira](../assets/images/uma-esteira.jpeg)

    <p style="text-align:center">Fonte: Autoria própria.</p>

A terceira parte da construção consiste na montagem dos elementos estruturais que darão suporte aos componentes eletrônicos e no refinamento estético do projeto. Tal fase foi finalizada simulataneamente à integração dos demais setores de maneira e garantir adequação da estrutura às necessidades de cada um.

1. Câmera: A partir de um suporte articulável, a câmera que aponta para a balança pôde ser inserida de maneira a garantir boa visualização para atuação da inteligência artificial.

2. Funil e balança: Construídos com uma chapa de zinco de 1mm, o funil foi cortado e dobrado para obter um formato adequado e rígido, garantindo leveza e resistência para a aplicação. A célula de carga foi acomodada em uma pequena caixa de acrílico com paredes de ACM, proporcionando resistência e suporte às peças que devem cair sobre ela.

3. Caixa de comando e conduítes: Devido à finalidade deste subsistema e à sua complexidade de construção, optou-se por utilizar um gabinete de quadro elétrico. Essa escolha visa diminuir o tempo de fabricação e aumentar a confiabilidade no acondicionamento dos equipamentos eletroeletrônicos.

De modo geral, a terceira e última parte da construção da estrutura envolveu a integração com outros subsistemas, como energia e eletrônica. Após a montagem, foi necessário iniciar o acabamento, que consistiu no tratamento do material, removendo a oxidação e ferrugem adquiridas ao longo do tempo, e na pintura de toda a estrutura para melhorar a apresentação estética e a conservação do material.

**<h6 align="center"> Figura 4.8: Integração final da esteira. </h6>**

![esteira](../assets/images/esteira-completa.jpeg)

<p style="text-align:center">Fonte: Autoria própria.</p>