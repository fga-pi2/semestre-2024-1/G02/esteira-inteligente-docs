# **1. Requisitos de Estruturas**{#requisitos-estruturas}

**<h6 align="center"> Tabela 1.1: Requisitos funcionais para a Estrutura do produto</h6>**

| Requisito |                  Descrição                   |                                                                      Observações                                                                      |
| :-------: | :------------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------: |
|   RFE01   |              Divisão da esteira              |                                   As esteiras devem ter compartimentos que permitam o encaixe de cada peça dos kits                                   |
|   RFE02   |     Limitação da quantidade de esteiras      |                                                   Deve existir uma esteira para cada item dos kits.                                                   |
|   RFE03   |          Padronização das esteiras           |                       É desejável que as esteiras sejam do mesmo tamanho e que consigam armazenar a mesma quantidade de itens.                        |
|   RFE04   |       Suporte sobre a célula de carga        |                        O suporte da célula de carga deve ser fixado de maneira que permita a queda das peças sem sofrer danos.                        |
|   RFE05   |        Queda das peças para a balança        |    As esteiras devem ser conectadas por um funil inclinado de forma que as peçam possam cair no compartimento de célula de carga sem impedimentos.    |
|   RFE06   |        Observação da célula de carga         |                         Acima do compartimento de célula de carga deve existir um suporte para fixar a câmera de verificação.                         |
|   RFE07   |              Fixação do suporte              |                                          O suporte de câmera deve ser fixo e impedir movimentação acidental.                                          |
|   RFE08   |          Constância da movimentação          |                                       As esteiras devem ser capazes de se movimentar sem impedimento mecânico.                                        |
|   RFE09   |             Mobilidade com peso              |                                  As esteiras devem ser capazes de suportar o peso das peças sem perder a mobilidade.                                  |
|   RFE10   |          Limitação dos componentes           |                                                 Os itens tem que ser dos mesmos tamanhos limitantes.                                                  |
|   RFE11   | Manter os componentes em sua posição correta | O compartimento de célula de carga precisa ter paredes para evitar que os itens escapem, essas devem ser transparentes para a visualização da câmera. |
|   RFE12   |           Componentes eletrônicos            |                              A estrutura completa deve ter espaços para encaixar os componentes elétricos e eletrônicos.                              |

<p style="text-align:center">Fonte: Autoria própria.</p>

**<h6 align="center"> Tabela 1.2: Requisitos não funcionais para a Estrutura do produto</h6>**

| Requisito |           Descrição           |                                     Observações                                     |
| :-------: | :---------------------------: | :---------------------------------------------------------------------------------: |
|  RNFE01   |   Necessidade de manutenção   |    É desejável que as esteiras tenham baixa necessidade de manutenção mecânica.     |
|  RNFE02   |         Durabilidade          |                     As esteiras devem ser de material durável.                      |
|  RNFE03   | Permanência do funcionamento  |     É desejável que as esteiras funcionem sem falhas ou interrupções mecânicas.     |
|  RNFE04   | Compatibilidade com o mercado | A esteira deve conter materiais e componentes com padrões encontrados na indústria. |
|  RNFE05   |       Robustez e leveza       |                    A estrutura mecânica deve ser robusta e leve.                    |
|  RNFE06   |       Redução de custos       |   É desejável que tenham poucas peças usináveis para reduzir o custo do projeto.    |
|  RNFE07   |     Aderência da esteira      |           Os roletes e a lona da esteira devem ser de material aderente.            |

<p style="text-align:center">Fonte: Autoria própria.</p>
