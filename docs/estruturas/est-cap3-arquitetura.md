# **2. Arquitetura de Estruturas**

Após a definição de requisitos funcionais e não funcionais do projeto estrutural presentes nas Tabelas 1.1 e 1.2 da [Seção de Requisitos](../estruturas/est-cap2-requisitos.md), a arquitetura da estrutura (Figura 2.1) pôde ser desenvolvida.

**<h6 align="center"> Figura 2.1: Esteira Inteligente </h6>**

![esteiras-e-pecas](../assets/images/estruturas/esteira-final-quadrado.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

Assim, o projeto estrutural da Esteira Inteligente tem como objetivo a criação de um sistema que controla a quantidade de peças nos kits de produto e valida se as peças estão corretas nestes. Para isso, foram desenvolvidos três modelos de peças em MDF que foram cortadas em CNC, unidas com cola contato e pintadas com tinta spray e podem ser visualizadas na Figura 2.2 abaixo. 

**<h6 align="center"> Figura 2.2: Peças dos Kits </h6>**

![esteiras-e-pecas](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-frontend/-/raw/main/public/kit1.jpg?ref_type=heads)

<p style="text-align:center">Fonte: Autoria própria.</p>

Especificamente, as peças azuis têm 12g, as amarelas 22g e as quadradas 32g. Além disso, a largura máxima de cada peça foi definida como 5cm para adequar-se às divisórias da cinta que possuem 10cm. A Figura 2.3 contém as peças dispostas na cinta em cada divisória.

**<h6 align="center"> Figura 2.3: Peças, divisórias, mancal e conexão de canto </h6>**

![esteiras-e-pecas](../assets/images/estruturas/canto-final.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>


Para facilitar a compreensão, a Figura 2.4 contém números que representam cada subsistema. Também, os desenhos técnicos contidos na [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts) podem auxiliar no entendimento do projeto.

**<h6 align="center"> Figura 2.4: Subsistemas numerados </h6>**

![esteiras-e-pecas](../assets/images/estruturas/esteira-cad.png)

<p style="text-align:center">Fonte: Autoria própria.</p>

Em que,
1. Subsistema da esteira;
2. Suporte de integração;
3. Funil;
4. Subsistema da balança.

Inicialmente, os roletes das esteiras foram desenvolvidos com canos e caps de PVC para reduzir os custos do projeto. Também, como a cinta deve ser de material aderente que garanta a transferência do torque e o não deslizamento das peças, o material escolhido foi a borracha. Sendo assim, tiras de 9cm de borracha com 2mm foram cortadas e coladas com cola contato, bem como as divisórias de borracha de 1cm cada.

Além disso, os mancais das esteiras foram projetados em plástico ABS de impressão 3D. Ressalta-se que os mancais foram empregados para apoiar o eixo dos roletes e acoplar os motores de passo que acionarão as esteiras de maneira a garantir um controle preciso da queda das peças, conforme representado nas Figuras 2.5 e 2.6.

**<h6 align="center"> Figura 2.5: Vista superior do rolete acoplado ao motor </h6>**

![esteiras-e-pecas](../assets/images/estruturas/motor-e-rolete.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

**<h6 align="center"> Figura 2.6: Vista diagonal do rolete acoplado ao motor </h6>**

![esteiras-e-pecas](../assets/images/estruturas/motor-quadrado.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

Ao final das esteiras, é utilizado um funil de zinco galvanizado que direciona as peças até a balança e pode ser melhor visualizado nas Figuras 2.1 e 2.4. Para melhor descrever, a balança é o conjunto de uma caixa de acrílico que armazena um prato de medição móvel de material plástico fixado na célula de carga - vista explodida do conjunto pode ser acessada nos desenhos técnicos da [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts) e a Figura 2.7 contém o conjunto montado.

**<h6 align="center"> Figura 2.7: Balança e peças </h6>**

![esteiras-e-pecas](../assets/images/estruturas/balanca-quadrada.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

Além da balança, existe a inspeção quantitativa e qualitativa das peças pela câmera instalada em suporte articulável comercial fixado no centro da base de integração das esteiras. Adicionalmente, uma câmera extra pode ser alocada externamente para visualizar a queda das peças e utilizar a inteligência artificial que permite a contagem destas.

Por conseguinte, conduítes foram instalados na estrutura para isolar a cablagem eletrônica, botões estratégicos para melhorar o uso do operador e uma caixa de comando de fácil acesso para manutenção e ajustes (Figura 2.8).

**<h6 align="center"> Figura 2.8: Caixa de comando </h6>**

![esteiras-e-pecas](../assets/images/estruturas/caixa-comando-estrutura.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

Finalmente, a estrutura que integra todos os componentes foi desenvolvida com barras de metalon de seção quadrada (20x20 milímetros e parede com espessura de 0,9 milímetros) que podem ter suas dimensões alteradas conforme a disponibilidade no mercado local. Adicionalmente, as conexões de funil e de canto foram projetadas para desenvolvimento em plástico ABS com impressão 3D de maneira a garantir os formatos projetados e facilitar a reposição das peças. Ressalta-se que a Figura 2.3 ilustra a conexão de canto projetada pelo grupo em uso.  

Os desenhos técnicos presentes na [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts), bem como o orçamento presente na Tabela 4.1 da [Seção de Projeto e Construção Geral](../geral/cap4-projeto-e-construcao.md), complementam as informações supracitadas.
