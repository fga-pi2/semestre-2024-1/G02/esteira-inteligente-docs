# **5. Desenhos Técnicos Mecânicos**

Todos os desenhos técnicos foram realizados respeitando o primeiro diedro recomendado pela [ABNT NBR 17006](bib.md). A fim de garantir uma melhor resolução, os pdfs dos desenhos podem ser visualizados [aqui](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-estruturas/-/blob/main/DTs/.pdf/00-DTs-Esteira-Inteligente.pdf). Ressalta-se que os componentes sem desenho técnico são comerciais e foram adquiridos conforme disponibilidade do mercado.

## **5.1. Visão geral**

![explodida-geral](../assets/images/dts/01-sistema-completo-explodido-v2.jpg)

## **5.2. Subsistema da esteira**

![explodida-esteira](../assets/images/dts/02-esteira-completa-explodida-v2.jpg)

![explodida-rolete](../assets/images/dts/03-explodida-rolete-motor.jpg)

![mancal](../assets/images/dts/04-mancal-v2.jpg)

![cano](../assets/images/dts/05-cano-v2.jpg)

![cap](../assets/images/dts/06-cap-v2.jpg)

## **5.3. Subsistema de integração**

![explodida-integracao](../assets/images/dts/07-integracao-explodida-v2.jpg)

![suporte-integracao](../assets/images/dts/08-suporte-integracao-v2.jpg)

![barra-estabilizacao](../assets/images/dts/09-barra-estabilizacao.jpg)

![suporte-esteira-com-motor](../assets/images/dts/10-suporte-esteiras-com-motor-v2.jpg)

![suporte-esteira-sem-motor](../assets/images/dts/11-suporte-das-esteiras.jpg)

![joelho](../assets/images/dts/12-joelho-v2.jpg)

![barra-angulo](../assets/images/dts/13-barra-angulo.jpg)

![barra-inferior](../assets/images/dts/14-barra-inferior.jpg)

![conector-canto](../assets/images/dts/15-conexao-canto.jpg)

![conector-funil](../assets/images/dts/16-conector-funil-v2.jpg)

![mesa](../assets/images/dts/17-mesa.jpg)

![suporte-balanca](../assets/images/dts/18-suporte-balanca.jpg)

![funil](../assets/images/dts/19-funil.jpg)

## **5.4. Subsistema da balança**

![conjunto-balanca](../assets/images/dts/20-conjunto-balanca.jpg)






