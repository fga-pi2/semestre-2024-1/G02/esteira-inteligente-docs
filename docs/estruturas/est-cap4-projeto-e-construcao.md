# **3. Projeto e Construção de Estruturas**

O projeto estrutural da Esteira Inteligente pode ser dividido em alguns subsistemas, sendo eles:

- Subsistema da esteira;
- Subsistema da célula de carga;
- Subsistema de suporte de integração;
- Subsistema de controle.

O subsistema da esteira abrange todos os componentes essenciais para sua constituição, ou seja, o conjunto rolete, o conjunto motor, a cinta, a polia e a correia.

Por conseguinte, o subsistema da célula de carga considera a célula de carga em si, a caixa de acrílico e o prato de medição.

No que tange o subsistema de suporte, tem-se todo os componentes necessários para sustentar e integrar os demais componentes. Sendo assim, ele é composto de barras e conexões que interligam todos os elementos.

Por fim, o subsistema de controle é constituído pelo suporte para as câmeras, a tela, o mouse, o teclado e a caixa de comando que armazena todos os componentes eletrônicos.

Portanto, vale ressaltar que existem critérios técnicos que embasaram as tomadas de decisão de projeto. Assim, questões como disponibilidade de material no mercado, custos associados, propriedades mecânicas e esforços estruturais foram levados em consideração para as escolhas. Tal discussão - que resulta nos desenhos técnicos presentes na [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts.md) - será abordada nos tópicos subsequentes.

## 3.1. **Métodos de Fabricação e Materiais**

Inicialmente, os componentes e seus respectivos materiais estão descritos detalhadamente nos desenhos técnicos da [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts.md).

Os materiais foram selecionados com base nas exigências do mercado e em considerações de custo, visando facilitar a aquisição e montagem do produto de maneira eficiente. Destaca-se a adoção da tecnologia de impressão 3D para a fabricação de alguns componentes, o que elimina a necessidade de usinagem e reduz consideravelmente os custos de produção. Componentes como mancais, conexões de canto e de funil - todos eles com elevado grau de complexidade na fabricação - foram produzidos com ABS, aproveitando sua resistência e as características de preenchimento específicas que foram aplicadas. Na Figura 2.3 da [Seção de Arquitetura de Estruturas](../estruturas/est-cap3-arquitetura.md), tem-se o resultado da impressão do mancal e do encaixe de canto produzidos em ABS.

No que tange às estruturas de integração, elementos como a mesa, o suporte em V da balança, a barra de ângulo e as barras de suporte foram construídos com metalon. Este material é de fácil manipulação e possui custo reduzido em comparação com alternativas como alumínio ou aço. Além disso, é estruturalmente suficiente e não compromete a robustez necessária para suportar os esforços mecânicos esperados no produto, proporcionando uma solução econômica e resistente.

Dessa maneira, o perfil de metalon escolhido foi de 20x20x0.9mm, e foram obtidos aproximadamente 18 metros deste material na Ferro e Aço do Gama. A produção da estrutura iniciou-se com os cortes do metal, utilizando a serra disponível no laboratório do LDTEA, e com suporte dos desenhos técnicos apresentados na [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts.md). Posteriormente, os furos foram marcados e realizados com a furadeira de bancada. As soldas foram feitas com a máquina de solda inversora (MIG) e os componentes furados foram presos com os parafusos adequados. Na Figura 3.1.1, é possível visualizar o resultado da estrutura principal antes do acabamento observado na Figura 2.1 da [Seção de Arquitetura de Estruturas](../estruturas/est-cap3-arquitetura.md). Resumidamente, as barras do suporte foram soldadas e os encaixes esteira-suporte foram parafusados.

**<h6 align="center"> Figura 3.1.1: Suporte de integração </h6>**

![esteira-fisica](../assets/images/esteira.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

Além disso, os roletes foram confeccionados com cano de PVC para água, com 40mm de diâmetro, fechados com os caps correspondentes e porcas M8, responsáveis por manter a barra roscada interna no lugar, conforme definido pelos desenhos da [Seção de Desenhos Técnicos](../estruturas/est-cap6-dts.md).

O funil foi confeccionado em zinco galvanizado devido à sua facilidade de manuseio e moldagem, além dos benefícios que oferece, como o baixo coeficiente de atrito, tornando-o ideal para a aplicação específica que requer deslizamento das peças.

**<h6 align="center"> Figura 3.1.2: Funil feito em zinco.</h6>**

![funil-fisico](../assets/images/funil-real.jpeg)

<p style="text-align:center">Fonte: Autoria própria.</p>

Quanto à cinta, após pesquisa no mercado brasiliense, optou-se por utilizar borracha, pois oferece resistência suficiente para suportar os esforços de tração ao longo de múltiplos ciclos sem comprometer suas propriedades elásticas. Além disso, a borracha possui alto coeficiente de atrito estático e dinâmico, fator fundamental para manter a integridade do funcionamento mecânico da esteira, evitando deslizamentos, quedas de peças ou outros problemas relacionados à movimentação indesejada das peças. Cerca de 2 metros dessa borracha foram obtidos na Pioneira da Borracha e cortados em larguras de 10 centímetros com estilete e régua. A união das extremidades foi realizada com cola de contato, que garantiu resistência de tração suficiente sem descolar.

A fim de curiosidade, o grupo enfrentou questões de deslizamento da cinta ao longo da rolagem da esteira que pode ser observada neste [vídeo](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-estruturas/-/blob/main/esteira-video.mp4?ref_type=heads). Para sanar o problema, uma coroa de PVC foi inserida no centro dos roletes das extremidades de maneira a simular um rolete comercial bicônico. Além disso, a largura das cintas foram reduzidas de 10cm para 9cm e a velocidade de rotação do motor de passo foi ajustada de forma a mitigar tal problema.

Finalmente, o espaço do motor e o sistema responsável por tracionar a esteira foram desenvolvidos. Ambos contam com furos contínuos que criam um efeito de trilho, permitindo a movimentação em um grau de liberdade. A Figura 3.1.2 e a Figura 3.1.3 representam esses sistemas antes da finalização e do acabamento. Destaca-se que há um suporte do motor por esteira e, para cada esteira, os dois mancais da extremidade funcionam como esticadores. Adicionalmente, ressalta-se que os trilhos dos motores finais foram cortados a laser e dobrados com auxílio da loja Mineiraço de Samambaia.

**<h6 align="center"> Figura 3.1.2: Espaço do motor preliminar </h6>**

![espaco-motor](../assets/images/suporte-motor-fisico.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

**<h6 align="center"> Figura 3.1.3: Mancal tracionador </h6>**

![mancal-tracionador](../assets/images/esticador.jpg)

<p style="text-align:center">Fonte: Autoria própria.</p>

A [Seção de Integração de Estruturas](..estruturas/est-cap5-integracao.md) conta com todas as atividades detalhadas que permitiram o avanço e construção da estrutura da esteira inteligente.

Abaixo, na Tabela 3.1.1, tem-se as principais propriedades mecânicas dos materiais escolhidos para o projeto estrutural. Bem como, na [Seção de Arquitetura de Estruturas](../estruturas/est-cap3-arquitetura.md), tem-se os valores pesquisados para cada respectivo material. Destaca-se que tais dados podem ser usados de argumento para suas escolhas.

**<h6 align="center"> Tabela 3.1.2: Propriedades dos Materiais </h6>**

|    **MATERIAL**   	| **MASSA ESPECÍFICA [g/cm3]** 	| **MÓDULO DE ELASTICIDADE [GPa]** 	| **MÓDULO DE CISALHAMENTO [GPa]** 	| **TENSÃO DE ESCOAMENTO [MPa]** 	| **TENSÃO DE RUPTURA [MPa]** 	| **ALONGAMENTO ATÉ RUPTURA [%]** 	|
|:-----------------:	|:---------------------------:	|:--------------------------------:	|:--------------------------------:	|:------------------------------:	|:---------------------------:	|:-------------------------------:	|
|        MDF        	|             0,75            	|                 4                	|                2,5               	|                -               	|              18             	|                -                	|
|        ABS        	|         0,885 - 3,5         	|           0,778 - 21,2           	|                 -                	|           2,00 - 77,0          	|         2,60 - 73,1         	|                -                	|
|     PMMA e PVC    	|         1,26 - 1,45         	|                 -                	|                 -                	|           39,3 - 40,7          	|         42,0 - 46,9         	|                -                	|
|      Borracha     	|            0,950            	|                 -                	|                 -                	|                -               	|         7,00 - 28,0         	|            125 - 850            	|
|      Metalon      	|         0,451 - 8,26        	|            13,8 - 235            	|            78,0 - 82,7           	|           275 - 3340           	|          161 - 3200         	|           0,500 - 30,0          	|
| Zinco galvanizado 	|             7,80            	|                200               	|                80                	|                -               	|              -              	|                -                	|
|       Latão       	|         0,969 - 8,89        	|            75,8 - 121            	|            26,9 - 44,1           	|           34,5 - 683           	|         20,7 - 1030         	|                -                	|
|    Aço carbono    	|             7,80            	|             190 - 215            	|                80                	|               290              	|             435             	|                -                	|

<p style="text-align:center">Fonte: <a href="bib.md"> MakeltFrom, 2024 e MatWeb, 2024. </a></p>


Para o desenvolvimento de um projeto estruturalmente seguro, foram levantados alguns pontos críticos para estudo com cautela. Além de garantir segurança estrutural, os cálculos permitem otimização de geometria para redução de material e, consequentemente, massa e custo. Abaixo, tem-se os pontos críticos levantados pelo grupo:

- Suporte do motor: esforços relacionados à rotação do motor;
- Mancal: esforços relacionados à rotação do motor;
- Correia e cinta: esforços de tração;
- Conjunto rolete: esforços de flexão e torque;
- Fixação mancal e suporte do motor: esforços relacionados à rotação do motor;
- Fixação conjunto caixa e célula de carga: esforços de movimentação e queda de peças. 

Ressalta-se que os pontos críticos foram calculados e estão descritos na [Seção de Cálculos Estruturais](../estruturas/est-cap7-calculos-estruturais.md). 