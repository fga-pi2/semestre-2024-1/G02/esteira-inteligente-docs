# **Apêndice 07 - Memorial de decisões de desenvolvimento de software**

## 1. Introdução
Este apêndice pode ser utulizado para detalhar os cálculos, simulações,
etc. relacionados ao projeto de elementos dos subsistema. Deve-se
registrar as principais decisões realizadas durante a realização do
projeto, particularmente as relativas à escolha de tecnologias para o
desenvolvimento do software, arquiteturas do software, inovações ou
arquitetura da informação.

Deverá ser registrado o histórico das decisões tomadas, um tópico para
cada decisão

Ex:

01.01.2022 \--\> (inovação) Decidiu-se por utilizar redes neurais
artificiais como proposta de inovação para que se pudesse, a partir dos
dados coletados pelo projeto, identificar padrões e favorecer decisões
quanto a \.... (detalhes específicos do projeto)

\...

\...

02.02.2022 -\> (inovação) Decidiu-se desistir da utilização de redes
neurais artificiais. Optou-se por substituí-la por IOT (Internet das
Coisas), a partir da integração com alunos da Engenharia Eletrônica.

# **Apêndice 08 - Plano de testes funcionais do produto**

Esta seção apresenta o plano de testes do produto.

# **Apêndice 09 - Manual de montagem e uso do produto**

Esta seção apresenta o manual de uso do produto. De forma opcional, este
manual pode ser entregue em arquivo separado.

# **Apêndice 10 - Manual de manutenção do produto**

Esta seção apresenta o manual de uso do produto. De forma opcional, este
manual pode ser entregue em arquivo separado.

# **Apêndice 11 - Testes e Manual de instalação do software**

Esta seção apresenta a estratégia realizada para efetivação dos testes
de software(unitário, integração, funcional, aceitação, desempenho,
carga, vulnerabilidade ou outros) manual de instalação e uso dos
produtos de software. De forma opcional, o manual de uso pode ser
entregue em arquivo separado.

# **Apêndice 12 - Autoavaliação dos integrantes**

**<h6 align="center"> Tabela 1: Autoavaliação Ponto de Controle 1</h6>**
|                    Nome                    | Matrícula | Nota  |                                                                                                                                                                                                                                                     Descrição da Contribuição                                                                                                                                                                                                                                                      |
| :----------------------------------------: | :-------: | :---: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|        Ana Paula de Souza de Araujo        | 190024577 |   8   |         Coordenação e organização das atividades do grupo de estrutura, escrita e organização do relatório, pesquisa de referencial industrial para desenvolvimento do produto, auxílio nas atividades dos demais componentes do grupo de estruturas, CADs dos componentes do conjunto da esteira, integração em CAD do sistema completo, todos os desenhos técnicos do PC1, renderizações para o PC1, definição de materiais, definição de requisitos funcionais e não funcionais e desenvolvimento da EAP de estruturas.         |
|      Caio Sampaio de Carvalho e Silva      | 190085321 |   8   |                                                                                                                                                                  Dimensionamento dos cabos, dimensionamento da bateria, escrita de relatório, escolha dos subsistemas de controle, diagrama de proteção e unifilar, pesquisas de normas (NBR 5410 e ABNT 14039).                                                                                                                                                                   |
| Fernanda Noronha Coelho dos Santos Marques | 190027762 |   8   |                                                                                                                                                         Desenvolvimento das peças dos kits em CAD, CAD do módulo de célula de carga, organização do gitlab, escrita de relatório, acompanhamento da qualidade do projeto, verificação do andamento, alinhamento da equipe.                                                                                                                                                         |
|      Joel Jefferson Fernandes Moreira      | 190043083 |   8   |                                                                        Concepção da idea do grupo; Coleta de requisitos com processistas industriais; Auxilio na líderança do grupo de energia e eletrônica; Auxilio na utilização dos recursos do GitLab para a execução das atividade de energia e eletrônica; Auxilio no desenvolvimento da EAP de energia e eletrônica; Desenvolvedor dos esquemáticos eletrônicos; Auxilio na montagem dos diagramas;                                                                         |
|        Lucas Alexandre Gama Correia        | 180149741 |   8   |                                                                                                      CAD dos componentens e conexões do suporte de integração do sistema completo, CAD dos componentes que transferem o torque para a cinta, definição do sistema de torque, definição dos materiais, pesquisa de referencial industrial para desenvolvimento das peças, cálculo do torque requerido e escrita do relatório.                                                                                                       |
|        Michael Emanuel Silva Costa         | 190035528 |   9   |                                                                                                                      Responsável pelo dimensionamento e escolha da bateria, dimensionamento de cabos dos circuitos eletrônicos, confecção do diagrama unifilar, confecção do diagrama de proteção, aplicação das normas ABNT NBR 5410 e ABNT 14039 no sistema elétrico do projeto e escrita do relatório.                                                                                                                      |
|      Mauricio Maiyson Hoffman de Melo      | 160138574 |   9   | Responsável por gerenciar, dividir e organizar as atividades relacionadas ao grupo de Eletrônica/Energia. Criação da EAP, desenvolvimento do cronograma de atividades, participação no  desenvolvimento de diagramas e esquemáticos do subsistemas eletrônicos, pesquisa e criação da lista de componentes (BOM), auxilio no dimensionamento de bateria, e diagrama de proteção. Participação na escrita do relatório do PC1 e referencial teórico, suporte aos outros integrantes do grupo no desenvolvimento de suas atividades. |
| Danilo Domingo Vitoriano Silva | 180015311 | 8 | Responsável por gerenciar, dividir e organizar as atividades relacionadas ao grupo de Software. Criação da EAP, Documento de Visão, Diagrama de Componente, Backlog, Requisitos de Software, Documento de Arquiteura de Software|
| Gabrielle Ribeiro Gomes | 170011020 | 9 | Responsável pela criação do Backlog, Requisitos de Software, Documento de Arquiteura de Software, Épicos, Diagrama de Sequências, Diagrama de Pacotes, Diagrama de Caso de uso |
| Matheus Henrick Dutra dos Santos | 190018101 | 7 | Responsável pela criação do Backlog, Requisitos de Software, Documento de Arquiteura de Software, Diagrama de Classes, Diagrama de Atividades |
| Jefferson França Santos | 180102761 | 8 | Responsável pela criação do Backlog, Requisitos de Software, Documento de Arquiteura de Software, Épicos, Diagrama de Sequências, Diagrama de Pacotes, Diagrama de Caso de uso |
| Matheus Phillipo Silverio Silva| 150154348 | 7 | Responsável pela criação do Backlog, Requisitos de Software, Documento de Arquiteura de Software, Rich Picture |
| Heitor Marques Simões Barbosa | 202016462 | 9 | Responsável pela criação do Backlog, Requisitos de Software, Documento de Arquiteura de Software, Épicos, Diagrama de Sequências, Diagrama de Pacotes, Diagrama de Caso de uso |


<p style="text-align:center">Fonte: Autoria própria.</p>
