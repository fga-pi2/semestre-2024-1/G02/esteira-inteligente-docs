# **Referências Bibliográficas**

[1] ABNT NBR 17006: Desenho técnico — Requisitos para representação dos métodos de projeção. Disponível em: <https://edisciplinas.usp.br/pluginfile.php/7525605/mod_folder/content/0/NBR17006%20-%20Desenho%20t%C3%A9cnico%20%E2%80%94%20Requisitos%20para%20representa%C3%A7%C3%A3o%20dos%20m%C3%A9todos%20de%20proje%C3%A7%C3%A3o.pdf?forcedownload=1>. Acesso em: 1 maio 2024.

[2] ABNT NBR 5410: Instalações elétricas de baixa tensão. Disponível em: <http://www.inmetro.gov.br/painelsetorial/palestras/palestranbr5410.pdf>. Acesso em: 1 maio 2024.

[3] ABNT NBR 9050: Acessibilidade a edificações, mobiliário, espaços e equipamentos urbanos. Rio de Janeiro, 2020. Disponível em: <https://www.abntcatalogo.com.br/norma.aspx?ID=351818>. Acesso em: 12 jul. 2024.

[4] ALLWINNER H3 Datasheet V1.2. Disponível em: <https://drive.google.com/file/d/1AV0gV4J4V9BVFAox6bcfLu2wDwzlYGHt/view>. Acesso em: 28 abril 2024.

[5] ARDUINO. UNO R3 | Arduino Documentation. Disponível em: <https://docs.arduino.cc/hardware/uno-rev3/>. Acesso em: 29 abril 2024.

[6] BRASIL. Ministério do Trabalho e Emprego. NR-12: Segurança no Trabalho em Máquinas e Equipamentos. Disponível em: <https://www.gov.br/trabalho-e-previdencia/pt-br/assuntos/inspecao/seguranca-e-saude-no-trabalho/normas-regulamentadoras/nr-12>. Acesso em: 12 jul. 2024.

[7] BRASIL. Ministério do Trabalho e Emprego. NR-17: Ergonomia. Disponível em: <https://www.gov.br/trabalho-e-previdencia/pt-br/assuntos/inspecao/seguranca-e-saude-no-trabalho/normas-regulamentadoras/nr-17>. Acesso em: 12 jul. 2024.

[8] BRASIL. Ministério do Trabalho e Emprego. NR-26: Sinalização de Segurança. Disponível em: <https://www.gov.br/trabalho-e-previdencia/pt-br/assuntos/inspecao/seguranca-e-saude-no-trabalho/normas-regulamentadoras/nr-26>. Acesso em: 12 jul. 2024.

[9] CARSLAW, J. M. The friction and wear of elastomers. J. Plastics and Rubber Institute, Vol. 32, 1960.

[10] CREATELY. O Guia Fácil de Diagramas de Classe UML | Tutorial de Diagramas de Classe, 2022. Disponível em: <https://creately.com/blog/pt/diagrama/tutorial-diagrama-de-classes/>. Acesso em: 27 abril 2024.

[11] Django The web framework for perfectionists with deadlines. Disponível em: <https://www.djangoproject.com/>. Acesso em: 09 junho 2024.

[12] Driver A4988, DMOS Microstepping Driver with Translator And Overcurrent Protection. [s.l.: s.n.]. Disponível em: <https://www.pololu.com/file/0J450/a4988_DMOS_microstepping_driver_with_translator.pdf>. Acesso em: 01 maio 2024.

[13] Embarcados.com.br. Bateria de lítio. Disponível em: <https://embarcados.com.br/baterias-de-litio-ion-um-guia-completo/>. Acesso em: 1 maio 2024.

[14] FastAPI framework, high performance, easy to learn, fast to code, ready for production. Disponível em: <https://fastapi.tiangolo.com/>. Acesso em: 09 junho 2024.

[15] Free to use online tool for labelling photos. Disponível em: <https://www.makesense.ai/>. Acesso em: 09 junho 2024.

[16] Handson Technology User Manual 3-Axis CNC/Stepper Motor Shield for Arduino. [s.l.: s.n.]. Disponível em: <https://www.handsontec.com/dataspecs/cnc-3axis-shield.pdf>. Acesso em: 01 maio 2024.

[17] MakeltFrom. Painel de fibra de média densidade (MDF). [S. l.], 30 mai. 2020. Disponível em: <https://www.makeitfrom.com/material-properties/Medium-Density-Fiberboard-MDF>. Acesso em: 1 maio 2024.

[18] MatWeb. Aço ASTM A242, tipo 1, 19 mm de espessura. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/DataSheet.aspx?MatGUID=5a34cd9bab9e4f82afa85943dcc10827>. Acesso em: 1 maio 2024.

[19] MatWeb. Aço Galvanizado ASTM A525. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/datasheet.aspx?MatGUID=abbf07b7f93a4c358a0ddd194f5c18be>. Acesso em: 1 maio 2024.

[20] MatWeb. ATG srl SBR Borracha de Estireno-Butadieno. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/DataSheet.aspx?MatGUID=2e5aa174e89d4f0293d29fc76582ac99>. Acesso em: 1 maio 2024.

[21] MatWeb. Visão geral dos materiais para Acrilonitrila Butadieno Estireno (ABS), Moldado. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/DataSheet.aspx?MatGUID=eb7a78f5948d481c9493a67f0d089646&ckck=1>. Acesso em: 1 maio 2024.

[22] MatWeb. Visão geral dos materiais para aço de alto carbono. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/DataSheet.aspx?MatGUID=ee25302df4b34404b21ad67f8a83e858>. Acesso em: 1 maio 2024.

[23] MatWeb. Visão geral dos materiais para latão. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/DataSheet.aspx?MatGUID=d3bd4617903543ada92f4c101c2a20e5>. Acesso em: 1 maio 2024.

[24] MatWeb. Visão geral dos materiais para liga de PVC/acrílico. [S. l.], 2024. Disponível em: <https://www.matweb.com/search/DataSheet.aspx?MatGUID=2e5aa174e89d4f0293d29fc76582ac99>. Acesso em: 1 maio 2024.

[25] PMEs: o que são as pequenas e médias empresas? Disponível em: <https://www.cnnbrasil.com.br/economia/saiba-o-que-sao-pmes/>. Acesso em: 26 maio 2024.

[26] PMI. Um guia do conhecimento em gerenciamento de projetos. Guia PMBOK® 4a. ed. - EUA, Project Management Institute, 2008.

[27] React The library for web and native user interfaces. Disponível em: <https://react.dev/>. Acesso em: 09 junho 2024.

[28] Under the Hood: YOLOv8 Architecture Explained. Disponível em: <https://keylabs.ai/blog/under-the-hood-yolov8-architecture-explained/>. Acesso em: 09 junho 2024.

[29] Uml Diagrams. Activity Diagrams, 2016. Disponível em: <https://www.uml-diagrams.org/activity-diagrams.html>. Acesso em: 27 abril 2024.

[30] Uml Diagrams. Package Diagrams Overview, 2016. Disponível em: <https://www.uml-diagrams.org/package-diagrams-overview.html>. Acesso em: 27 abril 2024.

[31] Uml Diagrams. Sequence Diagrams Reference, 2016. Disponível em: <https://www.uml-diagrams.org/sequence-diagrams-reference.html>. Acesso em: 27 abril 2024.

[32] Ultralytics YOLOv8. Disponível em: <https://github.com/ultralytics/ultralytics>. Acesso em:

 09 junho 2024.

[33] VANLANDINGHAM, M. R. Friction and wear of elastomers. Rubber Chemistry and Technology, Vol. 73, No. 3, 2000.

[34] Visual Paradigm. Component Diagram Tutorial. Disponível em: <https://online.visual-paradigm.com/diagrams/tutorials/component-diagram-tutorial/>. Acesso em: 28 abril 2024.

[35] What is YOLOv8? The Ultimate Guide. [2024]. Disponível em: <https://blog.roboflow.com/whats-new-in-yolov8/>. Acesso em: 09 junho 2024.