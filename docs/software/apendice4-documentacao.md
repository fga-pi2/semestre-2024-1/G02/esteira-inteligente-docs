# **Documentação de software**

## **Rich picture**

### **1.1. Introdução**

Rich Picture é uma técnica visual que ajuda a compreender complexidades em um sistema, projeto ou situação de maneira mais holística. A técnica do Rich Picture envolve a criação de uma imagem ou diagrama que representa um sistema em questão, com todos os seus elementos, relacionamentos e nuances. Ao fornecer uma representação visual do sistema, o Rich Picture pode ajudar a criar uma compreensão compartilhada de todos os elementos e suas interações, facilitando assim a tomada de decisões.

### **1.2. Metodologia**

A metodologia utilizada para desenvolvimento dos Rich Picture aqui presentes consistem na investigação analítica junto com os stakeholders, afim de identificar todos os elementos que compõem o sistema durante o processo da montagem de um Kit na esteira inteligente.


### **1.3. Rich Picture**

#### **1.3.1. - Visão Geral** 

<h6 align = "center">Figura 1.1: Rich Picture v1</h6>

![rich_picture_v1](../assets/images/rich_picture_visao_geral.png)

<p style="text-align:center">Fonte: Autoria própria.</p>

A primeira versão do Rich Picture, representa o entendimento inicial do sistema.


#### **1.3.2. - Exemplo com Tipos de Usuários**

- Engenheiro de Produto: Possui permissões de Criar/Editar/Listar/Excluir as entidades de kit e item. Isso permite manter o cadastro dos kits produzidos e das peças utilizadas, garantindo a manutenção do seu catálogo e do estoque.

<h6 align = "center">Figura 1.2: Rich Picture - Engenheiro De produto</h6>

![rich_picture_eng_produto](../assets/images/rich_picture_eng_produto.png)

<p style="text-align:center">Fonte: Autoria própria.</p>

- Operador: Tem permissões de Listar e Enviar para produção. Além disso,interage externamente ao sistema com a reposição de itens em falta no estoque.

<h6 align = "center">Figura 1.3: Rich Picture - Operador</h6>

![rich_picture_eng_produto](../assets/images/rich_picture_operador.png)

<p style="text-align:center">Fonte: Autoria própria.</p>


### **1.4. Versionamento Rich Picture**

**<h6 align="center"> Tabela 1.1:Versionamento Rich Picture  </h6>**
| Data       | Versão | Descrição                                             | Autor(es)                                           | Revisor(es)    |
| ---------- | ------ | ----------------------------------------------------- | --------------------------------------------------- | -------------- |
| 26/04/2024 | 1.0    | Criação do Documento de Rich Picture                  | [Matheus Silverio](https://github.com/MattSilverio) | Danilo Domingo |
| 01/05/2024 | 1.1    | Adição dos Rich Pictures por visão de tipo de usuário | [Matheus Silverio](https://github.com/MattSilverio) | Danilo Domingo |

<p style="text-align:center">Fonte: Autoria própria.</p>

## **Documentação de Visão**
### **1.5. Introdução**
#### **1.5.1. Finalidade**
Este documento tem como objetivo apresentar um panorama claro e em detalhado da proposta de projeto da Esteira Inteligente, compartilhado pela equipe de desenvolvimento. Nele, serão abordados diversos aspectos relacionados à execução do software, e forma acessível mesmo para leitores não familiarizados com termos técnicos.

#### **1.5.2. Escopo**
O projeto Esteira Inteligente visa solucionar um problema recorrente na linha de produção de kits de projetos, onde frequentemente ocorrem erros de montagem com peças incorretas ou em quantidades inadequadas. A proposta é automatizar esses processos para minimizar tais falhas.

#### **1.5.3. Visão Geral**

Este documento oferece uma visão abrangente do produto, organizado em tópicos que abordam desde a introdução até os detalhes sobre o posicionamento, as partes envolvidas, os recursos e restrições do produto, além dos critérios de qualidade, prioridades e precedências.


### **1.5. Posicionamento**

#### **1.5.1. Oportunidade de Negócio**

Um dos membros da disciplina estagia na fabrica da Rossi, onde enfrentam problemas recorrentes na linha de produção devido a kits de montagem com erros na contagem de peças e a aparição de peças incorretas. Para reduzir esses erros, propõe-se o uso da Esteira Inteligente, que fornecerá as peças de acordo com o kit selecionado pelo o operador da linha de produção. Após a liberação das peças será realizada uma verificação de quantidade e tipo através de uma câmera e balança, com o objetivo de minimizar ao máximo os erros.

#### **1.5.2. Descrição do Problema**
**<h6 align="center"> Tabela 1.5.2: Descrição do Problema </h6>**

| O problema de     | quantidade e tipo de peças erradas                                                   |
| ----------------- | ------------------------------------------------------------------------------------ |
| afeta             | industrias que apresentam linha de montagem                                          |
| cujo o impacto é  | impossibilidade de montar o kit após a separação das peças                           |
| uma boa solução é | criar um sistema automatizado para entregar as peças na quantidade e tipagem correta |

<p style="text-align:center">Fonte: Autoria própria.</p>


### **1.6. Descrição dos Envolvidos**
#### **1.6.1. Resumo dos Envolvidos**

**<h6 align="center"> Tabela 1.6.1: Resumo dos Envolvidos </h6>**
| Nome                                   | Descrição                            | Responsabilidades     |
| -------------------------------------- | ------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------- |
| Equipe de Desenvolvimento de Software  | Estudantes de Engenharia de Software da Disciplina PI 2                              | Realizar a documentação, bem como o desenvolvimento do Software do projeto.  |
| Equipe de Desenvolvimento de Hardware  | Estudantes de Engenharia de Eletrônica e de Engenharia de Energia da Disciplina PI 2 | Realizar a documentação, bem como o desenvolvimento do Hardware do projeto.  |
| Equipe de Desenvolvimento de Estrutura | Estudantes de Engenharia de Aeroespacial da Disciplina PI 2                          | Realizar a documentação, bem como o desenvolvimento da estrutura do projeto. |

<p style="text-align:center">Fonte: Autoria própria.</p>


#### **1.6.2. Resumo dos Usuários**
**<h6 align="center"> Tabela 1.6.1: Resumo dos Usuários </h6>**
| Nome                  | Descrição               |
| --------------------- |-------------------------------------------------------------------------- |
| Operadores da fábrica | Operadores da linha de produção, que ficam responsáveis pela produção dos kits |

<p style="text-align:center">Fonte: Autoria própria.</p>


#### **1.6.3. Ambiente do Usuário**
Será possível utilizar o site por meio de navegadores web como Mozilla Firefox, Google Chrome e Opera.

#### **1.6.4. Perfis dos Envolvidos**
##### **1.6.4.1. Equipe de Desenvolvimento de Software**
**<h6 align="center"> Tabela 1.10: Equipe de Desenvolvimento de Software </h6>**
| Representantes       | Danilo Domingo, Gabrielle Ribeiro, Heitor, Jefferson França, Matheus Henrick, Matheus Silverio                          |
| -------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Descrição            | Realizar a documentação, bem como o desenvolvimento do Software do projeto.                       |
| Tipo                 | Estudantes de Engenharia de Software da Disciplina PI 2             |
| Critérios de Sucesso | Entregar toda a documentação, bem como a implementação do software proposto dentro do prazo estipulado pela disciplina. |
| Envolvimento         | Alto.|

<p style="text-align:center">Fonte: Autoria própria.</p>


##### **1.6.4.2. Equipe de Desenvolvimento de Hardware**
**<h6 align="center"> Tabela 1.6.4.2: Equipe de Desenvolvimento de Hardware </h6>**
| Representantes       | Joel Jefferson, Maurício, Caio, Michael                  |
| -------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Descrição            | Realizar a documentação, bem como o desenvolvimento do Hardware do projeto.                                             |
| Tipo                 | Estudantes de Engenharia de Eletrônica e de Engenharia de Energia da Disciplina PI 2                 |
| Critérios de Sucesso | Entregar toda a documentação, bem como a implementação do Hardware proposto dentro do prazo estipulado pela disciplina. |
| Envolvimento         | Alto.                                        |
<p style="text-align:center">Fonte: Autoria própria.</p>


##### **1.6.4.3. Equipe de Desenvolvimento de Estrutura**
**<h6 align="center"> Tabela 1.6.4.3: Equipe de Desenvolvimento de Estrutura </h6>**
| Representantes       | Ana Paula, Fernanda, Lucas                                                                                         |
| -------------------- | ------------------------------------------------------------------------------------------------------------------ |
| Descrição            | Realizar a documentação, bem como o desenvolvimento da estrutura do projeto.                                       |
| Tipo                 | Estudantes de Engenharia de Aeroespacial da Disciplina PI 2                                                        |
| Critérios de Sucesso | Entregar toda a documentação, bem como a criação da estrutura proposto dentro do prazo estipulado pela disciplina. |
| Envolvimento         | Alto.                                                                                                              |

<p style="text-align:center">Fonte: Autoria própria.</p>


#### **1.6.5. Perfis dos Usuários**
##### **1.6.5.1. Operadores da Fábrica**
**<h6 align="center"> Tabela 1.6: Operadores da Fábrica </h6>**

| Representantes       | Proprietários de uma cafeteria                                     |
| -------------------- | ------------------------------------------------------------------ |
| Tipo                 | -                                                                  |
| Responsabilidades    | Não possuem envolvimento direto com o desenvolvimento do software. |
| Critérios de Sucesso | Facilidade na montagem de kits                                      |
| Envolvimento         | Médio                                                              |

<p style="text-align:center">Fonte: Autoria própria.</p>


#### **1.6.6. Principais Necessidades dos Envolvidos ou Usuários**
**<h6 align="center"> Tabela 1.6.6: Principais Necessidades dos Envolvidos ou Usuários </h6>**

| Representantes                  | Necessidade                                                              | Prioridade | Solução Atual                                       | Solução Proposta                                                                                   |
| ------------------------------- | ------------------------------------------------------------------------ | ---------- | --------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| Operadores da Linha de Montagem | Minimizar o erro na contagem das peças e da distribuição de peças erradas | Alta       | Processo da separação de kits feito de forma manual | Automatizar a liberação de peças para montagem de kits, com checagem no final por camera e balança |

<p style="text-align:center">Fonte: Autoria própria.</p>


### **1.7. Visão Geral do Produto**
#### **1.7.1. Perspectiva do Produto**
Principais características do produto:
  * Separação das peças a partir da seleção do kit;
  * Quantidade de peças e tipos de peças definidas pela a seleção de kit;
  * Verificação do peso esperado pelo o kit com o a saída da linha através de uma balança;
  * Verificação visual da quantidade e tipos de peças no final da linha, através de camera com uso de inteligência artificial para fazer a checagem.

#### **1.7.2. Resumo das Capacidades**
**<h6 align="center"> Tabela 1.15: Resumo das Capacidades </h6>**

| Benefício para o cliente      | Recursos de Suporte                                                      |
| ----------------------------- | ------------------------------------------------------------------------ |
| Kits pré definidos            | Selecionar os kits cadastrados no sistema com a peças já definidas         |
| Separação das peças           | As peças de acordo com o kit selecionado                                 |
| Checagem das peças            | Após a separação as peças serão checadas para garantir que estão corretas |
| Interface simples e intuitiva | Essencial para não tenham dificuldades na navegação no sistema           |

<p style="text-align:center">Fonte: Autoria própria.</p>


#### **1.7.3. Suposições e Dependências**
Para o correto funcionamento do software, espera-se que a equipe o desenvolva ao longo do período correspondente à disciplina de Projeto Integrador 2. É importante ressaltar que o projeto não está sendo desenvolvido para um cliente específico, mas sim como parte dos requisitos da disciplina mencionada anteriormente. Nesse contexto, também é relevante considerar o aspecto financeiro, visto que a equipe irá contribuir para cobrir os custos e elaborar um protótipo dentro do âmbito da disciplina.

#### **1.7.4. Custo e Precificação**
O produto será disponibilizado em um domínio na WEB, com custos relacionados às equipes de desenvolvimento e gerenciamento do projeto,além dos valores pagos para manter seus servidores em funcionamento. Também terá uma Orange Pi e a Raspberry Pi para o funcionamento da I.A.

#### **1.7.5. Instalação**
O produto será instalado diretamente na Orange Pi e Raspberry Pi, eliminando a necessidade de instalação separada.O acesso será feito via Web, exigindo apenas de um dispositivo com acesso à rede da empresa.

### **1.8. Recursos do Produto**
#### **1.8.1. Cadastro de Operador**
O cadastro de operador inclui dados básicos como nome, número de identificação do operador, setor do mesmo. O operador tem acesso ao kits no sistema, peças no estoque e à operação da linha de montagem.

#### **1.8.2. Cadastro de Kits**
O cadastro de kit é realizado pelo operador e inclui informações sobre as peças que o compõem.

#### **1.8.3. Cadastro de Peças**
O cadastro de peças é realizado pelo operador inclui informações detalhadas sobre cada peça.

#### **1.8.4. Controle de estoque**
O operador pode controlar o estoque de peças na esteira e fazer a recarrega quando necessário.

#### **1.8.5.  Acompanhamento da Separação de Peças**
O operador pode acompanhar a separação das peças, verificando quais foram usadas e, ao final, checar o kit e caso necessário fazer correções, se necessário.

### **1.7. Restrições**
  * O sistema deve estar em funcionamento até o final da disciplina;
  * É necessário um dispositivo com acesso a internet;
  * Ter a Orange Pi e Raspberry Pi com a I.A. treinada.

#### **1.7.1 Restrições Externas**
Dentre as restrições externas, a dificuldade no uso da tecnologia e a compreensão no uso da linguagem estabelecida , além de possíveis transtornos entre a equipe.

#### **1.7.2. Restrições de Design**
Toda a interação com o software deve ser natural, de modo que o operador não tenha dúvidas sobre como realizar determinada tarefa dentro do sistema. Os recursos aos quais o operador tem acesso devem ser de fácil entendimento, para evitar desistência durante a ação. Para facilitar o desenvolvimento será criada uma página web.

### **1.8. Faixas de Qualidade**
Para maior eficiência e acessibilidade, a aplicação será web, permitindo que o operador acesse o sistema fora da linha de produção, caso precise verificar o estoque e os kits cadastrados antes de usar a linha.

### **1.8. Precedência e Prioridade**
Essa etapa descreve as prioridades dos recursos do sistema e dependência de outros recursos (precedência), estabelecendo critérios de prioridade.
**<h6 align="center"> Tabela 1.8.1: Prioridade </h6>**

| Definição da prioridade | Descrição                                                                                                                       |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| Crítico                 | Refere-se a um recurso essencial para o sistema, sem ele as necessidades do cliente não serão atendidas e o projeto fracassará. |
| Importante              | Não determina o fracasso do projeto, mas afetará a satisfação do usuário.                                                       |
| Útil                    | São úteis, porém não críticos. Utilizados, geralmente, com menos frequência. Não afetam a satisfação do usuário.                |

<p style="text-align:center">Fonte: Autoria própria.</p>


**<h6 align="center"> Tabela 1.8.2 Precedência </h6>**

| Recurso                              | Precedência                                               | Prioridade |
| ------------------------------------ | --------------------------------------------------------- | ---------- |
| Cadastro de Operador                 | Não se aplica                                             | Crítico    |
| Cadastro de Peça                     | Cadastro de Operador                                      | Crítico    |
| Cadastro de Kits                     | Cadastro de Operador e Cadastro de Peça                   | Crítico    |
| Controle de estoque                  | Cadastro de Operador                                      | Importante |
| Acompanhamento da Separação de Peças | Cadastro de Operador, Cadastro de Peça e Cadastro de Kits | Útil       |

### **1.9. Histórico de Versão do Documento de Visão**

**<h6 align="center"> Tabela 1.9: Histórico de Versão do Documento de Visão </h6>**

| Data       | Versão | Descrição                     | Autor(es)      | Revisor(es)      |
| ---------- | ------ | ----------------------------- | -------------- | ---------------- |
| 25/04/2024 | 1.0    | Criação do Documento de Visão | Danilo Domingo | Jefferson França |

<p style="text-align:center">Fonte: Autoria própria.</p>

## **YOLOv8**

### **1.10. Visão Geral**

YOLOv8 (You Only Look Once) representa a última iteração na série de detectores de objetos em tempo real YOLO, oferecendo desempenho de ponta em termos de precisão e velocidade. Com avanços em relação às versões anteriores, o YOLOv8 introduz novos recursos e otimizações que o tornam uma escolha ideal para diversas tarefas de detecção de objetos em uma ampla gama de aplicações.

### **1.11. Tarefas Suportadas e Modos**

A série YOLOv8 oferece uma variedade de modelos especializados para tarefas específicas em visão computacional, incluindo:

- Detecção de Objetos
- Segmentação de Instâncias
- Detecção de Poses/Pontos-Chave
- Detecção de Objetos Orientados
- Classificação

Cada variante é otimizada para sua respectiva tarefa, garantindo alto desempenho e precisão.

### **1.12. Uso em CPUS com Exportação ONNX**

Ao exportar modelos YOLOv8 para o formato ONNX, é possível aproveitar a otimização do ONNX Runtime para execução eficiente em CPUs. Isso permite a implantação de modelos YOLOv8 em uma ampla gama de sistemas, desde servidores de alto desempenho até dispositivos embarcados, sem comprometer a velocidade ou a precisão da detecção de objetos.

A combinação do poder do YOLOv8 com a flexibilidade do formato ONNX e do ONNX Runtime oferece uma solução robusta e escalável para implantação de modelos de detecção de objetos em CPUs, garantindo alto desempenho e precisão em uma variedade de cenários de aplicação.

### **1.13. Funcionamento:**

O YOLOv8 baseia-se no conceito central do YOLO desenvolvido pela Ultralytics. Em essência, o YOLOv8 funciona dividindo a imagem de entrada em uma grade, geralmente com um tamanho de 13x13 ou 26x26. Cada célula da grade assume a responsabilidade de prever objetos dentro de seu domínio espacial. A seguir estão as etapas básicas do princípio de funcionamento do YOLOv8.

1.23.1. Processamento de Entrada: O YOLOv8 recebe uma imagem como entrada e a divide em uma grade, cujo tamanho varia conforme o modelo:

    * YOLO Tiny: Grade de 13x13, ideal para dispositivos com menor capacidade de processamento.
    * YOLO Small, Medium, Large, X-Large (S, M, L, X): Grades de 13x13, 26x26, e até 52x52, ajustadas para diferentes níveis de desempenho e precisão.

    Estas grades permitem ao YOLO detectar objetos em diferentes escalas, melhorando a detecção de objetos de vários tamanhos na imagem de entrada.


1.23.2.  Extração de Características: A rede extrai características de alto nível da imagem de entrada usando uma rede neural convolucional profunda (CNN). A escolha da arquitetura da rede geralmente é baseada em modelos estabelecidos como o ResNeXt ou Darknet que representam maneiras específicas de construir a CNN.

1.23.3. Previsão da Caixa Delimitadora: O YOLOv8 prevê a caixa delimitadora para objetos regredindo as coordenadas do canto superior esquerdo, largura e altura da caixa. Além disso, calcula uma pontuação de confiança que indica a probabilidade de que a caixa prevista contenha o objeto.
   
1.23.4. Previsão de Classes: Junto com as previsões das caixas delimitadoras, o YOLOv8 também prevê probabilidades de classes para cada célula da grade. Isso significa que o modelo pode não apenas detectar objetos, mas também identificar suas categorias relativas.
   
1.23.5. Pós-Processamento: Uma vez feitas as previsões, um limiar de confiança é aplicado para filtrar as detecções de baixa confiança. A supressão não-máxima é então usada para remover caixas delimitadoras duplicadas ou sobrepostas, garantindo que apenas as previsões mais precisas continuem.

### **1.14. Diagrama YOLOv8**

A arquitetura do YOLOv8 foi escolhida por utilizar o modelo n, que está sendo utilizada no projeto, conhecido por sua eficiência em termos de velocidade e leveza. Ela incorpora componentes essenciais para realizar a detecção de objetos de forma eficiente. Abaixo, na imagem 1, temos o diagrama apresentando todas as etapas.

<h6 align = "center">Figura 1.4: Diagrama YOLOv8</h6>

![Diagrama YOLOv8](../assets/images/diagramaYOLO.jpg)

<p style="text-align:center">(Fonte: <a href="https://github.com/ultralytics/ultralytics/issues/189">RangeKing</a>)</p>

#### **1.14.1. Backbone**

O Backbone é composto por uma série de camadas convolucionais que extraem as características mais importantes da imagem de entrada.

#### **1.14.2. Camada SPPF**

A camada SPPF, em conjunto com as camadas convolucionais subsequentes, processa as características em diversas escalas. As camadas de Upsample aumentam a resolução dos mapas de características, aprimorando os detalhes.

#### **1.14.3. Módulo C2f**

O módulo C2f combina características de alto nível com informações contextuais, melhorando a precisão e a robustez da detecção.

#### **1.14.4. Detecção**

Por fim, o módulo de detecção utiliza uma combinação de camadas convolucionais e lineares para transformar as características de alta dimensão em caixas delimitadoras e classes de objetos na saída.

### **1.15. Histórico de Versão YOLOv8**

**<h6 align="center"> Tabela 1.15: Histórico de Versão YOLOv8 </h6>**

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 05/06/2024 | 1.0    | Criação do artefato | Jefferson França e Heitor Marques | [Matheus Silverio](https://github.com/MattSilverio) |

<p style="text-align:center">Fonte: Autoria própria.</p>

## **Treinamento IA**

### **1.16. Introdução**

O treinamento da IA via YOLOv8 é um processo fundamental para capacitar modelos de detecção de objetos em imagens. Utilizando conjuntos de dados anotados, o algoritmo ajusta os pesos das suas camadas de convolução para aprender a identificar padrões e características dos objetos. Ao final do treinamento, o modelo resultante é capaz de detectar objetos com alta precisão em tempo real, sendo útil em diversas aplicações de visão computacional.

### **1.17. Metodologia**

#### **1.17.1. Preparação dos Dados**
   - Seleção do conjunto de dados: Utilizou-se um conjunto de dados, ao todo foram tiradas 188 fotos de três objetos diferentes, os quais foram rotulados utilizando a plataforma [MakeSense](https://www.makesense.ai/), como podemos observar na Figura 1.17. É importante ressaltar que as peças utilizadas para o treinamento não são as peças finais do projeto.

<h6 align = "center"> Figura 1.17: Rotulação de Objeto</h6>

![Print do MakeSense](../assets/images/MakeSense.png)

<p style="text-align:center">Fonte: Autoria própria.</p>

#### **1.17.2. Treinamento do Modelo**

- Para o treinamento, após realizar todas as rotulações foi desenvolvido o [arquivo de configuração de treinamento]() do modelo onde é separado o diretório de treino e validação com os nomes das classes e o [código de treino]() com a quantidade de epochs definido.

#### **1.17.3. Avaliação do Modelo**
   - Para avaliar as métricas o YOLOv8 gera métricas ao decorrer do treinamento, Figura 1.6, para avaliação do modelo.

---
<h6 align = "center">Figura 1.17.3: Métricas</h6>

![Print do MakeSense](../assets/images/metricas.png)

<p style="text-align:center">Fonte: Autoria própria.</p>

---

##### **1.17.3.1. Gráficos de Treinamento**

- **Perda por caixa (train/box loss):** A perda por caixa é uma medida de quão bem o modelo prediz as localizações das caixas delimitadoras em torno dos objetos nas imagens. Uma perda por caixa menor indica que o modelo está melhorando na localização precisa dos objetos.

- **Perda de classificação (train/cls loss):** A perda de classificação é uma medida de quão bem o modelo classifica os objetos nas imagens. Uma perda de classificação menor indica que o modelo está melhorando em identificar os objetos corretos.

- **Perda de localização de recurso (train/dfl loss):** A perda de localização de recurso é uma medida de quão bem o modelo localiza os recursos-chave dentro dos objetos nas imagens. Uma perda de localização de recurso menor indica que o modelo está melhorando em identificar os detalhes importantes dos objetos.

- **Precisão (metrics/precision(B)):** A precisão é a proporção de objetos detectados pelo modelo que são realmente objetos. Uma precisão maior indica que o modelo está melhorando em detectar apenas objetos reais.

- **Recall (metrics/recall(B)):** A recuperação é a proporção de objetos reais que são detectados pelo modelo. Uma recuperação maior indica que o modelo está melhorando em detectar todos os objetos reais nas imagens.

##### **1.17.3.2. Gráficos de Validação**

- **Perda por caixa (val/box loss):** A perda por caixa validada é semelhante à perda por caixa de treinamento, mas é calculada em um conjunto de dados de validação diferente que não foi usado para treinar o modelo. Isso ajuda a avaliar o desempenho do modelo em dados não vistos.

- **Perda de classificação (val/cls loss):** A perda de classificação validada é semelhante à perda de classificação de treinamento, mas é calculada em um conjunto de dados de validação diferente.

- **Perda de localização de recurso (val/dfl loss):** A perda de localização de recurso validada é semelhante à perda de localização de recurso de treinamento, mas é calculada em um conjunto de dados de validação diferente.

- **mAP50 (metrics/mAP50(B)):** A mAP50 é uma medida da precisão média do modelo em detectar objetos em diferentes níveis de confiança. Uma mAP50 maior indica que o modelo está melhorando em detectar objetos com precisão.

- **mAP50-95 (metrics/mAP50-95(B)):** A mAP50-95 é semelhante à mAP50, mas se concentra em objetos com pontuações de confiança mais altas. Uma mAP50-95 maior indica que o modelo está melhorando em detectar objetos com alta confiança.

#### **1.17.4. Resultados**
   - Resultados do Treinamento: Abaixo, Vídeo 1, o código de teste foi executado e apresentado o resultado do treinamento.

<h6 align = "center">Video 1: Resultado Modelo</h6>

<div style="text-align: center">
<iframe width="750" height="400" src="https://www.youtube.com/embed/ybC2K-lLb-Q?si=ijrHcJIZpIfCtqO_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
<p style="text-align:center">Fonte: Autoria própria.</p>

### **1.18. Histórico de Versão Treinamento IA**

**<h6 align="center"> Tabela 1.18: Histórico de Versão Treinamento IA </h6>**

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 05/06/2024 | 1.0    | Criação do artefato | Jefferson França e Heitor Marques | [Matheus Silverio](https://github.com/MattSilverio) |

<p style="text-align:center">Fonte: Autoria própria.</p>

## **Protótipo de Alta Fidelidade**

O protótipo de alta fidelidade foi feito tendo em vista as decisões tomadas na guia de estilo e nos requisitos levantados, a ideia desse protótipo é representar com precisão o que se espera do produto final.

Para o montagem do protótipo foi usada a ferramenta de prototipagem do [Figma](https://www.figma.com).

### **1.19. Versão 1.0 - Protótipo**

Protótipo disponível em: https://www.figma.com/design/GhF4ugEQ0wWMHDUpNlY4xX/Prot%C3%B3tipo-Esteira?node-id=0-1&t=7nMWQjsggxCrTQ0e-1

#### **1.19.2. Telas protótipo**

<h6 align = "center">Figura 1.19.2.1: Tela de Login</h6>

![alt text](../assets/images/prototipo/Login.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 align = "center"> Figura 1.19.2.2: Tela de Seleção de Kit</h6>

![alt text](../assets/images/prototipo/Kits.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 align = "center">Figura 1.19.2.3: Tela de Aviso</h6>

![alt text](../assets/images/prototipo/Aviso.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 align = "center">Figura 1.19.2.4: Tela de Erro</h6>

![alt text](../assets/images/prototipo/Error.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

<h6 align = "center">Figura 1.19.2.5: Tela de Sucesso</h6>

![alt text](../assets/images/prototipo/Sucesso.png)
<p style="text-align:center">Fonte: Autoria própria.</p>

### **1.20. Histórico de Versão Protótipo de Alta Fidelidade**

**<h6 align="center"> Tabela 1.20: Histórico de Versão Protótipo de Alta Fidelidade </h6>**

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 01/05/2024 | 1.0    | Criação do protótipo | Danilo Domingo e Matheus Enrick | Jefferson França |

<p style="text-align:center">Fonte: Autoria própria.</p>