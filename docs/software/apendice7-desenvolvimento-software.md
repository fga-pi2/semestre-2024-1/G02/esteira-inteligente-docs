# **Memorial de decisões de desenvolvimento de software**

## **Backlog**

### **1.1. Introdução**

As histórias de usuário são uma técnica essencial na metodologia ágil para capturar requisitos de forma simples e compreensível. Elas representam funcionalidades ou características do sistema do ponto de vista do usuário, descrevendo o que o usuário deseja alcançar ao interagir com o software. Esta abordagem centrada no usuário permite uma comunicação mais eficaz entre desenvolvedores, stakeholders e usuários finais, garantindo que as necessidades e expectativas sejam claramente entendidas e atendidas durante o processo de desenvolvimento.

Cada história de usuário geralmente consiste em um título curto que descreve a funcionalidade desejada e uma descrição mais detalhada que aborda os requisitos específicos, critérios de aceitação e possíveis cenários de uso. Esta estrutura flexível e focada em valor permite uma maior adaptabilidade às mudanças e priorização das funcionalidades mais importantes para os usuários, facilitando a entrega incremental de valor ao longo do ciclo de desenvolvimento do software.

### **1.2. Histórias de Usuário**

**Tabela 1.2.1: História de Usuário**

| Épico |  Feature  | Requisitos |  US   |                                                       Descrição                                                       |   MoSCoW    | Pontuação |
| :---: | :-------: | :--------: | :---: | :-------------------------------------------------------------------------------------------------------------------: | :---------: | :-------: |
|  E01  |    F01    |   RFSO01   | US01  |                       Eu como administrador desejo ser capaz de cadastrar novos kits no sistema                       |  Deve ter   |     1     |
|  E01  |    F01    |   RFSO02   | US02  |                    Eu como administrador desejo ser capaz de editar os kits cadastrados no sistema                    | Deveria ter |     1     |
|  E01  |    F01    |   RFSO03   | US03  |                       Eu como operador desejo ser capaz de listar os kits existentes no sistema                       |  Deve ter   |     1     |
|  E01  |    F01    |   RFSO04   | US04  |                     Eu como administrador desejo ser capaz de excluir kits cadastrados no sistema                     |  Pode ter   |     1     |
|  E01  |    F02    |   RFSO05   | US05  |                       Eu como administrador desejo ser capaz de cadastrar operadores no sistema                       |  Deve ter   |     1     |
|  E01  |    F02    |   RFSO06   | US06  |                        Eu como administrador desejo ser capaz de editar operadores no sistema                         | Deveria ter |     1     |
|  E01  |    F02    |   RFSO07   | US07  |                        Eu como administrador desejo ser capaz de listar operadores no sistema                         | Deveria ter |     1     |
|  E01  |    F02    |   RFSO08   | US08  |                        Eu como administrador desejo ser capaz de excluir operadores no sistema                        |  Deve ter   |     1     |
|  E01  |    F03    |   RFSO09   | US09  |                     Eu como administrador desejo ser capaz de cadastrar os componentes no sistema                     |  Deve ter   |     1     |
|  E01  |    F03    |   RFSO10   | US10  |                  Eu como operador desejo ser capaz de editar a quantidade dos componentes no sistema                  |  Deve ter   |     1     |
|  E01  |    F03    |   RFSO11   | US11  |                         Eu como operador desejo ser capaz de listar os componentes no sistema                         |  Pode ter   |     1     |
|  E01  |    F03    |   RFSO12   | US12  |                      Eu como administrador desejo ser capaz de excluir os componentes no sistema                      |  Pode ter   |     1     |
|  E02  |    F04    |   RFSO13   | US13  |           Eu como operador desejo saber através da IA quando um componente está a mais ou a menos em um kit           |  Deve ter   |     7     |
|  E02  | F04 e F05 |   RFSO14   | US14  | Eu como operador desejo ser informado quando os dados comparados entre a IA e a célula de carga não forem compatíveis |  Deve ter   |     7     |
|  E02  |    F06    |   RFSO15   | US15  |  Eu como operador desejo ser informado quando o estoque de um ou mais componentes estiverem precisando de reposição   |  Deve ter   |     2     |
|  E03  |    F07    |   RFSO16   | US16  |                 Eu como usuário quero que os kits finalizados sejam armazenados em um histórico de log                 |  Deve ter   |     3     |
|  E03  |    F07    |   RFSO16   | US17  |                    Eu como operador quero saber quais componentes foram utilizadas para montar o kit                    |  Deve ter   |     2     |
|  E03  |    F07    |   RFSO16   | US18  |            Eu como operador quero saber a quantidade dos componentes que foram utilizadas para montar o kit             |  Deve ter   |     2     |
|  E02  |    F04    |   RFSO17   | US19  |                        Eu como operador desejo que a IA verifique a qualidade dos componentes                         | Deveria ter |    13     |
|  E01  |    F03    |   RFS018   | US20  |                     Eu como operador desejo ser capaz de selecionar um ou mais kits para produção                     |  Deve ter   |     2     |
|  E01  |    F02    |   RFS019   | US21  |                                     Eu como usuário desejo fazer login no sistema                                     |  Deve ter   |     3     |

<p style="text-align:center">Fonte: Autoria própria.</p>


### **1.3. Histórico de Versão do Backlog**
**<h6 align="center"> Tabela 1.3.1.: Histórico de Versão do Backlog </h6>**

|    Data    | Versão |                          Descrição                          |                     Autor(es)                      |                      Revisor                       |
| :--------: | :----: | :---------------------------------------------------------: | :------------------------------------------------: | :------------------------------------------------: |
| 24/04/2024 |  1.0   |                    Criação do documento                     |           Gabrielle, Jefferson e Heitor            | Danilo, Matheus Henrick, Matheus Silverio e Heitor |
| 25/04/2024 |  1.1   |                        Mapeando US's                        |           Gabrielle, Jefferson e Heitor            | Danilo, Matheus Henrick, Matheus Silverio e Heitor |
| 27/04/2024 |  1.2   | Adição de uma nova US e do método MosCow, pontuação das USs | Danilo, Matheus Henrick, Matheus Silverio e Heitor |               Gabrielle e Jefferson                |

<p style="text-align:center">Fonte: Autoria própria.</p>

## **Épicos**

### **1.4. Introdução**

Na engenharia de software, a gestão eficaz de requisitos é fundamental para o desenvolvimento de sistemas robustos e eficientes. Os épicos de software emergem como uma ferramenta crucial neste contexto, atuando como contêineres de alto nível que encapsulam funcionalidades amplas e complexas de um sistema. 

Estes épicos servem como pontos focais que direcionam a visão estratégica do produto, facilitando a comunicação entre as partes interessadas e orientando a alocação de recursos durante o ciclo de desenvolvimento. 

Este conceito permite às equipes de desenvolvimento abordar de forma organizada e escalável as demandas significativas, dividindo-as em histórias de usuário menores e mais gerenciáveis. Nesta introdução, exploraremos a natureza dos épicos de software, sua importância na gestão de projetos ágeis e as melhores práticas para sua implementação eficaz.

### **1.5. Épicos**


**<h6 align="center"> Tabela 4.2:Épicos da aplicação</h6>**

| Épico |   Descrição   |
| :---: | :-----------: |
|  E01  | Gerenciamento |
|  E02  |   Controle    |
|  E03  |    Análise    |

<p style="text-align:center">Fonte: Autoria própria.</p>


#### **1.6.1. Feature**

**<h6 align="center"> Tabela 4.3: Feature da aplicação</h6>**

| Épico | Feature |                                                                                      Descrição                                                                                      |
| :---: | :-----: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|  E01  |   F01   |                      Gerenciamento de Kits: esta feature engloba todos os requisitos relacionados ao cadastro, edição, listagem e exclusão de kits no sistema.                      |
|  E01  |   F02   |               Gerenciamento de Usuários:  esta feature abrange todas as funcionalidades relacionadas ao cadastro, edição, listagem e exclusão de usuários no sistema.               |
|  E01  |   F03   |                Gerenciamento de Componentes: esta feature trata das funcionalidades relacionadas ao cadastro, edição, listagem e exclusão de componentes no sistema.                |
|  E02  |   F04   |              Controle de Reconhecimento de Componentes por imagem: esta feature foca na integração da IA para reconhecimento dos componentes e controle de qualidade.               |
|  E02  |   F05   |                     Controle de Reconhecimento de Componentes por peso: esta feature foca na integração da célula de carga para reconhecimento dos componentes.                     |
|  E02  |   F06   |                              Controle de Estoque: esta feature foca na funcionalidade de reportar ao usuário quando há necessidade de repor o estoque.                              |
|  E03  |   F07   | Histórico e Análise de Kits: esta feature envolve funcionalidades relacionadas ao armazenamento de kits finalizados, análise de montagens e rastreamento de componentes utilizados. |

<p style="text-align:center">Fonte: Autoria própria.</p>


### **1.7. Histórico de Versão Épicos**
**<h6 align="center"> Tabela 4.4: Histórico de Versão Épicos </h6>**

|    Data    | Versão |      Descrição       |           Autor(es)           | Revisor |
| :--------: | :----: | :------------------: | :---------------------------: | :-----: |
| 25/04/2024 |  1.0   | Criação do Documento | Gabrielle, Jefferson e Heitor | Danilo  |

<p style="text-align:center">Fonte: Autoria própria.</p>