## ** Detalhamento de atividades para integração de software**

Para demonstrar como foi feito a integração dentro dos subsistemas, foi escolhido alguns dos diagramas que estão presente na arquitetura do projeto.

### ** Diagrama de Implantação**
A implantação de software é uma fase crucial no ciclo de vida de desenvolvimento de um sistema, pois envolve a distribuição dos componentes de software em um ambiente de execução. Um diagrama de implantação é uma representação visual dessa distribuição, que ajuda a compreender a arquitetura do sistema e a tomar decisões sobre a infraestrutura necessária.

Com ele pode ser observar como que a comunicação com os demais hardwares do sistema é feito, sendo através de APIs utilizando o método HTTP e através de portas Serial/USB.

<h6 align = "center">Figura 5.3.1: Diagrama de Implantação V1</h6>

![Diagrama de Implantação](../assets/images/diagrama-de-implatacao.png)

<p style="text-align:center">Fonte: Autoria própria.</p>

#### ** Diagrama de Pacotes**
O Diagrama de Pacotes, é um diagrama estático que possibilita a organização mais adequada do sistema representando uma visão em Pacotes. O diagrama completo pode ser encontrado na parte de Diagrama de Pacotes na wiki do projeto. [22](https://www.uml-diagrams.org/package-diagrams-overview.html)

Podemos observar melhor dentro do diagrama de pacotes como são divididas os subsistemas de software e como funciona o fluxo dentro de cada um deles, reforçando a ideia que foi traga no diagrama de implantação de comunicação via API, todos os subsistemas de software estão se comunicado entre si via API utilizando o método HTTP.

<h6 align = "center">Figura 5.3.2: Diagrama de Pacotes</h6>

![alt text](../assets/images/diagrama_pacotes.jpeg)

<p style="text-align:center">Fonte: Autoria própria.</p>

### ** Teste de Integração**

No Vídeo 1, é demonstrado o funcionamento completo do sistema, abrangendo diversas etapas fundamentais. Primeiramente, o usuário é apresentado à tela de login, onde deve inserir suas credenciais para acessar a plataforma. Em seguida, o vídeo mostra a seleção dos kits disponíveis, destacando a interface intuitiva e fácil de usar. Por fim, são exibidas as telas de sucesso e erro, que informam o usuário sobre o status de suas ações de forma clara e objetiva.

Temos também a imagem com as peças identificadas pelo YOLOv8, modelo esse que treinamos anteriormente, e que também será demonstrado na tela para que o usuário possa identificar de maneira mais rápida as peças que estão defeituosas ou faltando.

<div style="text-align: center">
<iframe width="750" height="400" src="https://www.youtube.com/embed/8rkK4V5dNXw?si=8Ozf7KO0egpf5gX3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
<div style="text-align: center;">
  <b>Vídeo 1: Teste de Integração</b> (Fonte: Autoria Própria)
</div>

## ** Versionamento**


| Data       | Versão | Descrição                | Autor(es)                                                                    | Revisor          |
| ---------- | ------ | ------------------------ | ---------------------------------------------------------------------------- | ---------------- |
| 08/06/2024 | 1.0    | Atividades de Integração | Danilo Domingo, Jefferson França, Matheus Henrick, Matheus Silverio e Heitor | -                |
| 09/06/2024 | 1.1    | Adicionando Tópico       | Danilo Domingos, Jefferson França, Matheus Silveiro e Heitor Marques         | Matheus Hendrick |