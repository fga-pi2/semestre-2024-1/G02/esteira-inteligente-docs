# Análise de Imagem

## Câmera tempo real

Para o projeto utilizamos uma câmera que opera em tempo real. Inicialmente, traçamos uma linha em um ponto específico da imagem capturada por essa câmera. A função dessa linha é servir como um ponto de referência: todas as peças rotuladas que cruzarem essa linha serão detectadas e contabilizadas pelo sistema. Esse processo de contagem é essencial para o monitoramento contínuo da produção ou do fluxo de peças.

A contabilização em tempo real permite que o sistema registre cada peça que passa pela linha de referência, garantindo um controle preciso e imediato. Esses dados são armazenados e posteriormente comparados e validados com os dados coletados pela segunda câmera. A integração das informações de ambas as câmeras assegura a precisão dos dados, possibilitando identificar qualquer discrepância e validando a contagem inicial.

![Câmera 1](../assets/images/camera1.png)

<div style="text-align: center;">
  <b>Câmera 1 em funcionamento</b> (Fonte: Autoria própria</a>)
</div>

## Frame da câmera

Para o projeto, utilizamos uma segunda câmera direcionada para a balança ao final do processo da esteira. Essa câmera captura um frame após a conclusão de cada ciclo, identificando e contabilizando os objetos presentes na balança. Essa etapa é crucial para a validação dos dados, pois permite a comparação entre a quantidade de peças detectadas pela primeira câmera e aquelas identificadas pela segunda câmera.

Ao final de cada ciclo, a segunda câmera registra um frame da balança, contabilizando o número de peças e identificando quais são esses objetos. Em seguida, comparamos esses dados com as informações coletadas pela primeira câmera, que monitorou o fluxo de peças ao longo da esteira. Essa comparação é essencial para assegurar a precisão do sistema, detectando quaisquer discrepâncias entre a contagem inicial e a final.

Se houver inconsistências entre os dados das duas câmeras, o sistema sinaliza o erro para o usuário. Essa abordagem permite uma detecção precoce de falhas no processo, possibilitando a correção imediata e garantindo a integridade do controle de qualidade.

![Câmera 2](../assets/images/camera.jpg)

<div style="text-align: center;">
  <b>Câmera 2 em funcionamento</b> (Fonte: Autoria própria</a>)
</div>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 10/07/2024 | 1.0    | Criação do artefato | Jefferson França e Heitor Marques | [Matheus Silverio](https://github.com/MattSilverio) |