# **Testes e Manual de instalação do software**

## Teste de Integração

No Vídeo 1, é demonstrado o funcionamento completo do sistema, abrangendo diversas etapas fundamentais. Primeiramente, o usuário é apresentado à tela de login, onde deve inserir suas credenciais para acessar a plataforma. Em seguida, o vídeo mostra a seleção dos kits disponíveis, destacando a interface intuitiva e fácil de usar. Por fim, são exibidas as telas de sucesso e erro, que informam o usuário sobre o status de suas ações de forma clara e objetiva.

Temos também a imagem com as peças idêntificadas pelo YOLOv8, modelo esse que treinamos anteriormente, e que também será demonstrado na tela para que o usuário possa identificar de maneira mais rápida as peças que estão defeituosas ou faltando.

<div style="text-align: center">
<iframe width="750" height="400" src="https://www.youtube.com/embed/8rkK4V5dNXw?si=8Ozf7KO0egpf5gX3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
<div style="text-align: center;">
  <b>Vídeo 1: Teste de Integração</b> (Fonte: Autoria Própria)
</div>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                                                            | Revisor          |
| ---------- | ------ | ------------------- | -------------------------------------------------------------------- | ---------------- |
| 09/06/2024 | 1.0    | Criação do artefato | Danilo Domingos, Jefferson França, Matheus Silverio e Heitor Marques | Matheus Hendrick |