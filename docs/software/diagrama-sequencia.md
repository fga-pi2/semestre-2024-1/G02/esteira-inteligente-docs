# Diagrama de Sequência

## Introdução

O diagrama de sequência, uma ferramenta essencial na engenharia de software, oferece uma representação visual das interações entre objetos em um sistema ao longo do tempo. Ele é particularmente útil para descrever o fluxo de mensagens entre os objetos durante a execução de um caso de uso, fornecendo uma visão clara e cronológica do comportamento dinâmico do sistema. Ao identificar os objetos envolvidos, suas responsabilidades e as mensagens trocadas, o diagrama de sequência facilita a compreensão e a comunicação entre os membros da equipe de desenvolvimento, contribuindo significativamente para a análise, projeto e implementação de sistemas de software robustos e eficientes.

## Diagrama de Sequência 
![alt text](../assets/images/diagrama_de_sequencia_1.jpeg)
<center>
<b>Figura 1: Diagrama de Sequência "Gerenciamento de Componentes"</b> (Fonte: Autoria Própria)
</center>

![alt text](../assets/images/diagrama_de_sequencia.jpeg)
<center>
<b>Figura 2: Diagrama de Sequência "Gerenciamento de Kits"</b> (Fonte: Autoria Própria)
</center>

![alt text](../assets/images/diagrama_de_sequencia_2.jpeg)
<center>
<b>Figura 3: Diagrama de Sequência "Gerenciamento de Operadores"</b> (Fonte: Autoria Própria)
</center>

![alt text](../assets/images/diagrama_de_sequencia_3.jpeg)
<center>
<b>Figura 4: Diagrama de Sequência "Gerenciamento de Kits"</b> (Fonte: Autoria Própria)
</center>

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 03/05/2024 | 1.0    | Criação do artefato | Jefferson França e Heitor Marques | [Matheus Silverio](https://github.com/MattSilverio) |