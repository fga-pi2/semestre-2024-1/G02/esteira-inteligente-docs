# Diagrama Entidade Relacionamento

## Introdução
Este artefato apresenta o Diagrama Entidade-Relacionamento (DER)  para um sistema de gerenciamento de kits e componentes que compõem o projeto da esteira inteligente, conforme especificado nas imagens fornecidas. O objetivo é descrever as entidades, atributos e relacionamentos de forma clara e organizada para facilitar a implementação do banco de dados.

## Entidades e Relacionamentos

1. **Usuário**
   - **Atributos:**
     - `id_usuario` (PK): Identificador único do usuário.
     - `nome`: Nome do usuário.
     - `id_cargo` (FK): Identificador do cargo do usuário.
     - `id_trabalho`: Identificador do trabalho do usuário.
   - **Relacionamentos:**
     - Um usuário possui um cargo (1:N).

2. **Cargo**
   - **Atributos:**
     - `id_cargo` (PK): Identificador único do cargo.
     - `descricao`: Descrição do cargo.
   - **Relacionamentos:**
     - Um cargo pode ser atribuído a vários usuários.

3. **Kit**
   - **Atributos:**
     - `id_kit` (PK): Identificador único do kit.
     - `nome`: Nome do kit.
     - `peso`: Peso do kit.
   - **Relacionamentos:**
     - Um kit possui vários componentes (1:N) através da tabela de junção `KitComponente`.

4. **Componente**
   - **Atributos:**
     - `id_componente` (PK): Identificador único do componente.
     - `nome`: Nome do componente.
     - `id_tipo` (FK): Identificador do tipo do componente.
   - **Relacionamentos:**
     - Um componente possui um tipo (N:1).

5. **Tipo**
   - **Atributos:**
     - `id_tipo` (PK): Identificador único do tipo.
     - `nome`: Nome do tipo.
   - **Relacionamentos:**
     - Um tipo pode ser atribuído a vários componentes.

6. **Histórico_Produção**
   - **Atributos:**
     - `id_historico` (PK): Identificador único do histórico de produção.
     - `id_kit` (FK): Identificador do kit.
     - `hora_producao`: Hora da produção.
     - `quantidade_pecas`: Quantidade de peças produzidas.
   - **Relacionamentos:**
     - Um histórico de produção registra a relação entre um kit, incluindo o horário da produção e a quantidade de peças totais.

7. **KitComponente**
   - **Atributos:**
     - `id_kit` (FK): Identificador do kit.
     - `id_componente` (FK): Identificador do componente.
     - `quantidade_peca`: Quantidade de peças do componente no kit.
   - **Relacionamentos:**
     - Tabela de junção para representar o relacionamento muitos-para-muitos (N:M) entre **Kit** e **Componente**.

## Estrutura das Tabelas

### Usuário
| Coluna     | Tipo       | Descrição                |
|------------|------------|--------------------------|
| id_usuario | INT (PK)   | Identificador único      |
| nome       | VARCHAR    | Nome do usuário          |
| id_cargo   | INT (FK)   | Identificador do cargo   |
| id_trabalho| VARCHAR    | Identificador do Trabalhador, gerado a partir de seu cargo e nome|

### Cargo
| Coluna     | Tipo       | Descrição                |
|------------|------------|--------------------------|
| id_cargo   | INT (PK)   | Identificador único      |
| descricao  | VARCHAR    | Descrição do cargo       |

### Kit
| Coluna     | Tipo       | Descrição                |
|------------|------------|--------------------------|
| id_kit     | INT (PK)   | Identificador único      |
| nome       | VARCHAR    | Nome do kit              |
| peso       | DECIMAL    | Peso do kit              |

### Componente
| Coluna        | Tipo       | Descrição                |
|---------------|------------|--------------------------|
| id_componente | INT (PK)   | Identificador único      |
| nome          | VARCHAR    | Nome do componente       |
| id_tipo      | INT (FK)   | Identificador do tipo    |

### Tipo
| Coluna  | Tipo       | Descrição                |
|---------|------------|--------------------------|
| id_tipo | INT (PK)   | Identificador único      |
| nome    | VARCHAR    | Nome do tipo             |

### HistoricoProducao
| Coluna        | Tipo       | Descrição                |
|---------------|------------|--------------------------|
| id_historico  | INT (PK)   | Identificador único      |
| id_kit        | INT (FK)   | Identificador do kit     |
| hora_producao | DATETIME   | Hora da produção         |
| quantidade_peca     | INT        | Quantidade de peças      |

### KitComponente
| Coluna        | Tipo       | Descrição                |
|---------------|------------|--------------------------|
| id_kit        | INT (FK)   | Identificador do kit     |
| id_componente | INT (FK)   | Identificador do componente|
| quantidade_peca     | INT        | Quantidade de peças      |

#### Diagrama Entidade-Relacionamento (DER)

![DER](../assets/images/diagrama_entidade_relacionamento.png)

<div style="text-align: center;">
<b>Figura 1: DER - Sistema de gerenciamento dos kits</b> (Fonte: Autoria Própria)
</div>


## Versionamento
| Data | Versão | Descrição | Autor(es) | Revisor(es) |
|------|------|------|------|------|
|05/06/2024|1.0| Criação do Diagrama de Entidade Relacionamento |[Matheus Silverio](https://github.com/MattSilverio)|[Danilo Domingo](https://github.com/danilow200) |
