## **4.3. Projeto do Subsistema de Software**

### **4.3.1. Tecnologias**

Segue a Justificativa das tecnologias escolhidas:

### **4.3.2. Frontend**

* **TypeScript:** A escolha do TypeScript é vem da necessidade de uma abordagem mais estruturada que facilita no desenvolvimento e segura para o frontend. A adição de tipagem estática opcional ao JavaScript oferece uma camada adicional de verificação de tipos, o que resulta em um código mais robusto e com menor chance de erros devido a tipagem;

* **React:** Optar pelo React como biblioteca principal para o frontend se deve à sua popularidade e vasto ecossistema de recursos e suporte. Sua abordagem de componentização simplifica o desenvolvimento de interfaces de usuário interativas e dinâmicas, enquanto sua sintaxe JavaScript familiar permite uma curva de aprendizado suave para desenvolvedores, também possui suporte para o TypeScript; 

* **Next.js:** A escolha do Next.js é motivada pela necessidade de alta performance e flexibilidade no desenvolvimento de aplicações web React. Sua capacidade de renderização no lado do servidor e geração de sites estáticos é particularmente vantajosa para o nosso projeto, possuindo também carregamentos rápidos. Next.js roda perfeitamente com React, permitindo uma fácil construção do nosso sistema;

### **4.3.3. Backend**
* **Python:** A seleção do Python como linguagem principal para o backend é guiada por sua versatilidade, simplicidade e vasta gama de bibliotecas e frameworks disponíveis. Sua sintaxe limpa e legível torna o processo de desenvolvimento mais eficiente e rápido. Também possui as bibliotecas necessárias para poder fazer o treinamento e desenvolvimento da I.A;

* **Django:** A escolha do Django como framework backend é fundamentada em sua estrutura robusta, segura e altamente escalável. A arquitetura baseada em modelos, templates e views simplifica o processo de desenvolvimento. Além disso, a integração nativa do Django com bancos de dados SQL facilita na escolha do modelo de banco de dados.

### **4.3.4. Reconhecimento de Imagem**

* **YOLOv8:** A escolha do YOLOv8 para reconhecimento de imagem é vantajoso devido à sua combinação de alta precisão e velocidade. Ele aprimora a série YOLO com técnicas avançadas e otimizações que melhoram o desempenho em diversos cenários, inclusive em dispositivos de menor potência, tornando-o ideal para aplicações em sistemas embarcados.