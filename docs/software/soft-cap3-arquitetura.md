# Documento de Arquitetura de Software

## 1. Introdução
### 1.1. Finalidade
Este documento oferece uma visão geral arquitetural abrangente do sistema, usando diversas visões arquiteturais para representar diferentes aspectos do sistema. O objetivo deste documento é capturar e comunicar as decisões arquiteturais significativas que foram tomadas em relação ao sistema.

### 1.2. Escopo
Esse documento serve de guia para outros tópicos da construção arquitetural do software, abordando os principais pontos desenvolvidos na arquitetura do projeto, a partir desse breve resumo de cada tópico é possível se orientar com relação a outros documentos da arquitetura desenvolvidos no projeto, onde é possível ter uma visão detalhada de cada tema.

### 1.3. Definições, Acrônimos e Abreviações
  * API: É um acrônimo para Application Programming Interface(Interface de Programação de Aplicações). A API é um conjunto de definições e protocolos usado no desenvolvimento e na integração de software de aplicações, permitindo que um serviço interaja com outros produtos e serviços sem a necessidade de saber como eles foram implementados.
  * AWS: É um acrônimo para Amazon Web Services. O AWS é uma plataforma de serviços de computação em nuvem.
  * DB: É um acrônimo para database(Banco de Dados), local onde persistem os dados que devem ser salvos pela aplicação.
  * HTML: É um acrônimo para HyperText Markup Language(Linguagem de Marcação de Hipertexto). O HTML é o bloco de construção mais básico da web. Define o significado e a estrutura do conteúdo da web.
  *HTTP: É um acrônimo para Hypertext Transfer Protocol(Protocolo de Transferência de Hipertexto). O HTTP é um protocolo que permite a obtenção de recursos, como documentos HTML.
  * REST: É um acrônimo para Representational State Transfer(Transferência Representacional de Estado). O REST é um conjunto de princípios de arquitetura. Os desenvolvedores podem implementar a arquitetura REST de maneiras variadas.
  * UML: É um acrônimo para Unified Modeling Language(Linguagem de Modelagem Unificada). O UML é uma linguagem utilizada para visualizar, especificar, construir e documentar a arquitetura completa de um software, fornecendo informações necessárias para que o desenvolvedor implemente o software.

## 2. Representação Arquitetural
### 2.1. Tecnologias
#### Frontend
  * **TypeScript**: TypeScript é uma linguagem de programação de código aberto desenvolvida pela Microsoft. É um superconjunto sintático estrito de JavaScript e adiciona tipagem estática opcional à linguagem.

  * **React**: O React é um conjunto de bibliotecas open source voltados para a criação de UIs interativas de forma mais fácil. Toda a lógica do React é escrita em JavaScript, facilitando a passagem de dados ao longo da aplicação. Essa tecnologia foi escolhida por conta da quantidade de conteúdo disponível na internet, facilitando o treinamento e aprendizado da equipe. Outro fator importante é a comunidade, que está sempre ativa para ajudar em possíveis dúvidas.
  * **Next.js**: O Next.js é uma estrutura da web de desenvolvimento front-end React de código aberto criada por Vercel que permite funcionalidades como renderização do lado do servidor e geração de sites estáticos para aplicativos da web baseados em React. É uma estrutura pronta para produção que permite que os desenvolvedores criem rapidamente sites estáticos e dinâmicos e é amplamente usada por muitas grandes empresas.

#### Backend
  * **Python**: Python é uma linguagem de programação de alto nível, interpretada de script, imperativa, orientada a objetos, funcional, de tipagem dinâmica e forte.

  * **Django**: Django é um framework para desenvolvimento rápido para web, escrito em Python, que utiliza o padrão model-template-view. Também foi utilizado o banco de dados gerado pelo o Django que utiliza o SQL.

#### **Reconhecimento de imagem**

* Para a parte de reconhecimento de imagem no projeto, optarmos pelo uso do [YOLO](https://arxiv.org/abs/1506.02640) (You Only Look Once) em sua [8° versão](https://github.com/ultralytics/ultralytics) em conjunto com a exportação para o formato [ONNX](https://onnx.ai/).   
  
* **YOLOv8** representa a última iteração na série de detectores de objetos em tempo real YOLO, oferecendo desempenho de ponta em termos de precisão e velocidade. Com avanços sobre as versões anteriores, o YOLOv8 introduz novos recursos e otimizações que o tornam uma escolha ideal para diversas tarefas de detecção de objetos em uma ampla gama de aplicações.

## 3. Metas e Restrições da Arquitetura
### 3.1 - Restrições
  * O software deve ser desenvolvido nas tecnologias definidas;
  * O software deve rodar nos navegadores: Web Firefox e Google Chrome;
  * A I.A. do software deve rodar na Raspberry Pi e Orange Pi; 
  * O ambiente de desenvolvimento do software deve funcionar tanto em Windows, Linux e MacOS;
  * Para utilizar o software é necessário ter internet;
  * O escopo do projeto deve ser concluído até o final da disciplina.

### 3.2 - Metas
As metas planejadas para a aplicação são:

  * **Usabilidade** - O software deve possuir alta aprendibilidade e inteligibilidade, para que atenda aos requisitos elicitados no formulário criado pelo grupo;
  * **Manutenibilidade** - O código e as documentações realizadas pelo grupo devem estar num nível de qualidade, seguindo os padrões de projeto e bibliografia, onde a sua manutenção seja fácil de ser realizada.

## 4. Visão de Casos de Uso
Para representação dos Casos de Uso do sistema especificado, foi criado um diagrama de Casos de Uso que expões os pontos principais do sistema.

![alt text](../assets/images/diagrama_caso_de_uso.jpeg)
<center>
<b>Figura 1: Diagrama de Caso de Uso</b>  (Fonte: Autoria Própria)
</center>


## 5. Visão Lógica
A visão lógica descreve como o sistema é estruturado, em termos de unidade e implementação. Mostra como está a organização conceitual do sistema em termos de camadas, pacotes, classes e interfaces. O relacionamento entre os elementos mostra as dependências, as realizações de interface, os relacionamento parte-todo e assim por diante.

### 5.1 Diagrama de Classes
O Diagrama de Classes representa como que as classes serão realmente programadas, os principais objetos ou as interações entre classes e objetos. O diagrama completo pode ser encontrado na parte de Diagrama de Classes na wiki do projeto.

A versão apresentada é a mais recente do projeto.

![diagrama_de_classes_v1](../assets/images/Diagrama_de_Classes.png)
<center>
<b>Figura 2: Diagrama de Classes V1</b> (Fonte: Autoria Própria)
</center>

### 5.2 Diagrama de Pacotes
O Diagrama de Pacotes, é um diagrama estático que possibilita a organização mais adequada do sistema representando uma visão em Pacotes. O diagrama completo pode ser encontrado na parte de Diagrama de Pacotes na wiki do projeto.

A versão apresentada é a mais recente do projeto.

![alt text](../assets/images/diagrama_pacotes.jpeg)
<center>
<b>Figura 3: Diagrama de Pacotes</b>  (Fonte: Autoria Própria)
</center>


### 5.3 Diagrama de Atividades
O Diagrama de Atividades é uma ferramenta essencial que descreve o fluxo de atividades dentro de um sistema, fornecendo uma visão detalhada das ações realizadas, decisões tomadas e interações entre os elementos do sistema. Ele atua como um guia para os desenvolvedores, permitindo uma compreensão clara de como as atividades são executadas e como os diferentes elementos do sistema se comunicam entre si, facilitando a identificação de oportunidades de otimização e melhorias no processo.
![diagrama_de_atividades_v1](../assets/images/Diagrama_de_Atividades.png)
<center>
<b>Figura 4: Diagrama de Atividades v1</b> (Fonte: Autoria Própria)
</center>

### 5.4 Diagrama de Componente
O Diagrama de Componentes é uma representação visual que descreve a estrutura de componentes de um sistema de software e suas interações. Ele mostra como os diversos componentes do sistema estão organizados e como se relacionam entre si para formar uma arquitetura coesa. Cada componente representa uma unidade lógica e funcional do sistema, encapsulando sua própria funcionalidade e responsabilidades específicas. As conexões entre os componentes indicam as dependências e as interações entre eles, destacando como a informação e o controle fluem dentro do sistema. Ao examinar o Diagrama de Componentes, os arquitetos de software podem entender rapidamente a estrutura geral do sistema e identificar pontos de integração, reutilização e modularidade, facilitando o desenvolvimento, a manutenção e a evolução do sistema de software ao longo do tempo.
![alt text](../assets/images/Diagrama%20de%20Componentes.png)
<center>
<b>Figura 5: Diagrama de Componentes V1</b>  (Fonte: Autoria Própria)
</center>

### 5.5 Diagrama de Implantação
A implantação de software é uma fase crucial no ciclo de vida de desenvolvimento de um sistema, pois envolve a distribuição dos componentes de software em um ambiente de execução. Um diagrama de implantação é uma representação visual dessa distribuição, que ajuda a compreender a arquitetura do sistema e a tomar decisões sobre a infraestrutura necessária.
![Diagrama de Implantação](../assets/images/diagrama-de-implatacao.png)
<div style="text-align: center;">
<b>Imagem 6: Diagrama de Implantação V1</b> (Fonte: Autoria Própria)
</div>

### 5.6 Diagrama YOLO
YOLOv8 (You Only Look Once) representa a última iteração na série de detectores de objetos em tempo real YOLO, oferecendo desempenho de ponta em termos de precisão e velocidade. Com avanços em relação às versões anteriores, o YOLOv8 introduz novos recursos e otimizações que o tornam uma escolha ideal para diversas tarefas de detecção de objetos em uma ampla gama de aplicações.

A arquitetura do YOLOv8 foi escolhida por utilizar o modelo n, que está sendo utilizada no projeto, conhecido por sua eficiência em termos de velocidade e leveza. Ela incorpora componentes essenciais para realizar a detecção de objetos de forma eficiente. Abaixo, na imagem 1, temos o diagrama apresentando todas as etapas.

![Diagrama YOLOv8](../assets/images/diagramaYOLO.jpg)

<div style="text-align: center;">
  <b>Imagem 7: Diagrama YOLOv8</b> (Fonte: <a href="https://github.com/ultralytics/ultralytics/issues/189">RangeKing</a>)
</div>


## 6. Visão de Processos
### 6.1 Visão Geral
Descreve como o sistema de tempo-de-execução é estruturado na forma de um conjunto de elementos que têm interações e comportamento de tempo-de-execução. A estrutura de tempo-de-execução normalmente tem pouca semelhança com a estrutura de código. Consiste de redes de comutação rápida de objetos de comunicação.

### 6.2 Diagrama de Sequência
O diagrama de sequência é uma das soluções fornecidas pela UML, que descreve dinamicamente o ciclo de vida do sistema em desenvolvimento. Descrição detalhada. O foco principal deste diagrama é descrever as interações entre os componentes do sistema, processos e módulos que existem ao mesmo tempo de uma determinada maneira e trocam mensagens entre si. Os ciclos de vida podem ser classes, atores ou até mesmo abstrações que ocorrem entre as classes.

![alt text](../assets/images/diagrama_de_sequencia_1.jpeg)
<center>
<b>Figura 8: Diagrama de Sequência "Gerenciamento de Componentes"</b> (Fonte: Autoria Própria)
</center>

![alt text](../assets/images/diagrama_de_sequencia.jpeg)
<center>
<b>Figura 9: Diagrama de Sequência "Gerenciamento de Kits"</b> (Fonte: Autoria Própria)
</center>

![alt text](../assets/images/diagrama_de_sequencia_2.jpeg)
<center>
<b>Figura 10: Diagrama de Sequência "Gerenciamento de Operadores"</b> (Fonte: Autoria Própria)
</center>

![alt text](../assets/images/diagrama_de_sequencia_3.jpeg)
<center>
<b>Figura 11: Diagrama de Sequência "Gerenciamento de Kits"</b> (Fonte: Autoria Própria)
</center>

## 7. Tamanho e Desempenho
### 7.1 Visão Geral
Discrição do desempenho e das características do software que impactam na arquitetura de software.

### 7.2 Requisitos Mínimos
  * É necessário possuir conexão com a internet;
  * Navegador com suporte a HTML 5, CSS e JavaScript;
  *  Camera para a I.A. fazer o reconhecimento.
  * Raspberry Pi e Orange Pi capaz de comporta a I.A. de reconhecimento do projeto;
  * Para desenvolvimento de possuir: Windows, Linux ou MacOS;

## 8. Qualidade
Qualidade de software tem como objetivo atingir requisitos especificados durante a elaboração do projeto, e as necessidades ou expectativas de usuários e clientes, estando relacionado diretamente com: escalabilidade, manutibilidade, confiabilidade, usabilidade e assim por diante.

## Versionamento

| Data       | Versão | Descrição                   | Autor(es)      | Revisor           |
| ---------- | ------ | --------------------------- | -------------- | ----------------- |
| 03/05/2024 | 1.0    | Documento de Arquitetura V1 | Danilo Domingo | Gabrielle Ribeiro |
| 56/06/2024 | 1.1    | Adiciona reconhecimento de imagem | Heitor Marques | Danilo Domingo |
| 06/06/2024 | 2.0    | Documento de Arquitetura V2 | Danilo Domingo | Heitor Marques |
| 08/06/2024 | 2.1    | Documento de Arquitetura V2.1 | Heitor Marques | Danilo Domingo |