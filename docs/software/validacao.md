# Validação de dados

## Introdução

Para obter um resultado preciso das peças utilizadas em um kit, implementamos um sistema de validação que integra duas câmeras e uma balança, todas operando com um modelo de inteligência artificial. A primeira câmera monitora em tempo real, detectando e contabilizando as peças que passam por uma linha de referência na esteira. A segunda câmera, posicionada sobre a balança no final do processo, captura um frame para identificar e contar os objetos presentes. A balança fornece dados adicionais sobre o peso total, contribuindo para a precisão da identificação.

## Regras de Validação

Para que o sistema valide corretamente o kit de peças, ele deve atender as condições abaixo:

- **Massa total da balança:** A massa total das peças na balança deve corresponder exatamente à massa esperada para a quantidade de peças do kit.

- **Quantidade de peças detectada:** A quantidade de peças identificadas pela câmera de frame no final do processo deve ser igual à quantidade de peças esperada no kit.

Se ambas as condições forem atendidas, então o sistema considera que o kit está correto e exibe uma mensagem de sucesso.

---

- **Câmera de tempo real:** A câmera que monitora em tempo real deve contabilizar uma quantidade de peças igual à quantidade esperada para o kit.
  
- **Massa total da balança:** A massa total das peças na balança deve corresponder exatamente ao peso esperado para o kit.

Se ambas as condições forem atendidas, então o sistema considera que o kit está correto e exibe uma mensagem de sucesso.

---

- **Câmera de tempo real:** A câmera que monitora em tempo real deve contabilizar a quantidade de peças corretamente.

- **Câmera de frame:** A câmera que captura o frame no final do processo deve contar a quantidade de peças corretamente.

- **Massa total da balança:** A massa total das peças na balança deve corresponder exatamente ao peso esperado para o kit.

Se a câmera de tempo real e a câmera de frame contabilizarem a quantidade de peças corretamente, mas a balança não corresponder ao peso esperado, então o sistema detecta um erro.

---

- **Câmera de tempo real:** A câmera que monitora em tempo real deve contabilizar a quantidade de peças corretamente.

- **Câmera de frame:** A câmera que captura o frame no final do processo deve contar a quantidade de peças corretamente.

- **Massa total da balança:** A massa total das peças na balança deve corresponder exatamente ao peso esperado para o kit.
  
Se ambas as câmeras (a de tempo real e a de frame) contabilizarem a quantidade de peças de forma incorreta, mas a balança indicar a massa correta, então o sistema ainda detecta um erro.

---

## Histórico de Versão

| Data       | Versão | Descrição           | Autor(es)                         | Revisor                                             |
| ---------- | ------ | ------------------- | --------------------------------- | --------------------------------------------------- |
| 10/07/2024 | 1.0    | Criação do artefato | Jefferson França e Heitor Marques | [Matheus Silverio](https://github.com/MattSilverio) |