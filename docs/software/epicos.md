# Épicos

## Introdução

Na engenharia de software, a gestão eficaz de requisitos é fundamental para o desenvolvimento de sistemas robustos e eficientes. Os épicos de software emergem como uma ferramenta crucial neste contexto, atuando como contêineres de alto nível que encapsulam funcionalidades amplas e complexas de um sistema. 

Estes épicos servem como pontos focais que direcionam a visão estratégica do produto, facilitando a comunicação entre as partes interessadas e orientando a alocação de recursos durante o ciclo de desenvolvimento. 

Este conceito permite às equipes de desenvolvimento abordar de forma organizada e escalável as demandas significativas, dividindo-as em histórias de usuário menores e mais gerenciáveis. Nesta introdução, exploraremos a natureza dos épicos de software, sua importância na gestão de projetos ágeis e as melhores práticas para sua implementação eficaz.

## Épicos

**Tabela 1: Épicos da aplicação**

| Épico |   Descrição   |
| :---: | :-----------: |
|  E01  | Gerenciamento |
|  E02  |   Controle    |
|  E03  |    Análise    |

### Feature

**Tabela 2: Feature da aplicação**

| Épico | Feature |                                                                                      Descrição                                                                                      |
| :---: | :-----: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|  E01  |   F01   |                      Gerenciamento de Kits: esta feature engloba todos os requisitos relacionados ao cadastro, edição, listagem e exclusão de kits no sistema.                      |
|  E01  |   F02   |               Gerenciamento de Usuários:  esta feature abrange todas as funcionalidades relacionadas ao cadastro, edição, listagem e exclusão de usuários no sistema.               |
|  E01  |   F03   |                Gerenciamento de Componentes: esta feature trata das funcionalidades relacionadas ao cadastro, edição, listagem e exclusão de componentes no sistema.                |
|  E02  |   F04   |              Controle de Reconhecimento de Componentes por imagem: esta feature foca na integração da IA para reconhecimento dos componentes e controle de qualidade.               |
|  E02  |   F05   |                     Controle de Reconhecimento de Componentes por peso: esta feature foca na integração da célula de carga para reconhecimento dos componentes.                     |
|  E02  |   F06   |                     Controle de Estoque: esta feature foca na funcionalidade de reportar ao usuário quando há necessidade de repor o estoque.                    |
|  E03  |   F07   | Histórico e Análise de Kits: esta feature envolve funcionalidades relacionadas ao armazenamento de kits finalizados, análise de montagens e rastreamento de componentes utilizados. |


## Histórico de Versão

|    Data    | Versão |      Descrição       |           Autor(es)           | Revisor |
| :--------: | :----: | :------------------: | :---------------------------: | :-----: |
| 25/04/2024 |  1.0   | Criação do Documento | Gabrielle, Jefferson e Heitor | Danilo  |