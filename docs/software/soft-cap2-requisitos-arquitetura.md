## 2.4. **Requisitos de Software**

Um esboço dos requisitos foram decididos em uma reunião presencial, onde a equipe de software colaborou ativamente na definição e esboço das especificações. Posteriormente, esses requisitos foram organizados e detalhados utilizando a plataforma [Miro](https://miro.com/app/board/uXjVKQT0lBc=/), facilitando a visualização, discussão e acompanhamento por todos os membros envolvidos no projeto.


**<h6 align="center"> Tabela 2.4.1: Requisitos funcionais para o Software do produto</h6>**

|   ID   |             Descrição             |                                            Requisito                                            |
| :----: | :-------------------------------: | :---------------------------------------------------------------------------------------------: |
| RFSO01 |          Cadastrar kits           |                     O sistema deve permitir que os usuários cadastrem kits                      |  |
| RFSO02 |            Editar kits            |                     O sistema deve permitir que os usuários editem os kits                      |
| RFSO03 |            Listar kits            |                     O sistema deve permitir que os usuários listem os kits                      |
| RFSO04 |           Excluir kits            |                     O sistema deve permitir que os usuários excluam os kits                     |
| RFSO05 |        Cadastrar usuários         |                     O sistema deve permitir que usuários sejam cadastrados                      |
| RFSO06 |         Editar usuários          |                       O sistema deve permitir que usuários sejam editados                       |
| RFSO07 |         Listar usuários          |                       O sistema deve permitir que usuários sejam listados                       |
| RFSO08 |         Excluir usuários         |                      O sistema deve permitir que usuários sejam excluídos                       |
| RFSO09 |       Cadastrar componentes       |                 O sistema deve permitir que os usuários cadastre os componentes                 |
| RFSO10 |        Editar componentes         |                  O sistema deve permitir que os usuários editem os componentes                  |
| RFSO11 |        Listar componentes         |                  O sistema deve permitir que os usuários listem os componentes                  |
| RFSO12 |        Excluir componentes        |                  O sistema deve permitir que os usuários exclua os componentes                  |
| RFSO13 |     Reconhecimento de imagens     |            O sistema deve ser capaz de reconhecer as imagens dos elementos dos kits             |
| RFSO14 |      Tratar dados da balança      |  O sistema deve ser capaz de comparar os dados da balança e das imagens dos componentes do kit  |
| RFSO15 |   Gerenciar controle de estoque   |                    O sistema deve ser capaz de realizar controle de estoque                     |
| RFSO16 |  Listagem de componentes usados   | O sistema deve ser capaz de informar quantos e quais componentes, ao concluir a montagem do kit |
| RFSO17 | Avaliar qualidade dos componentes |             O sistema deve ser capaz de avaliar a qualidade dos componentes do kit              |
| RFS018 |   Selecionar kit para produção    |               O sistema deve permitir que o usuários selecione kits para produção               |
| RFS019 |     Autenticação de usuário     |     O sistema deve permitir que o usuários se autentique no sistema com o seu devido papel      |

<p style="text-align:center">Fonte: Autoria própria.</p>


**<h6 align="center"> Tabela 2.4.2: Requisitos não funcionais para o Software do produto</h6>**

|   ID    |                      Descrição                      |                                                               Requisito                                                               |
| :-----: | :-------------------------------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------: |
| RNFSO01 |                  Papel de usuário                   |                                 Os usuários do sistemas serão separados por administrador e operador                                  |
| RNFSO02 |        Tempo de reconhecimento de componente        |                                A detecção e reconhecimento de componentes devem ocorrer em tempo real                                 |
| RNFSO03 |         Poder lidar com múltiplas demandas          |                         O sistema deve ser capaz de lidar com múltiplas solicitações de CRUD simultaneamente                          |
| RNFSO04 | Lidar com múltiplas requisições de origens diversas | O sistema deve ser capaz de suportar a adição de novos tipos de componentes e atualizações  sem interromper as operações em andamento |
| RNFSO05 |             Antecipar possíveis falhas              |                          O sistema deve ser robusto o suficiente para lidar co possíveis falhas de hardware                           |
| RNFSO06 |              Usabilidade na interface               |                                       A interface do usuário deve ser intuitiva e fácil de usar                                       |
| RNFSO07 |                  Mensagens claras                   |                                           Mensagens de erro devem ser claras e informativas                                           |
| RNFSO08 |                   Logs do sistema                   |                  Deve ser implementado um sistema de registro de logs abrangente para rastrear atividades do sistema                  |
| RNFSO09 |                Tecnologia do backend                |                                              O backend sera desenvolvido em Python/Django                                              |
| RNFSO10 |              Tecnologia do Frontend S               |                                            O frontend sera desenvolvido em typescript/React                                            |
| RNFSO11 |             Plataforma que deve operar              |             O sistema deve ser uma aplicação web acessível através de navegadores web padrão, como Chrome, Firefox e etc.             |
| RNFSO12 |            Tecnologia do banco de dados             |                                              O banco de dados sera desenvolvido em MySQL                                              |
<p style="text-align:center">Fonte: Autoria própria.</p>

## Histórico de Versão

|    Data    | Versão |                   Descrição                    |                     Autor(es)                      |                      Revisor                       |
| :--------: | :----: | :--------------------------------------------: | :------------------------------------------------: | :------------------------------------------------: |
| 23/04/2024 |  1.0   |              Criação do Documento              |           Gabrielle, Jefferson e Heitor            |                         -                          |
| 23/04/2024 |  1.1   |              Criação dos RF e RNF              |           Gabrielle, Jefferson e Heitor            |                         -                          |
| 24/04/2024 |  1.2   |             Adicionando introdução             |           Gabrielle, Jefferson e Heitor            | Danilo, Matheus Henrick, Matheus Silverio e Heitor |
| 27/04/2024 |  1.3   | Adicionando RFS018 e RFS019, alterando RNFSO01 | Danilo, Matheus Henrick, Matheus Silverio e Heitor |               Gabrielle e Jefferson                |
