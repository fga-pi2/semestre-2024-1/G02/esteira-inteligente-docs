# Diagrama de Atividades

## Introdução

O Diagrama de Atividades é uma ferramenta visual dinâmica que se concentra na representação do fluxo de interação entre os objetos. Ele oferece uma perspectiva comportamental, destacando a maneira como os objetos interagem e se relacionam ao longo do tempo.

Este diagrama é particularmente útil para dar ênfase aos processos de negócio, fluxos de trabalho e procedimentos, permitindo uma compreensão clara e concisa de como as tarefas são realizadas e sequenciadas dentro de um sistema. Portanto, o Diagrama de Atividades é uma ferramenta essencial para a modelagem eficaz de sistemas complexos.

## Diagrama de Atividades

![diagrama_de_atividades_v1](../assets/images/Diagrama_de_Atividades.png)
<div style="text-align: center;">
<b>Figura 1: Diagrama de Atividades v1</b> (Fonte: Autoria Própria)
</div>

## Versionamento
| Data | Versão | Descrição | Autor(es) | Revisor(es) |
|------|------|------|------|-----|
|30/04/2024|1.0| Criação do Diagrama de Atividades |[Matheus Henrick](https://github.com/MatheusHenrickSantos)| |
|09/06/2024|1.1| Adiciona introdução |Heitor Marques e Danilo Domingo| Matheus Henrick |


