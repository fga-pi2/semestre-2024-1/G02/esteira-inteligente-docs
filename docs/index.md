<style>
.fotos {
  border: 3px solid #1C5739; 
  border-radius: 999px;
  transition: ease-in-out 200ms;
}

.fotos:hover {
  border-color: #309c64;
  box-shadow: 0 0 15px #1C5739;
}  
</style>

<div style="display: flex; flex-direction: column; justify-content: center; align-items: center; text-align: center;">
    <h1 style="font-weight: bold;">Esteira Inteligente</h1>
    <br>
    <div style="text-align: center">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/_agGI31cu70?si=TKL2BA88i2sHtoNq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
    </div>
    <p><strong>Disciplina</strong>: PROJETO INTEGRADOR DE ENGENHARIA 2</br>
    <strong>Código da Disciplina</strong>: FGA0250</br>
    <strong>Número do Grupo</strong>: 02</p>
</div>
</br>

Bem-vindos à página da Esteira Inteligente! Esse projeto tem o principal objetivo de resolver problemas de operador durante a separação de peças industriais através de uma solução tecnológica e semi-automática.

# Nosso grupo:


<h2>Equipe e Documentação de Estruturas</h2>
<div style="display: flex; gap: 10px; justify-content: space-around">
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/anapdesouza" target="_blank">
      <img class="fotos" src="./assets/images/membros/ana.webp" width="300px" height="300px" />
    </a>
    <h4>Ana Paula de Souza</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/fernoronha" target="_blank">
      <img class="fotos" src="./assets/images/membros/fernanda.webp" width="300px" height="300px" />
    </a>
    <h4>Fernanda Noronha</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/lualcorreia" target="_blank">
      <img class="fotos" src="./assets/images/membros/lucas.webp" width="300px" height="300px" />
    </a>
    <h4>Lucas Alexandre</h4>
  </div>
</div>

[Documentação de Estruturas](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-estruturas)

<h2>Equipe e Documentação de Eletrônica e Energia</h2>
<div style="display: flex; gap: 10px; justify-content: space-around">
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/CaioSampaio81194" target="_blank">
      <img class="fotos" src="./assets/images/membros/caio.jpg" width="300px" height="300px" />
    </a>
    <h4>Caio Sampaio</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/JoelJFMoreira" target="_blank">
      <img class="fotos" src="./assets/images/membros/joel.webp" width="300px" height="300px" />
    </a>
    <h4>Joel Moreira</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/Mauriciohoffman" target="_blank">
      <img class="fotos" src="./assets/images/membros/mauricio.webp" width="300px" height="300px" />
    </a>
    <h4>Maurício Hoffman</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/michaelemanuelsc9" target="_blank">
      <img class="fotos" src="./assets/images/membros/michael.webp" width="300px" height="300px" />
    </a>
    <h4>Michael Costa</h4>
  </div>
</div>

[Documentação de Eletrônica e Energia](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-eletrica-eletronica)

<h2>Equipe e Documentação de Software</h2>
<div style="display: flex; gap: 10px; justify-content: space-around">
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/danilow200" target="_blank">
      <img class="fotos" src="./assets/images/membros/danilo.jpeg" width="300px" height="300px" />
    </a>
    <h4>Danilo Domingo</h4>
  </div>

  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/heitormsb" target="_blank">
      <img class="fotos" src="./assets/images/membros/heitor.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Heitor Barbosa</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/Fran6s" target="_blank">
      <img class="fotos" src="./assets/images/membros/jefferson.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Jefferson França</h4>
  </div>
</div>
<div style="display: flex; gap: 10px; justify-content: space-around">
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/MatheusHenrickSantos" target="_blank">
      <img class="fotos" src="./assets/images/membros/henrick.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Matheus Henrick</h4>
  </div>
  <div style="display: grid; justify-items: center">
    <a href="https://gitlab.com/MattSilverio" target="_blank">
      <img class="fotos" src="./assets/images/membros/silveiro.jpeg" width="300px" height="300px"  />
    </a>
    <h4>Matheus Silverio</h4>
  </div>
</div>

[Documentação de Software](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-software)

[Documentação do Backend](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-backend)

[Documentação do Frontend](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-frontend)

[Documentação da Inteligência Articifical](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-inteligencia-artificial)