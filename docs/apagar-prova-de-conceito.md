
# **Projeto e construção de subsistemas da solução proposta**

## 4.1. **Projeto do subsistema Elétrica/Eletrônica**

Visando atender as necessidades do projeto da Esteira Inteligente, foi realizada a divisão de quatro subsistemas, afim de atender aos requisitos propostos para o desenvolvimento da esteira inteligente. São eles:  Subsistema de Controle, Subsistema de Sensoriamento, Subsistema de Motores e atuadores e  Subsistema de Alimentação;

Os componentes foram escolhidos de acordo com os requisitos, onde foram priorizados componentes de disponibilidade abundante no mercado, preços acessíveis e que sejam capazes de realizar de maneira satisfatória os requisitos estabelecidos;

- O subsistema de controle é responsável por realizar o processamento de entrada e saída de dados do sistema como um todo;

- O subsistema de sensoriamento é responsável por receber e processar dados provenientes dos módulos LRD e também da célula de carga juntamente com seu conversor Analógico-digital;

- O subsistema de motores e atuares é responsável por captar e processar dados de entrada e saída provenientes dos motores NEMA 17 juntamente com os drivers controladores de motor;

- O subsistema de alimentação é o responsável por garantir o bom funcionamento dos componentes eletrônicos, fornecendo assim corrente e tensão necessárias.

### 4.1.1. **Dimensionamento dos Cabos**

Para realizar o dimensionamento dos cabos do circuito é necessário primeiro saber a potência total e a tensão do projeto, com esses dois dados é calculada a corrente. Temos que a potência total do projeto é de 118,25W, a tensão utilizada é de 12V e a corrente é de 12,4A. No entendo, como temos um circuito que terá influência de um conversor (de 12V para 5V), vamos calcular a seção para cada uma das partes, sendo a primeira recebendo 12V, 96W e 8A, e a segunda 5V, 22,25W e 4,45A.

Os primeiros passos são iguais para ambos os cenários, logo, eles serão aplicados para os dois. De acordo com a norma ABNT NBR 5410 ([10](bib.md)), o método de instalação da máquina tem o método de referência B1 (Condutores isolados ou cabos unipolares em eletroduto aparente de seção circular sobre parede ou espaçado desta menos de 0,3 vez o diâmetro do eletroduto - Método de Instalação 3).

**<h6 align="center"> Tabela 1: Tipos de linhas elétricas</h6>**
![Tabela33](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_33.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

O número de condutores carregados é de dois (2), de acordo com a configuração eletrônica. Entretanto, no cálculo será utilizado apenas um, pois será necessário dividir o sistema, já que a tensão que passará pelos componentes será diferente (12V e 5V). Segundo a NBR 5410 ([10](bib.md)), teremos que utilizar dois fatores de correção. Em relação ao primeiro fator, temos que para 1 agrupamento de circuitos, Tabela 3 - Fatores de correção aplicáveis a condutores agrupados em feixe (em linhas abertas ou fechadas) e a condutores agrupados num mesmo plano, em camada única (item 1), é aplicado um fator de correção de 1,00. Um segundo fator de correção é necessário pela temperatura ambiente ser diferente de 30 graus célsius, este é obtido na Tabela 2 - Fatores de correção para temperaturas ambientes diferentes de 30ºC para linhas não-subterrâneas e de 20ºC (temperatura do solo) para linhas subterrâneas. Sabendo que o material de isolação é o PVC, seu fator de correção é de 0,94.

**<h6 align="center"> Tabela 2: Fatores de correção para temperaturas ambientes diferentes
de 30ºC para linhas não-subterrâneas e de 20ºC
(temperatura do solo) para linhas subterrâneas </h6>**
![Tabela40](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_40.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

**<h6 align="center"> Tabela 3: Fatores de correção aplicáveis a condutores agrupados em feixe (em linhas abertas ou fechadas) e a condutores agrupados num mesmo plano, em camada única</h6>**
![Tabela42](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_42.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

Para simplificar o processo do fator de correção, podemos multiplicar os dois fatores (temperatura e agrupamento) que resulta num único valor (0,94 x 1,00 = 0,94). 

Partindo para a escolha do cabo, utilizaremos a tabela 36 da norma. Sabendo que nosso método de referência é o B1 e possuímos 2 condutores carregados, primeiro temos que aplicar o fator de correção na corrente suportada pela seção nominal para obtermos um valor real. Visto que no primeiro caso (12V) a corrente é de 8,0A, é necessária uma seção que aguente uma corrente maior que essa. Neste caso, iremos utilizar a seção de 0,5m^2, que suporta uma corrente superior à projetada (9 x 0,94 = 8,46). No segundo caso, a corrente é de 4,45A, para este caso, também utilizaremos a seção de 0,5m^2, que suporta uma corrente superior à projetada (9 x 0,94 = 8,46).

**<h6 align="center"> Tabela 4: Capacidades de condução de corrente, em ampères, para os métodos de referência
A1, A2, B1, B2, C e D</h6>**
![Tabela36](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/G02/esteira-inteligente-docs/-/raw/main/docs/assets/images/Tabela_36.png)
<p style="text-align:center">Fonte: <a href="bib.md"> ABNT NBR 5410. </a></p>

### 4.1.2. **Dimensionamento da Bateria**

Para o dimensionamento foi admitido um requisito não funcional a respeito da eficiência energética, onde apenas um motor funciona por vez. Não permitindo desta forma o funcionamento de 2 ou mais ao mesmo tempo. Desta maneira a máquina passa a possuir um valor de corrente de descarga máxima de 8,5A. Uma bateria de 9,8Ah supre a necessidade do sistema assim como o requisito estabelecido anteriormente.

O tempo estipulado para o funcionamento do sistema de redundância será de cinco minutos, tempo este sendo mais que o suficiente para a conclusão de um ciclo de trabalho. Com esta margem de tempo garantimos uma margem de erro segura e mais que o suficiente para a conclusão de um ciclo, não sendo necessário a utilização de uma bateria maior.

### 4.1.3. **Equipamentos de Segurança**

1. Disjuntor unipolar;
2. Chave fusível;
3. DPS;
4. Fonte chaveada;
5. Aterramento.


Basicamente são 5 equipamentos/procedimentos que realizam a proteção do circuito como um todo. A corrente ao sair da rede passa por uma chave fusível (popularmente conhecida como corta-circuito) que tem a função de proteger contra a sobrecorrente originadas por sobrecargas, curto- circuitos, dentre outros. Atuando no impedimento ou barramento dessa sobrecorrente. Após passar pela chave fusível a corrente se direciona para outro dispositivo de proteção que é o disjuntor unipolar, é um mecanismo de segurança que possui a função  de prevenir que aparelhos e equipamentos sejam danificados, cortando a energia no instante antes de serem atingidos pela sobrecarga. Neste ponto a corrente se divide entre a fonte chaveada e o DPS, a fonte chaveada diminui a corrente consumida pelos equipamentos a níveis mais confiáveis além de atuar como chaves que em caso de falhas desliga automaticamente e funcionar por um curto período como uma bateria em caso de queda de energia. Já o DPS ou dispositivo de proteção contra surtos é um dispositivo que tem como função a proteção da instalação na ocorrência de surtos causados por descargas atmosféricas ou picos de tensão. Este último deve ser ligado ao aterramento para que em caso de falha no dps o aterramento conduzirá a elevada corrente para o solo impedindo a ocorrência de acidentes.

## 4.2. **Projeto do Subsistema de Estruturas**

O projeto estrutural para a Esteira Inteligente parte de alguns subsistemas, sendo eles:

- Subsistema da esteira;
- Subsistema da célula de carga;
- Subsistema de suporte de integração;
- Subsistema de controle.

O subsistema da esteira abrange todos os componentes essenciais para sua constituição, ou seja, o conjunto rolete, o conjunto motor, a cinta, a polia e a correia.

Por conseguinte, o subsistema da célula de carga considera a célula de carga em si, a caixa de acrílico e o prato de medição.

No que tange o subsistema de suporte, tem-se todo os componentes necessários para sustentar e integrar os demais componentes. Sendo assim, ele é composto de barras e conexões que interligam todos os elementos.

Por fim, o subsistema de controle é constituído pelo suporte para as câmeras, a tela, o mouse, o teclado e a caixa de comando que armazena todos os componentes eletrônicos.

Portanto, vale ressaltar que existem critérios técnicos que embasaram as tomadas de decisão de projeto. Assim, questões como disponibilidade de material no mercado, custos associados, propriedades mecânicas e esforços estruturais foram levados em consideração para as escolhas. Tal discussão - que resulta nos desenhos técnicos presentes no Apêndice 2 - será abordada nos tópicos subsequentes.

### 4.2.2. **Métodos de Fabricação e Materiais**

Inicialmente, os componentes e seus respectivos materiais estão descritos na Tabela 4 abaixo.

**<h6 align="center"> Tabela 5: Componentes, materiais e quantidades </h6>**

|         COMPONENTE         |     MATERIAL      | QUANTIDADE |
| :------------------------: | :---------------: | :--------: |
|         Acoplador          |  Alumínio ou ABS  |     3      |
|           Mancal           |        ABS        |     18     |
|           Motor            |        N/A        |     3      |
|      Suporte do motor      |        ABS        |     3      |
|  Caixa da célula de carga  |     Acrílico      |     1      |
|      Prato de medição      |     Acrílico      |     1      |
|      Célula de carga       |     Alumínio      |     1      |
|           Cinta            |     Borracha      |     3      |
|    Correia dentada GT2     |     Borracha      |     3      |
|         Rolamento          |        N/A        |     18     |
|       Cano do rolete       |        PVC        |     9      |
|         CAP Rolete         |        PVC        |     18     |
|      Eixo roscado M8       |       Ferro       |     9      |
|         Polia GT2          |        N/A        |     6      |
|       Conector funil       |        ABS        |     4      |
|     Conector de tampas     |        ABS        |     6      |
|      Peça cilíndrica       |        MDF        |     14     |
|       Peça quadrada        |        MDF        |     14     |
|      Peça triangular       |        MDF        |     14     |
|            Mesa            |      Metalon      |     1      |
|   Barra de estabilização   |      Metalon      |     3      |
|       Chapa inferior       |      Metalon      |     2      |
|           Joelho           |      Metalon      |     2      |
| Barra de suporte com motor |      Metalon      |     3      |
| Barra de suporte sem motor |      Metalon      |     3      |
| Canto de ajuste de ângulo  |        ABS        |     2      |
|           Funil            | Zinco galvanizado |     1      |
|     Suporte em V Caixa     |      Metalon      |     1      |
|      Barra de ângulo       |      Metalon      |     2      |
| Parafuso soberbo 3,5X60mm  |       Latão       |     54     |
| Parafuso soberbo 3,5X30mm  |       Latão       |     11     |
|      Parafuso M3X20mm      |    Aço carbono    |     12     |
|          Porca M3          |    Aço carbono    |     12     |
|      Parafuso M3X80mm      |    Aço carbono    |     3      |
|          Porca M4          |    Aço carbono    |     3      |
|      Porca borboleta       |    Aço carbono    |     3      |
|      Suporte da Câmera     |        N/A        |     2      |
|      Suporte da Tela       |        N/A        |     1      |


<p style="text-align:center">Fonte: Autoria própria.</p>


Os materiais foram selecionados com base nas exigências do mercado e em considerações de custo, visando facilitar a aquisição e montagem do produto de maneira eficiente. Destaca-se a adoção da tecnologia de impressão 3D para a fabricação de alguns componentes, o que elimina a necessidade de usinagem e reduz consideravelmente os custos de produção. Componentes como mancais, suportes do motor, conectores de tampas, cantos de ajuste de ângulo e acopladores - todos eles com elevado grau de complexidade na fabricação - serão produzidos com ABS, aproveitando sua resistência e as características de preenchimento específicas que serão aplicadas.

No que tange às estruturas de integração, elementos como a mesa, o suporte em V da balança, a barra de ângulo e as barras de suporte serão construídos com metalon. Este material é de fácil manipulação e possui custo reduzido em comparação com alternativas como alumínio ou aço. Além disso, é estruturalmente suficiente e não compromete a robustez necessária para suportar os esforços mecânicos esperados no produto, proporcionando uma solução econômica e resistente.

O funil será confeccionado em zinco galvanizado devido à sua facilidade de manuseio e moldagem, além dos benefícios que oferece, como o baixo coeficiente de atrito, tornando-o ideal para a aplicação específica que requer deslizamento das peças.

Resumidamente, as barras do suporte serão soldadas e os encaixes esteira-suporte serão parafusados ou rebitados.

Quanto à cinta, após pesquisa no mercado brasiliense, optou-se por utilizar borracha, pois oferece resistência suficiente para suportar os esforços de tração ao longo de múltiplos ciclos sem comprometer suas propriedades elásticas. Além disso, a borracha possui alto coeficiente de atrito estático e dinâmico, fator fundamental para manter a integridade do funcionamento mecânico da esteira, evitando deslizamentos, quedas de peças ou outros problemas relacionados à movimentação indesejada das peças. 

Abaixo, na Tabela 4, tem-se as principais propriedades mecânicas dos materiais escolhidos para o projeto estrutural. Bem como, na Seção de Arquitetura de Estruturas, tem-se os valores pesquisados para cada respectivo material. Destaca-se que tais dados podem ser usados de argumento para suas escolhas.

**<h6 align="center"> Tabela 6: Propriedades dos Materiais </h6>**

|    **MATERIAL**   	| **MASSA ESPECÍFICA [g/cm3]** 	| **MÓDULO DE ELASTICIDADE [GPa]** 	| **MÓDULO DE CISALHAMENTO [GPa]** 	| **TENSÃO DE ESCOAMENTO [MPa]** 	| **TENSÃO DE RUPTURA [MPa]** 	| **ALONGAMENTO ATÉ RUPTURA [%]** 	|
|:-----------------:	|:---------------------------:	|:--------------------------------:	|:--------------------------------:	|:------------------------------:	|:---------------------------:	|:-------------------------------:	|
|        MDF        	|             0,75            	|                 4                	|                2,5               	|                -               	|              18             	|                -                	|
|        ABS        	|         0,885 - 3,5         	|           0,778 - 21,2           	|                 -                	|           2,00 - 77,0          	|         2,60 - 73,1         	|                -                	|
|     PMMA e PVC    	|         1,26 - 1,45         	|                 -                	|                 -                	|           39,3 - 40,7          	|         42,0 - 46,9         	|                -                	|
|      Borracha     	|            0,950            	|                 -                	|                 -                	|                -               	|         7,00 - 28,0         	|            125 - 850            	|
|      Metalon      	|         0,451 - 8,26        	|            13,8 - 235            	|            78,0 - 82,7           	|           275 - 3340           	|          161 - 3200         	|           0,500 - 30,0          	|
| Zinco galvanizado 	|             7,80            	|                200               	|                80                	|                -               	|              -              	|                -                	|
|       Latão       	|         0,969 - 8,89        	|            75,8 - 121            	|            26,9 - 44,1           	|           34,5 - 683           	|         20,7 - 1030         	|                -                	|
|    Aço carbono    	|             7,80            	|             190 - 215            	|                80                	|               290              	|             435             	|                -                	|

<p style="text-align:center">Fonte: <a href="bib.md"> MakeltFrom, 2024 e MatWeb, 2024. </a></p>


Para o desenvolvimento de um projeto estruturalmente seguro, foram levantados alguns pontos críticos para estudo com cautela. Além de garantir segurança estrutural, os cálculos permitem otimização de geometria para redução de material e, consequentemente, massa e custo. Abaixo, tem-se os pontos críticos levantados pelo grupo:

- Suporte do motor: esforços relacionados à rotação do motor;
- Mancal: esforços relacionados à rotação do motor;
- Correia e cinta: esforços de tração;
- Conjunto rolete: esforços de flexão e torque;
- Fixação mancal e suporte do motor: esforços relacionados à rotação do motor;
- Fixação conjunto caixa e célula de carga: esforços de movimentação e queda de peças. 

Ressalta-se que todos esses pontos terão cálculos estruturais dedicados e serão incluídos no relatório a tempo do Ponto de Controle 2. Contudo, o torque requerido já foi calculado e o passo a passo está descrito abaixo.

### 4.2.3. **Torque requerido do motor**

Para calcular o torque do motor, adotamos o método de inércia de cargas. Inicialmente, analisamos a inércia da carga aplicada individualmente em cada esteira, considerando uma carga de 560g e um diâmetro de rolete de 50mm. Utilizando a Equação 1 abaixo, obtivemos um valor de 0,00035 kg m².

Em seguida, determinamos a inércia do rolete com base nos valores tabelados do material utilizado. Considerando 111g por rolete e dois roletes tracionadores, totalizando 222g, e um diâmetro de 50mm, conforme a Equação 2, encontramos um valor de 0,000069375 kg m².

Por fim, calculamos a inércia da cinta, que possui uma massa de 587g. Com a Equação 3, resultou em um valor de 0,00036688 kg m². Para a inércia do motor, recorremos aos dados fornecidos pelo fabricante no datasheet comercial disponível online.

Os próximos passos envolvem calcular os momentos totais por meio de um somatório simples e, em seguida, determinar o torque de aceleração. Com base nos dados fornecidos pelo fabricante sobre a velocidade máxima do motor em rad/s e seu tempo de resposta, chegamos a um valor de 0,0994 N m, que convertido para kgf cm² resulta em 1,01366427. Esses valores corroboram com os dados experimentais fornecidos pelo fabricante, servindo como uma validação preliminar dos cálculos realizados até o momento.

O próximo cálculo envolve o torque da carga, considerando o peso da carga na esteira e o ângulo de inclinação de 20º em relação ao solo, conforme descrito na equação 6. Obtivemos um valor de 1,70473781 kgf cm². Somando os torques, chegamos a um valor final de 2,718402 kgf cm². Para garantir uma margem de segurança adequada às características da indústria, aplicamos um fator de segurança de 1,5X a esse torque, resultando em um valor final de 4,07760312 kgf cm². Esse é o valor de referência seguro para dimensionar os componentes da esteira sujeitos ao torque gerado pelo funcionamento do motor.

![motor](assets/images/eq_motor.jpg)

Segue abaixo a Tabela 6 com os valores encontrados. 

**<h6 align="center"> Tabela 7: Valores obtidos </h6>**

| Descrição                  | Valor             |
| -------------------------- | ----------------- |
| Inércia da carga           | 0,00035 kg m²     |
| Inércia do rolete          | 0,000069375 kg m² |
| Inércia da cinta           | 0,00036688 kg m²  |
| Inércia total              | 0,0000048 kg m²   |
| Torque de aceleração       | 1,013 kgf cm      |
| Torque de carga            | 1,70 kgf cm       |
| Torque total               | 2,72 kgf cm       |
| Torque necessário do motor | 4,08 kgf cm       |

<p style="text-align:center">Fonte: Autoria própria.</p>

### 4.2.4. **Esforço de tração na correia e na cinta**

Para calcular o esforço de tração foi requirido fazer a avaliação para dois parâmetros de diâmetros distintos. O intuito aqui é fazer uma verificação inicial relacionada ao diâmetro escolhido com o intuito de redução de custos. Dessa forma, os cálculos serão levados em consideração para o diâmetro de 40mm e 50mm do tudo comercial de PVC.

Para isso foi necessário levar em consideração o Torque Total estipulado na seção anterior dado por 0,2667 N m. Além disso, também foi necessário levar em consideração a distância entre os eixos a partir do seu centro, sendo essa distância de 800mm. Levando em consideração um coeficiente de atrito de 0,25 foi possível definir as forças relacionadas a tração.

Inicialmente, realizou-se os cálculos dos ângulos alpha, equação 9, e beta, equação 10, que nesse caso não afetará o sistema apresentado, pois este possui ambos os roletes com mesmo diâmetro.

Em seguida, foi possível calcular a força a partir da equação 11 que considera o Torque total já supracitado. Essa força irá proporcionar o cálculo da força tracionada, equação 12, e a força frouxa, equação 13, que ao serem relacionadas na equação 14 resulta na força de esticamento. 

![tracao](assets/images/tração%20da%20cinta.png)

É importante pontuar que o theta se refere ao radiano do alpha que inicialmente é calculado em graus.

**<h6 align="center"> Tabela 8: Valores obtidos </h6>**

| Dados                  | Resultado 50mm             | Resultado 40mm             |
| -------------------------- | ----------------- | ----------------- |
| Força            | 5,334 N    | 6,6675 N    |
| Força tracionada          | 9,804 N | 12,255 N     |
| Força frouxa          | 4,470 N  | 5,558 N     |
| Força de esticamento              | 14,274 N   | 17,843 N    |

<p style="text-align:center">Fonte: Autoria própria.</p>

### 4.2.4. **Esforços do conjunto rolete e verificar redução de diâmetro**

Para avaliar os esforços do conjunto rolete foi utilizado como base uma viga engastada com uma carga distribuída no valor da força de esticamento. 

Para solucionar este caso, utiliza-se os conceitos simples relacionados ao conceito de vigas a partir do cálculo de reações da viga. 

Dessa forma, inicialmente, foi feito o equilíbrio de forças na equação 15, que resulta neste caso na equação 16 em que R1 representa a força de reação vertical e o W1 representa a força distribuida. 

Considerando que tem-se uma carga retangular, W1 pode ser dado pela equação 17. Em seguida realiza-se o equilíbrio de momentos dado pelas equações 18 e 19. E através desse resultado é possível obter a segunda força de reação.

Nesse caso, vale ressaltar que a aplicação da força concentrada ocorre no centroide da geometria, nesse caso no meio da viga na equação 20 e com isso é possível obter o R2.

Seguindo com os cálculos é obtida a equação de esforço cortante, equação 21, para essa viga e para isso foi feito um corte de seção entre o comprimento de viga que resultou na equação 22.

Por fim, o cálculo do momento fletor é feito seguindo o mesmo princípio para o esforço constante e dessa forma, tem-se a equação 23 e por meio da análise de uma seção é possível determinar a função do momento fletor descrita pela equação 24 e considerando a carga retangular, utiliza-se o desdobramento realizado na equação 25 para finalizar. 

![esforcoengaste](assets/images/esforco_engaste.png)

**<h6 align="center"> Tabela 9: Valores obtidos </h6>**

| Dados                  | Resultado 50mm             | Resultado 40mm             |
| -------------------------- | ----------------- | ----------------- |
| Reação vertical            | 11,4192 N    | 14,2744 N    |
| Reação de momento          | 4,5677 Nm | 5,7098 Nm     |
| Equação Esforço cortante          | -14,274x + 11,4192 | -17,843x + 14,843    |
| Equação Momento Fletor            | -7,137x² + 11,419x - 4,5677   | -8,9215x² + 14,2744x - 5,7098    |

<p style="text-align:center">Fonte: Autoria própria.</p>

Por fim, é possível observar que não há tanta influência nos resultados as duas opções de tubos tanto os de 40mm quanto o de 50mm de diâmetro. Dessa forma, optou-se para o de menor custo que nesse caso é o de 40mm.

## 4.3. **Projeto do Subsistema de Software**

### 4.3.1. Tecnologias

Segue a Justificativa das tecnologias escolhidas:

### 4.3.2. Frontend

* **TypeScript:** A escolha do TypeScript é vem da necessidade de uma abordagem mais estruturada que facilita no desenvolvimento e segura para o frontend. A adição de tipagem estática opcional ao JavaScript oferece uma camada adicional de verificação de tipos, o que resulta em um código mais robusto e com menor chance de erros devido a tipagem;

* **React:** Optar pelo React como biblioteca principal para o frontend se deve à sua popularidade e vasto ecossistema de recursos e suporte. Sua abordagem de componentização simplifica o desenvolvimento de interfaces de usuário interativas e dinâmicas, enquanto sua sintaxe JavaScript familiar permite uma curva de aprendizado suave para desenvolvedores, também possui suporte para o TypeScript; 

* **Next.js:** A escolha do Next.js é motivada pela necessidade de alta performance e flexibilidade no desenvolvimento de aplicações web React. Sua capacidade de renderização no lado do servidor e geração de sites estáticos é particularmente vantajosa para o nosso projeto, possuindo também carregamentos rápidos. Next.js roda perfeitamente com React, permitindo uma fácil construção do nosso sistema;

### 4.3.3. Backend
* **Python:** A seleção do Python como linguagem principal para o backend é guiada por sua versatilidade, simplicidade e vasta gama de bibliotecas e frameworks disponíveis. Sua sintaxe limpa e legível torna o processo de desenvolvimento mais eficiente e rápido. Também possui as bibliotecas necessárias para poder fazer o treinamento e desenvolvimento da I.A;

* **Django:** A escolha do Django como framework backend é fundamentada em sua estrutura robusta, segura e altamente escalável. A arquitetura baseada em modelos, templates e views simplifica o processo de desenvolvimento. Além disso, a integração nativa do Django com bancos de dados SQL facilita na escolha do modelo de banco de dados.

### 4.3.4. Restrições

  Segue as restrições que foram levantadas durante a escolha das tecnologias

  * O software deve ser desenvolvido nas tecnologias definidas;
  * O software deve rodar nos navegadores: Web Firefox e Google Chrome;
  * A I.A. do software deve rodar na Raspberry e Orange PI; 
  * O ambiente de desenvolvimento do software deve funcionar tanto em Windows, Linux e MacOS;
  * Para utilizar o software é necessário ter internet;
  * O escopo do projeto deve ser concluído até o final da disciplina.
